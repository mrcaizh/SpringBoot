package com.javaczh.netty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName NettyApp
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/16 16:32
 * @Version 1.0
 */
@SpringBootApplication
public class NettyApp {
    public static void main(String[] args) {
        SpringApplication.run(NettyApp.class, args);
    }
}
