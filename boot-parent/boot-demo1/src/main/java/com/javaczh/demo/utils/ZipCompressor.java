package com.javaczh.demo.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @ClassName ZipCompressor
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/28 10:26
 * @Version 1.0
 */
public class ZipCompressor {

    /**
     * 字节数字的大小
     */
    private static final int BUFFER = 8192;
    /**
     * 最终输出的压缩包文件流
     */
    private File zipFile;

    public ZipCompressor(String pathName) {
        zipFile = new File(pathName);
    }

    public ZipCompressor(File zipFile) {
        this.zipFile = zipFile;
    }

    public void compress(String... pathName) {
        ZipOutputStream out = null;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
            CheckedOutputStream cos = new CheckedOutputStream(fileOutputStream, new CRC32());
            out = new ZipOutputStream(cos);
            String basedir = "";
            for (int i = 0; i < pathName.length; i++) {
                compress(new File(pathName[i]), out, basedir);
            }
            out.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void compress(String srcPathName) {
        File file = new File(srcPathName);
        if (!file.exists()) {
            throw new RuntimeException(srcPathName + "不存在！");
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
            CheckedOutputStream cos = new CheckedOutputStream(fileOutputStream, new CRC32());
            ZipOutputStream out = new ZipOutputStream(cos);
            String basedir = "";
            compress(file, out, basedir);
            out.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void compress(File file, ZipOutputStream out, String basedir) {
        /* 判断是目录还是文件 */
        if (file.isDirectory()) {
            System.out.println("目录压缩：" + basedir + file.getName());
            this.compressDirectory(file, out, basedir);
        } else {
            System.out.println("文件压缩：" + basedir + file.getName());
            this.compressFile(file, out, basedir);
        }
    }

    /**
     * 压缩一个目录
     *
     * @param dir
     * @param out
     * @param basedir
     */
    private void compressDirectory(File dir, ZipOutputStream out, String basedir) {
        if (!dir.exists()) {
            return;
        }
        File[] files = dir.listFiles();
        for (int i = 0; i < files.length; i++) {
            /* 递归 */
            compress(files[i], out, basedir + dir.getName() + "/");
        }
    }

    /**
     * 压缩一个文件
     */
    private void compressFile(File file, ZipOutputStream out, String basedir) {
        if (!file.exists()) {
            return;
        }
        try {
            BufferedInputStream bis = new BufferedInputStream(
                    new FileInputStream(file));
            //在压缩文件中，每一个压缩的内容都可以用一个ZipEntry表示，所以在进行压缩之前必须通过putNextEntry设置一个ZipEntry即可。
            ZipEntry entry = new ZipEntry(basedir + file.getName());
            out.putNextEntry(entry);
            int count;
            byte data[] = new byte[BUFFER];
            while ((count = bis.read(data, 0, BUFFER)) != -1) {
                out.write(data, 0, count);
            }
            bis.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取 zip的字节缓存
     *
     * @param isDelete 是否删除zip文件
     * @return
     */
    public ByteBuffer getZipByteBuffer(boolean isDelete) {
        //存储字节数据到缓冲区
        ByteBuffer byteBuffer = null;
        try {
            FileInputStream inputStream = new FileInputStream(zipFile);
            byteBuffer = ByteBuffer.allocate(inputStream.available());
            FileChannel channel = inputStream.getChannel();
            //将zip文件的字节流通过通道读取到缓冲区中
            channel.read(byteBuffer);
            inputStream.close();
            channel.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (isDelete) {
                    //删除临时zip压缩文件
                    File f = new File(zipFile.getPath());
                    f.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return byteBuffer;
    }


    /**
     * zip下载: 将文件的压缩包直接通过 HttpServletResponse 响应
     *
     * @param response
     * @param isDelete 删除标记,下载完成之后将zip文件删除
     */
    public void downloadZip(HttpServletResponse response, boolean isDelete) {
        try {
            //以流的形式下载文件。
            FileInputStream inputStream = new FileInputStream(zipFile);
            ByteBuffer byteBuffer = ByteBuffer.allocate(inputStream.available());
            FileChannel channel = inputStream.getChannel();
            channel.read(byteBuffer);
            inputStream.close();
            channel.close();
            // 清空response
            response.reset();
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            //如果输出的是中文名的文件，在此处就要用URLEncoder.encode方法进行处理
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(zipFile.getName(), "UTF-8"));
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
            toClient.write(byteBuffer.array());
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                //删除临时zip压缩文件
                if (isDelete) {
                    File f = new File(zipFile.getPath());
                    f.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 通过 url获取远程服务器的文件,并打包成zip
     *
     * @param urls
     */
    public void compressFileFromUrls(List<String> urls) {
        HttpURLConnection conn = null;
        FileOutputStream fileOutputStream = null;
        ZipOutputStream out = null;
        String fileName;
        String encodeURL;
        try {
            fileOutputStream = new FileOutputStream(zipFile);
            out = new ZipOutputStream(new CheckedOutputStream(fileOutputStream, new CRC32()));
            for (String urlStr : urls) {
                //url后面的中文需要进行url编码,否则会请求失败
                fileName = urlStr.substring(urlStr.lastIndexOf("/") + 1);
                encodeURL = urlStr.substring(0, urlStr.lastIndexOf("/") + 1)
                        .concat(URLEncoder.encode(fileName, "UTF-8"));
                conn = (HttpURLConnection) new URL(encodeURL).openConnection();
                conn.setDoInput(true);
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(6000);
                if (conn.getResponseCode() == 200) {
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
                    ZipEntry entry = new ZipEntry(fileName);
                    out.putNextEntry(entry);
                    int count;
                    byte[] data = new byte[bis.available()];
                    while ((count = bis.read(data, 0, data.length)) != -1) {
                        out.write(data, 0, count);
                    }
                    bis.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}