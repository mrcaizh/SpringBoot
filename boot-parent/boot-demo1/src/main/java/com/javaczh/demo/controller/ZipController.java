package com.javaczh.demo.controller;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @ClassName ZipController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/28 9:33
 * @Version 1.0
 */
@Controller
public class ZipController {

    @RequestMapping("downloadManual")
    public void downLoadFiles(HttpServletRequest request, HttpServletResponse response) {
        try {
            ClassPathResource classPathResource = new ClassPathResource("fileDir");
            InputStream inputStream =classPathResource.getInputStream();
            System.out.println(inputStream.available());
            File dir = new File("E:\\Java\\redis\\redis-2.8.9\\");
            /**创建一个临时压缩文件，我们会把文件流全部注入到这个文件中，这里的文件你可以自定义是.rar还是.zip**/
            File file = new File("E:\\project\\ideaproject\\boot-parent\\boot-demo1\\src\\main\\resources\\测试.zip");
            if (!file.exists()) {
                file.createNewFile();
            }
            response.reset();
            downloadZip2(file, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /**直到文件的打包已经成功了，文件的打包过程被我封装在FileUtil.zipFile这个静态方法中，稍后会呈现出来，接下来的就是往客户端写数据了**/
        //  return response;
    }

    /**
     * 以流的形式下载文件
     *
     * @param file
     * @param response
     * @return
     */
    public static HttpServletResponse downloadZip(File file, HttpServletResponse response) {
        try {
            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(file.getPath()));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            //如果输出的是中文名的文件，在此处就要用URLEncoder.encode方法进行处理
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(file.getName(), "UTF-8"));
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                File f = new File(file.getPath());
//                f.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    public static HttpServletResponse downloadZip2(File file, HttpServletResponse response) {
        try {
            // 以流的形式下载文件。

            FileInputStream inputStream = new FileInputStream(file.getPath());
            ByteBuffer byteBuffer = ByteBuffer.allocate(inputStream.available());
            FileChannel channel = inputStream.getChannel();
            channel.read(byteBuffer);
            inputStream.close();

            // 清空response
            response.reset();
            ServletOutputStream stream = response.getOutputStream();
            OutputStream toClient = new BufferedOutputStream(stream);
            response.setContentType("application/octet-stream");
            //如果输出的是中文名的文件，在此处就要用URLEncoder.encode方法进行处理
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(file.getName(), "UTF-8"));
            toClient.write(byteBuffer.array());
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                File f = new File(file.getPath());
                f.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return response;

    }


    /**
     * 根据输入的文件与输出流对文件进行打包
     *
     * @param inputFile
     * @param ouputStream
     */
    public static void zipFile(File inputFile, ZipOutputStream ouputStream) {
        try {
            if (inputFile.exists()) {
                /**如果是目录的话这里是不采取操作的，至于目录的打包正在研究中**/
                if (inputFile.isFile()) {
                    FileInputStream IN = new FileInputStream(inputFile);
                    BufferedInputStream bins = new BufferedInputStream(IN, 1024);
                    //org.apache.tools.zip.ZipEntry
                    ZipEntry entry = new ZipEntry(inputFile.getName());
                    ouputStream.putNextEntry(entry);
                    // 向压缩文件中输出数据
                    int nNumber;
                    byte[] buffer = new byte[1024];
                    while ((nNumber = bins.read(buffer)) != -1) {
                        ouputStream.write(buffer, 0, nNumber);
                    }
                    // 关闭创建的流对象
                    bins.close();
                    IN.close();
                } else {
                    try {
                        File[] files = inputFile.listFiles();
                        for (int i = 0; i < files.length; i++) {
                            zipFile(files[i], ouputStream);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
