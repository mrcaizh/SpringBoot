package com.javaczh.demo.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * @ClassName SimpleRestClient
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/9 9:30
 * @Version 1.0
 */
@Component
@Slf4j
@Lazy
public class SimpleRestClient extends RestTemplate {


    @PostConstruct
    public void init() {
        /**
         * 在调用RestTemplate请求的接口时，返回中文字是乱码的，
         * 在RestTemplate 的构造函数设置StringHttpMessageConvert这个类中的设置的编码格式为GBK的编码格式-ISO-8859-1
         */
        List<HttpMessageConverter<?>> httpMessageConverters = getMessageConverters();
        httpMessageConverters.stream().forEach(httpMessageConverter -> {
            if (httpMessageConverter instanceof StringHttpMessageConverter) {
                StringHttpMessageConverter messageConverter = (StringHttpMessageConverter) httpMessageConverter;
                messageConverter.setDefaultCharset(StandardCharsets.UTF_8);
            }
        });
    }

    /**
     * @param headerMap    请求头
     * @param remoteUrl    请求URL
     * @param param        请求参数
     * @param responseType 返回类型
     * @param <T>
     * @return
     * @throws Exception
     */
    public <T> T postRemoteData(Map<String, String> headerMap, String remoteUrl, Object param, Class<T> responseType) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        log.info("======================= 请求url:{} =======================", remoteUrl);
        /**
         * 设置请求头
         */
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/json;charset=UTF-8"));
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        if (null != headerMap && !headerMap.isEmpty()) {
            log.info("======================= 设置请求头 :{} =======================", objectMapper.writeValueAsString(headerMap));
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                headers.add(entry.getKey(), entry.getValue());
            }
        }
        /**
         * 设置请求参数
         */
        byte[] bytes = objectMapper.writeValueAsString(param).getBytes(StandardCharsets.UTF_8);
        String bodyData = new String(bytes, StandardCharsets.UTF_8);
        /**
         * 查看HttpEntity的构造方法，包含只有请求头和只有请求体的情况
         */
        log.info("======================= 设置请求体:{} =======================", bodyData);
        HttpEntity<String> httpEntity = new HttpEntity<>(bodyData, headers);
        /**
         * 执行操作
         */
        return postForObject(remoteUrl, httpEntity, responseType);
    }


}
