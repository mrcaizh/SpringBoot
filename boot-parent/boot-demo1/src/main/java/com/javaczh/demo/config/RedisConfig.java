package com.javaczh.demo.config;


import com.javaczh.demo.serializer.SimpleRedisSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @ClassName RedisConfig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/20 15:20
 * @Version 1.0
 */
@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        //设置连接工厂
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        //自定义序列化规则
        SimpleRedisSerializer serializer = new SimpleRedisSerializer();
        //key的序列化规则
        redisTemplate.setKeySerializer(serializer);
        //value的序列化规则
        redisTemplate.setValueSerializer(serializer);
        redisTemplate.setHashKeySerializer(serializer);
        redisTemplate.setHashValueSerializer(serializer);
        return redisTemplate;
    }
}
