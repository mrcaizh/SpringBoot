package com.javaczh.demo.controller;

import com.javaczh.demo.service.UserDemoService;
import com.javaczh.demo.utils.ZipCompressor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName IndexController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/19 20:53
 * @Version 1.0
 */
@RestController
public class IndexController {
    @GetMapping("/hello")
    public String hello() throws IOException {
//        ClassPathResource classPathResource = new ClassPathResource("fileDir");
//        InputStream inputStream =classPathResource.getInputStream();
//        System.out.println(inputStream.available());
        String relativelyPath = System.getProperty("user.dir");
        System.out.println(relativelyPath);
        // 获取跟目录
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        System.out.println(path);
        String parentPath = this.getClass().getClassLoader().getResource("").getPath();
        System.out.println();
        File file = new File(parentPath, "zip".concat(File.separator).concat("resource.zip"));

        System.out.println(file.getAbsolutePath());
        return "部署成功";
    }

    @RequestMapping("download")
    public void downLoadFiles(HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException {
       /* ZipCompressor zc = new ZipCompressor("H:\\zip\\resource.zip");
        zc.compress("H:\\DataGrip2020_53755\\DataGrip 2020\\");*/
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        System.out.println(path);

        String parentPath = this.getClass().getClassLoader().getResource("").getPath();
        File parent = new File(parentPath, "/zip");
        if (!parent.exists()) {
            parent.mkdirs();
        }
        System.out.println("父目录" + parent.getAbsolutePath());
        File file = new File(parent, "resource.zip");
        ZipCompressor zc = new ZipCompressor(file);
        List<String> url = new ArrayList<>(2);
        url.add("http://localhost:9090/Tomcat性能优化笔记.pdf");
        zc.compressFileFromUrls(url);
        zc.downloadZip(response, true);
    }

    @RequestMapping("/download2")
    public ResponseEntity<byte[]> testResponseEntity() throws IOException {
        String zipName = "resource.zip";
        ZipCompressor zc = new ZipCompressor("H:\\zip\\".concat(zipName));
        zc.compress("H:\\DataGrip2020_53755\\DataGrip 2020\\");
        ByteBuffer zipByteBuffer = zc.getZipByteBuffer(true);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf("application/octet-stream"));
        headers.add("Content-Disposition", "attachment;filename=".concat(URLEncoder.encode(zipName, "UTF-8")));
        HttpStatus statusCode = HttpStatus.OK;
        ResponseEntity<byte[]> response = new ResponseEntity<>(zipByteBuffer.array(), headers, statusCode);
        return response;
    }

    @Autowired
    private UserDemoService userDemoService;
    @RequestMapping("/test")
    public void  test()throws IOException {
        System.out.println(userDemoService);
    }

    @GetMapping("/user/add")
    private void update(){
        userDemoService.update();

    }

    @GetMapping("/user/select")
    private void select(){
        userDemoService.select();

    }

}
