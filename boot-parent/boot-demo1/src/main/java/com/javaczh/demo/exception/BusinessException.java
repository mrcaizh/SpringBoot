package com.javaczh.demo.exception;

import com.javaczh.demo.exception.code.ResponseCodeInterface;

/**
 * @ClassName BusinessException
 * @Description
 * @Author CaiZiHao
 * @Date 2020/4/21 22:39
 * @Version 1.0
 */
public class BusinessException extends RuntimeException {
    /**
     * 异常编号
     */
    private final int messageCode;
    /**
     * 异常信息进行补充说明
     */
    private final String detailMessage;

    public BusinessException(int messageCode, String detailMessage) {
        super(detailMessage);
        this.messageCode = messageCode;
        this.detailMessage = detailMessage;
    }
    public BusinessException(ResponseCodeInterface responseCodeInterface) {
        super(responseCodeInterface.getMsg());
        this.messageCode = responseCodeInterface.getCode();
        this.detailMessage = responseCodeInterface.getMsg();
    }


    public int getMessageCode() {
        return messageCode;
    }

    public String getDetailMessage() {
        return detailMessage;
    }
}
