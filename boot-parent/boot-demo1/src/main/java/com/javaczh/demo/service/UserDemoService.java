package com.javaczh.demo.service;

/**
 * @ClassName UserDemoService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/6 13:08
 * @Version 1.0
 */
public interface UserDemoService {
    void update();

    void select();
}
