package com.javaczh.demo.service.impl;

import com.javaczh.demo.entity.SysUser;
import com.javaczh.demo.mapper.UserMapper;
import com.javaczh.demo.service.UserDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName UserDemoServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/6 13:09
 * @Version 1.0
 */
@Service
public class UserDemoServiceImpl implements UserDemoService {
    @Autowired
    private UserMapper userMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update() {
        SysUser sysUser = userMapper.selectByPrimaryKey("126844864776333312023");
        System.out.println(sysUser);
        modified(sysUser);
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCommit() {
                RestTemplate restTemplate = new RestTemplate();
                String forObject = restTemplate.getForObject("http://localhost:9090/user/select", String.class);
                System.out.println(forObject);
            }


        });

    }

  //  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
    public void modified(SysUser sysUser) {

        sysUser.setRealName("Transactional");
        userMapper.updateByPrimaryKeySelective(sysUser);
    }


    @Override
    public void select() {
        SysUser sysUser = userMapper.selectByPrimaryKey("126844864776333312023");
        System.out.println("select:  " + sysUser);
    }
}
