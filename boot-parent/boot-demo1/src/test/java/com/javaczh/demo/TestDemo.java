package com.javaczh.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaczh.demo.config.RedisService;
import com.javaczh.demo.entity.UserVO;
import com.javaczh.demo.utils.SimpleRestClient;
import com.javaczh.system.common.entity.Result;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @ClassName TestDemo
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/26 15:34
 * @Version 1.0
 */
@SpringBootTest(classes = BootApp.class)
public class TestDemo {
    public static void main(String[] args) {

     /*   SysUser user = new SysUser();
        user.setUsername("zs");
        user.setId("12345");
        user.setPhone("123456789");
        UserVO vo = new UserVO();
        UserVO vo1 = UserMapStruct.INSTANCE.voToModel(user);
        System.out.println(vo1);
*/

    }

    @Autowired
    private RedisService redisService;

    @Test
    public void test1() {
        redisService.set("de", "sd");
      /*  SysUser user = new SysUser();
        user.setUsername("zs");
        user.setId("12345");
        user.setPhone("123456789");
        UserVO vo = new UserVO();
        UserVO vo1 = UserMapStruct.INSTANCE.voToModel(user);
        System.out.println(vo1);*/
    }

    @Autowired
    private RestTemplate restTemplate;


    @Test
    public void login() {
        String loginUrl = "http://localhost:8090/api/user/login";
        String param = "{\n" +
                "  \"imageCode\": \"string\",\n" +
                "  \"password\": \"123456\",\n" +
                "  \"type\": \"3\",\n" +
                "  \"username\": \"admin\"\n" +
                "}";

        String s = restTemplate.postForObject(loginUrl, new Login("123456", "admin", "1"), String.class);
        System.out.println(s);

    }

    @Autowired
    private SimpleRestClient simpleRestClient;

    @Test
    public void login2() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String url = "http://localhost:8090/api";
        Result admin = simpleRestClient.
                postRemoteData(null, url.concat("/user/login"), new Login("123456", "admin", "1"), Result.class);
        String data = objectMapper.writeValueAsString(admin.getData());
        UserVO userVO = objectMapper.readValue(data, UserVO.class);
        Map<String,String> header=new LinkedHashMap<>(3);
        header.put("authorization",userVO.getAccessToken());
        Result admin1 = simpleRestClient.postRemoteData(header, url.concat("/user/selectUserByParam"), null, Result.class);
        System.out.println(admin1);
    }


}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Login {
    private String password;
    private String username;
    private String type;
}