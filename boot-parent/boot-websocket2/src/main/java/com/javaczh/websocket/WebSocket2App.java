package com.javaczh.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName WebSocket2App
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/26 9:23
 * @Version 1.0
 */
@SpringBootApplication
public class WebSocket2App {
    public static void main(String[] args) {
        SpringApplication.run(WebSocket2App.class, args);
    }
}
