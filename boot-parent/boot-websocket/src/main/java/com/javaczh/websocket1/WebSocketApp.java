package com.javaczh.websocket1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName WebSocket
 * @Description 实时更新图表数据案例
 * @Author CaiZiHao
 * @Date 2020/5/21 20:37
 * @Version 1.0
 */
@SpringBootApplication
public class WebSocketApp {
    public static void main(String[] args) {
        SpringApplication.run(WebSocketApp.class,args);
    }
}
