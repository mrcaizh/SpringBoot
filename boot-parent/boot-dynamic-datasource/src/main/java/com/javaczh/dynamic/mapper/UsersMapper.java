package com.javaczh.dynamic.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.javaczh.dynamic.entity.Users;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName UsersMapper
 * @Description 
 * @Author CaiZiHao
 * @Date 2020/7/8 16:00
 * @Version 1.0
 */
@DS(value = "users")
public interface UsersMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Users record);

    int insertSelective(Users record);

    Users selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Users record);

    int updateByPrimaryKey(Users record);
}