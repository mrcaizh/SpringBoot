package com.javaczh.dynamic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @ClassName DynamicApplication
 * @Description 添加 @EnableAspectJAutoProxy 注解，重点是配置 exposeProxy = true ，
                希望 Spring AOP 能将当前代理对象设置到 AopContext中
 * @Author CaiZiHao
 * @Date 2020/7/8 15:27
 * @Version 1.0
 */
@SpringBootApplication
@MapperScan(basePackages = "com.javaczh.dynamic.mapper")
@EnableAspectJAutoProxy(exposeProxy = true)
public class DynamicApplication {
    public static void main(String[] args) {
        SpringApplication.run(DynamicApplication.class, args);
    }
}
