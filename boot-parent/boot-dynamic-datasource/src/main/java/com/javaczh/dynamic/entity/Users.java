package com.javaczh.dynamic.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName Users
 * @Description 
 * @Author CaiZiHao
 * @Date 2020/7/8 16:00
 * @Version 1.0
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Users implements Serializable {
    /**
    * 用户编号
    */

    private Integer id;

    /**
    * 账号
    */

    private String username;

    /**
    * 密码
    */
    private String password;

    /**
    * 创建时间
    */

    private Date createTime;

    private static final long serialVersionUID = 1L;
}