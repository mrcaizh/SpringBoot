package com.javaczh.dynamic.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName Orders
 * @Description 
 * @Author CaiZiHao
 * @Date 2020/7/8 16:07
 * @Version 1.0
 */
/**
    * 订单表
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Orders implements Serializable {
    /**
    * 订单编号
    */
    private Integer id;

    /**
    * 用户编号
    */
    private Integer userId;

    private static final long serialVersionUID = 1L;
}