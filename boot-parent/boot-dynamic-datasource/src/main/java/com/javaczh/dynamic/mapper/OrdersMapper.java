package com.javaczh.dynamic.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.javaczh.dynamic.entity.Orders;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName OrdersMapper
 * @Description  @DS 注解，是 dynamic-datasource-spring-boot-starter 提供，可添加在 Service 或 Mapper 的类/接口上，或
 *   者方法上。在其 value 属性种，填写数据源的名字。
 * @Author CaiZiHao
 * @Date 2020/7/8 16:07
 * @Version 1.0
 */
@Mapper
@DS(value = "orders")
public interface OrdersMapper {
    int insert(Orders record);

    int insertSelective(Orders record);
}