
import java.util.Date;import com.javaczh.dynamic.DynamicApplication;
import com.javaczh.dynamic.entity.Users;
import com.javaczh.dynamic.mapper.UsersMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName UserMapper
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 16:32
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DynamicApplication.class)
public class UserMapperTest {

    @Autowired
    private UsersMapper usersMapper;

    @Test
    public void testSelectById() {
        Users users=new Users();
        users.setId(0);
        users.setUsername("123");
        users.setPassword("123213");
        users.setCreateTime(new Date());
        int i = usersMapper.insertSelective(users);
        System.out.println(i);
    }

}