import com.javaczh.dynamic.DynamicApplication;
import com.javaczh.dynamic.entity.Orders;
import com.javaczh.dynamic.entity.Users;
import com.javaczh.dynamic.mapper.OrdersMapper;
import com.javaczh.dynamic.mapper.UsersMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @ClassName OrderMapperTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 16:10
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DynamicApplication.class)
public class OrderMapperTest {
    @Autowired
    private UsersMapper usersMapper;
    @Autowired
    private OrdersMapper ordersMapper;

    @Test
    @Transactional
    public void testSelectById() {
        Orders order=new Orders();
        order.setId(3);
        order.setUserId(2);
        int insert = ordersMapper.insert(order);
        System.out.println(insert);
        System.out.println(this.add());
    }


    public  int add(){
        Users users=new Users();
        users.setId(3);
        users.setUsername("2223");
        users.setPassword("323");
        users.setCreateTime(new Date());
        int i = usersMapper.insertSelective(users);
        System.out.println(i);
        return  i;
    }

}