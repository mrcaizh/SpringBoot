package com.javaczh.demo2.config;

import com.javaczh.demo2.entity.Jeep;
import com.javaczh.demo2.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @ClassName SpringConfig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/23 20:18
 * @Version 1.0
 */
@Configuration
@ComponentScan("com.javaczh.demo2")
public class SpringConfig {

   // @Bean(initMethod = "init", destroyMethod = "destroyMethod")
    @Bean
    //@Scope("prototype")
    public Person person() {
        return new Person( );
    }
    @Bean
    public Jeep jeep(){
        return new Jeep();
    }
}
