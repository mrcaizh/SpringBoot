package com.javaczh.demo2.entity;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * @ClassName Person
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/17 14:29
 * @Version 1.0
 */
public class Person implements DisposableBean, InitializingBean {
    private String name;
    private String age;

    public Person() {
        System.out.println("====== Person 对象创建了 ======");
    }

    public Person(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public void init() {

    }

    public void destroyMethod() {
        System.out.println("销毁");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    /**

     */
    @Override
    public void destroy() throws Exception {
        System.out.println("DisposableBean的销毁");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBean的初始化");
    }
}

