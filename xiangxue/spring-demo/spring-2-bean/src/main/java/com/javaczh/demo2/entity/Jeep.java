package com.javaczh.demo2.entity;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @ClassName Jeep
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/23 21:40
 * @Version 1.0
 */
public class Jeep {
    /**
     * 在Bean创建完成,且属于赋值完成后进行初始化,属于JDK规范的注解
     */
    @PostConstruct
    public void init() {
        System.out.println("Jeep的初始化方法");
    }

    /**
     *  在bean将被移除之前进行通知, 在容器销毁之前进行清理工作
     */
    @PreDestroy
    public void destroyMethod() {
        System.out.println("Jeep的销毁方法");
    }
}
