package com.javaczh.demo2;

import com.javaczh.demo2.config.SpringConfig;
import com.javaczh.demo2.entity.Person;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @ClassName SpringTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/23 20:17
 * @Version 1.0
 */

public class SpringTest {

    @Test
    public void test1() {
        AnnotationConfigApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(SpringConfig.class);
        //获取容器中定义的bean的ID
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String definitionName : beanDefinitionNames) {
            System.out.println(definitionName);
        }
        Person person = applicationContext.getBean("person", Person.class);
        System.out.println(person);
        /*  Person person2 = applicationContext.getBean("person", Person.class);

        System.out.println(person2);*/
        //单实例: 当容器关闭的时候,会调用destroy消耗
        //多实例: 容器只负责初始化,但不会管理bean, 容器关闭不会调用销毁方法,针对多实例，IOC容器不负责销毁的， 是由自己来控制
        applicationContext.close();
    }
}
