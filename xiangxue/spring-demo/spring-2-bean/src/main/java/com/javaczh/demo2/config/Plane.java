package com.javaczh.demo2.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @ClassName Plane
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/27 20:31
 * @Version 1.0
 */
@Component
public class Plane implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public Plane() {
        System.out.println("Plane... . Constructor....");
    }

    /**
     * 对象创建并赋值之后调用
     */
    @PostConstruct
    public void init() {
        System.out.println("Plane... . @PostConstruct....");
    }

    /**
     * 容器移除对象之前回调通知,销毁bean
     */
    @PreDestroy
    public void destory() {
        System.out.println("Plane... .@PreDestroy....");
    }

    /**
     * 容器初始化, 将applicationContext传进来，那么其它方法就可使用到IOC容器了
     * 这个功能是Applicat ionContextAwareProcessor做的
     * 点进看看此类的方法-- >
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
