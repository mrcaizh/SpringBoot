package com.javaczh.demo2.entity;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @ClassName DemoBeanPostProcessor
 * @Description 后置处理器类 后置处理器:初始化前后进行处理工作,将后置处理器加入到容器中
 * @Author CaiZiHao
 * @Date 2020/6/23 21:44
 * @Version 1.0
 */
@Component
public class DemoBeanPostProcessor implements BeanPostProcessor {
    /**
     * 返回一个的对象(传过来的对象)，也可包装好 再返回

     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessBeforeInitialization  bean: "+bean+"    beanName "+beanName);

        return bean;
    }

    /**

     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization  bean: "+bean+"    beanName "+beanName);
        return bean;
    }
}
