package com.javaczh.mvc.controller;

import com.javaczh.mvc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName OrderController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/5 16:50
 * @Version 1.0
 */
@Controller
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/ok")
    public String ok() {
        return "ok";
    }

    @GetMapping("/buy")
    @ResponseBody
    public String buy() {
        orderService.buy();
        return "OrderController.buy() success.......";
    }

}
