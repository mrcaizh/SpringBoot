package com.javaczh.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName AsyncOrderController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/5 18:16
 * @Version 1.0
 */
@Controller
public class AsyncOrderController {
    @ResponseBody
    @RequestMapping("/asyncOrder")
    public Callable<String> order01() {
        System.out.println("主线程开始.." + Thread.currentThread() + "==>" + System.currentTimeMillis());
        //声明
        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("子线程开始.." + Thread.currentThread() + "==>" + System.currentTimeMillis());
                TimeUnit.SECONDS.sleep(3);
                System.out.println("子线程结束.." + Thread.currentThread() + "==>" + System.currentTimeMillis());
                //向页面返回的内容
                return "order buy successul.......";
            }
        };
        System.out.println("主线程结束.." + Thread.currentThread() + "==>" + System.currentTimeMillis());
        return callable;
    }

}