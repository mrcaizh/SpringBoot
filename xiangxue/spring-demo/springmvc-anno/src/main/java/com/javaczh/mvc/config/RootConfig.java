package com.javaczh.mvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * @ClassName RootConfig
 * @Description: Spring的容器不扫描controller;父容器
 * @Author CaiZiHao
 * @Date 2020/7/5 16:32
 * @Version 1.0
 */
//
@ComponentScan(value="com.javaczh.mvc",excludeFilters={
        @ComponentScan.Filter(type= FilterType.ANNOTATION,classes={Controller.class})
})
public class RootConfig {
}
