package com.javaczh.mvc.service;

import org.springframework.stereotype.Service;

/**
 * @ClassName OrderService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/5 16:51
 * @Version 1.0
 */
@Service
public class OrderService {
    public void buy() {
        System.out.println("执行了OrderService.buy方法");
    }
}
