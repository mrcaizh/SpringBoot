package com.javaczh.spring.config;

/**
 * @ClassName Cat
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/22 20:22
 * @Version 1.0
 */
public class Cat {
    public Cat() {
        System.out.println("Cat 对象创建了");
    }
}
