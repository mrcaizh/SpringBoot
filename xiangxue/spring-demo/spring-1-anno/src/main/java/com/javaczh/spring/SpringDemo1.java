package com.javaczh.spring;

import com.javaczh.spring.config.*;
import com.javaczh.spring.entity.Person;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sun.security.krb5.internal.PAEncTSEnc;

/**
 * @ClassName SpringDemo1
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/17 14:28
 * @Version 1.0
 */
public class SpringDemo1 {

    @Test
    public void testXml() {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("bean.xml");
        Person person = applicationContext.getBean("person", Person.class);
        System.out.println(person);
    }

    @Test
    public void testAnno() {
        AnnotationConfigApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(ComponentScanConfig.class);
        //获取容器中定义的bean的ID
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String definitionName : beanDefinitionNames) {
            System.out.println(definitionName);
        }
    }

    @Test
    public void testScope() {
        AnnotationConfigApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(ScopeConfig.class);
        //获取容器中定义的bean的ID
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String definitionName : beanDefinitionNames) {
            System.out.println(definitionName);
        }
        System.out.println("多实例 创建person 对象 ");
        Person person = applicationContext.getBean("person", Person.class);
        Person person2 = applicationContext.getBean("person", Person.class);

        System.out.println(person);
        System.out.println(person2);
        System.out.println(person==person2);
    }
    @Test
    public void testLazy() {
        AnnotationConfigApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(LazyConfig.class);
        //获取容器中定义的bean的ID
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String definitionName : beanDefinitionNames) {
            System.out.println(definitionName);
        }
        System.out.println("开始使用 person 对象, person 才会注册进容器中");
        Person person = applicationContext.getBean("person", Person.class);
    }

    @Test
    public void testConditional() {
        AnnotationConfigApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(ConditionalConfig.class);
        //获取容器中定义的bean的ID
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String definitionName : beanDefinitionNames) {
            System.out.println(definitionName);
        }
    }
    @Test
    public void testImport() {
        AnnotationConfigApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(ImportConfig.class);
        //获取容器中定义的bean的ID
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String definitionName : beanDefinitionNames) {
            System.out.println(definitionName);
        }

        Object demoFactoryBean = applicationContext.getBean("demoFactoryBean");
        System.out.println("demoFactoryBean 的类型 :"+demoFactoryBean.getClass());

    }
}
