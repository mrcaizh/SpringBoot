package com.javaczh.spring.config;

/**
 * @ClassName Bean1
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/22 22:11
 * @Version 1.0
 */
public class Bean1 {
    public Bean1(){
        System.out.println("Bean1 对象创建了");
    }
}
