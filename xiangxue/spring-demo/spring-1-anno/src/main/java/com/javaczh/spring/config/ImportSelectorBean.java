package com.javaczh.spring.config;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @ClassName ImportSelectorBean
 * @Description 自定义逻辑返回需要导入的组件
 * @Author CaiZiHao
 * @Date 2020/6/22 22:10
 * @Version 1.0
 */
public class ImportSelectorBean implements ImportSelector {
    /**
     * 返回值，就是到导入到容器中的组件全类名
     * AnnotationMetadata: 当前标注@Import注解类的所有注解信息，不止能获取到import注解，还能获取到其它注解.
     *
     * @param annotationMetadata
     * @return
     */
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {


        /**返回全类名的bea
         * 方法不要返回null
         * return   null ;打开断点在方法，如果返回空，F 6跟进源码看看，数组. length报空指针,得返回空字符串数组
         * return new String[]{}; 是OK的
         */
        return new String[]{"com.javaczh.spring.config.Bean1"};
    }
}
