package com.javaczh.spring.config;

import com.javaczh.spring.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * @ClassName lazyConfig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/22 19:54
 * @Version 1.0
 */
@Configuration
public class LazyConfig {

    /**
     * 给容器中注册一个bean, 类型为返回值的类型, 默认是单实例
     * 懒加载: 主要针对单实例bean:默认在容器启动的时候创建对象
     * 懒加载:容器启动时候不创建对象, 仅当第一次使用(获取)bean的时候才创建被初始化
     */
    @Bean
    @Lazy
    public Person person() {
        Person person = new Person();
        person.setName("Spring 注解配置");
        person.setAge("18");
        return person;
    }
}
