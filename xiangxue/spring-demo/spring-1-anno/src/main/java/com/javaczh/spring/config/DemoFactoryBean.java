package com.javaczh.spring.config;

import org.springframework.beans.factory.FactoryBean;

/**
 * @ClassName DemoFactoryBean
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/22 23:12
 * @Version 1.0
 */
public class DemoFactoryBean implements FactoryBean<Demo>  {

    @Override
    public Demo getObject() throws Exception {
        return  new Demo();
    }

    @Override
    public Class<?> getObjectType() {
        return Demo.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
