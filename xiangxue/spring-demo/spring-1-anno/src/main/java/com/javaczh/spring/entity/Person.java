package com.javaczh.spring.entity;

import java.util.StringJoiner;

/**
 * @ClassName Person
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/17 14:29
 * @Version 1.0
 */
public class Person {
    private String name;
    private String age;

    public Person() {
        System.out.println("====== Person 对象创建了 ======");
    }

    public Person(String name, String age) {
        this.name = name;
        this.age = age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

   /* @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("age='" + age + "'")
                .toString();
    }*/
}

