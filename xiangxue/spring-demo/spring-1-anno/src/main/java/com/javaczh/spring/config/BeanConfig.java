package com.javaczh.spring.config;

import com.javaczh.spring.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName BeanConfig
 * @Description @Configuration 代表该类是一个spring配置类 配置类等于配置文件
 * @Author CaiZiHao
 * @Date 2020/6/17 14:42
 * @Version 1.0
 */
@Configuration
public class BeanConfig {

    /**
     * 给spring 容器中注册一个bean 类型为方法的返回值
     * 默认使用方法名作为bean的Id 注册进容器,
     * 如果 在 @Bean("person1")的value值写上 person1 ,则以value 的值作为bean 的id注册进容器中
     * @return
     */
    @Bean(value = "person1")
    public Person person() {
        Person person = new Person();
        person.setName("Spring 注解配置");
        person.setAge("18");
        return person;
    }
}
