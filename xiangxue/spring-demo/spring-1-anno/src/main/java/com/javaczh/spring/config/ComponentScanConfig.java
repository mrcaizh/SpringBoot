package com.javaczh.spring.config;

import com.javaczh.spring.controller.OrderController;
import com.javaczh.spring.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * @ClassName ComponentScan
 * @Description value = "com.javaczh.spring.config" 作用:指定要扫描的包
 * @Author CaiZiHao
 * @Date 2020/6/17 14:55
 * @Version 1.0
 */
@Configuration
@ComponentScan(value = "com.javaczh.spring",
        includeFilters  = {
                @Filter(type = FilterType.CUSTOM, classes = {
                        DemoTypeFilter .class
                })
        }, useDefaultFilters = false)
public class ComponentScanConfig {
    @Bean
    public Person person() {
        Person person = new Person();
        person.setName("ComponentScan 定义的 Person ");
        person.setAge("20");
        return person;
    }
}
