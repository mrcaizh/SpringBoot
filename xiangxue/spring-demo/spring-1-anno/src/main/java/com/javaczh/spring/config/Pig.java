package com.javaczh.spring.config;

/**
 * @ClassName Pig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/22 22:26
 * @Version 1.0
 */
public class Pig {
    public Pig() {
        System.out.println("Pig 对象创建了");
    }
}
