package com.javaczh.spring.config;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @ClassName DemoImportBeanDefinitionRegistrar
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/22 22:25
 * @Version 1.0
 */
public class DemoImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    /**
     * AnnotationMetadata:当前类的注解信息
     * BeanDefinitionRegistry:BeanDefinition注册类
     * 把所有需要添加到容器中的bean加入;
     *
     * @Scope
     */
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        //Import 导入的类都是以全类名作为Bean的id 注册进容器
        boolean bean1 = registry.containsBeanDefinition("com.javaczh.spring.config.Dog");
        boolean bean2 = registry.containsBeanDefinition("com.javaczh.spring.config.Cat");
        //如果Dog和Cat同时存在于我们IOC容器中,那么创建Pig类, 加入到容器
        //对于我们要注册的bean, 给bean进行封装,
        if (bean1 && bean2) {
            /**
             *  以前的BEAN都是全类名，现在自定义bean名|
             * 跟进registerBeanDefinition ()，第二个参数beanDefinition
             * 指定Bean的定义信息( Bean的类型，bean的scope)
             */
            RootBeanDefinition beanDefinition = new RootBeanDefinition(Pig.class);
            registry.registerBeanDefinition("pig", beanDefinition);
        }
    }

}
