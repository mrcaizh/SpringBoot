package com.javaczh.spring.config;

import com.javaczh.spring.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName ConditionalConfig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/22 20:08
 * @Version 1.0
 */
@Configuration
public class ConditionalConfig {
    @Bean("person")
    public Person person(){
        System.out.println("给容器中添加person.......");
        return new Person("person","20");
    }

    @Conditional(WinCondition.class)
    @Bean("window")
    public Person window(){
        System.out.println("当前系统是window,给容器中添加window.......");
        return new Person("window","58");
    }

    /**
     * bean在容器中的ID为james, IOC容器MAP,  map.put("liunx",value)
     * @return
     */
    @Conditional(LinCondition.class)
    @Bean("linux")
    public Person james(){
        System.out.println("当前系统是linux, 给容器中添加linux.......");
        return new Person("linux","20");
    }
}
