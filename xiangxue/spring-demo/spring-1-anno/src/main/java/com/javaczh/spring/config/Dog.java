package com.javaczh.spring.config;

/**
 * @ClassName Dog
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/22 20:22
 * @Version 1.0
 */
public class Dog {
    public Dog() {
        System.out.println("Dog 对象创建了");
    }
}
