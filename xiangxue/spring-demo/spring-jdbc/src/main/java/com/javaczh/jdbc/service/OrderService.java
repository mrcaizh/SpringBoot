package com.javaczh.jdbc.service;

import com.javaczh.jdbc.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName OrderService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/4 19:31
 * @Version 1.0
 */
@Service
public class OrderService {
    @Autowired
    private OrderDao orderDao;

    @Transactional
    public void addOrder() {
        orderDao.insert();
        int i = 1 / 0;
        System.out.println("插入成功");
    }
}
