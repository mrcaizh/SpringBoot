package com.javaczh.jdbc.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @ClassName DemoBeanFactoryPostProcessor
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/4 21:13
 * @Version 1.0
 */
@Component
public class DemoBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    /**
     *
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("MyBeanFactoryPostProcessor... postProcessBeanFactory .");
        //所有的bean定义已经保存加载到beanF actory,但是bean的实例还未创建
        int count = beanFactory.getBeanDefinitionCount();//拿bean数量
        String[] names = beanFactory.getBeanDefinitionNames();
        System.out.println("当前BeanFactory中有" + count + "个Bean");
        for (String name : names) {
            System.out.println(name);
        }

    }
}
