package com.javaczh.jdbc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @ClassName OrderDao
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/4 19:28
 * @Version 1.0
 */
@Repository
public class OrderDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insert() {
        String sql = "INSERT INTO  `order` (ordertime, ordermoney, orderstatus) VALUES(?,?,?)";
        jdbcTemplate.update(sql, new Date(), 20.0, 0);
    }
}
