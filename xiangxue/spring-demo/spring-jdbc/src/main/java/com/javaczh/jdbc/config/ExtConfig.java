package com.javaczh.jdbc.config;

import com.javaczh.jdbc.entity.Moon;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName ExtConfig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/4 20:59
 * @Version 1.0
 */
@Configuration
@ComponentScan("com.javaczh.jdbc")
public class ExtConfig {
    @Bean
    public Moon moon() {
        return new Moon();
    }
}
