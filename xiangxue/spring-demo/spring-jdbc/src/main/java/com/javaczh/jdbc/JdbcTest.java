package com.javaczh.jdbc;

import com.javaczh.jdbc.config.ExtConfig;
import com.javaczh.jdbc.config.SpringConfig;
import com.javaczh.jdbc.service.OrderService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @ClassName JdbcTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/4 19:34
 * @Version 1.0
 */

public class JdbcTest {

    @Test
    public void test(){
        ApplicationContext context=new AnnotationConfigApplicationContext(SpringConfig.class);
        OrderService orderService = context.getBean("orderService", OrderService.class);
        orderService.addOrder();
    }


    @Test
    public void test2(){
        AnnotationConfigApplicationContext app=new AnnotationConfigApplicationContext(ExtConfig.class);
       app.close();
    }
}
