package com.javaczh.jdbc.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * @ClassName SpringConfig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/4 19:20
 * @Version 1.0
 */
@Configuration
@ComponentScan("com.javaczh.jdbc")
@EnableTransactionManagement//开启事务管理功能，对@Transcationa1才起作用
public class SpringConfig {

    /**
     * 创建数据源
     *
     * @return
     */
    @Bean
    public DataSource dataSource() throws  Exception {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser("root");
        dataSource.setPassword("123456");
        dataSource.setJdbcUrl("jdbc:mysql://caizh.cn:3306/demo_01");
        dataSource.setDriverClass("com.mysql.jdbc.Driver");
        System.out.println(dataSource.getConnection());

        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


    @Bean
    public PlatformTransactionManager platformTransactionManager(DataSource dataSource) {
        ///需要加入数据源dataSource,管理数据源，才能控制数据源的每一条连接， 连接的提交，回滚都是由数据源操作的
        return new DataSourceTransactionManager(dataSource);
    }
}
