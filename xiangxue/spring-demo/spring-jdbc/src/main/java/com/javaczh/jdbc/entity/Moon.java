package com.javaczh.jdbc.entity;

/**
 * @ClassName Moon
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/4 21:00
 * @Version 1.0
 */
public class Moon {

    public Moon(){
        System.out.println("Moon constructor");
    }
}
