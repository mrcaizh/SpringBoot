package com.javaczh.servlet.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @ClassName OrderListener
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/5 15:41
 * @Version 1.0
 */
//@WebListener
public class OrderListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println(servletContextEvent.getServletContext());
        System.out.println("ServletContext 初始化了");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println(servletContextEvent.getServletContext());
        System.out.println("ServletContext 销毁了");
    }
}
