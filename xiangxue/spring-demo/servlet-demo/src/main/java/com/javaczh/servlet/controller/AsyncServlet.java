package com.javaczh.servlet.controller;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName AsyncServlet
 * @Description asyncSupported 支持异步处理
 * @Author CaiZiHao
 * @Date 2020/7/5 17:44
 * @Version 1.0
 */
@WebServlet(value = "/asyncServlet", asyncSupported = true)
public class AsyncServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("主线程开始..." + Thread.currentThread() + " start..." + System.currentTimeMillis());
        //开启异步模式
        AsyncContext startAsync = req.startAsync();

        //业务逻辑进行异步处理,开始异步处理
        startAsync.start(() -> {
            System.out.println("子线程开始..." + Thread.currentThread() + " start..." + System.currentTimeMillis());
            try {
                sayHel1o();
                // 异步业务处理结束,要调用complete通知容器
                startAsync.complete();
                //获取异步上下文
               // AsyncContext asyncContext = req.getAsyncContext();
                //获取响应
                ServletResponse response = startAsync.getResponse();
                response.getWriter().write("hello async");
                System.out.println("子线程结束..." + Thread.currentThread() + " end..." + System.currentTimeMillis());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        System.out.println("主程结束..." + Thread.currentThread() + " end..." + System.currentTimeMillis());
    }
    private void sayHel1o() throws InterruptedException {
        System.out.println(Thread.currentThread() + " processing...");
        //模拟业务处理，休眠3S
        TimeUnit.SECONDS.sleep(3);
    }
}
