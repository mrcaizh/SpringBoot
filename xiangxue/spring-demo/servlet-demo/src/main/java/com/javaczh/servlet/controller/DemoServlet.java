package com.javaczh.servlet.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName DemoServlet
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/5 17:25
 * @Version 1.0
 */
@WebServlet(name = "demo", urlPatterns = "/demoServlet")
public class DemoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println(Thread.currentThread() + " start...");
        try {
            sayHel1o();
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.getWriter().write("hello...");
        System.out.println(Thread.currentThread() + " end...");
    }

    private void sayHel1o() throws InterruptedException {
        System.out.println (Thread.currentThread() + " processing...");
        //模拟业务处理，休眠3S
        TimeUnit.SECONDS.sleep(3);
    }
}