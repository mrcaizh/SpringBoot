package com.javaczh.servlet.service;

import com.javaczh.servlet.controller.OrderServlet;
import com.javaczh.servlet.filter.OrderFilter;
import com.javaczh.servlet.listener.OrderListener;

import javax.servlet.*;
import javax.servlet.annotation.HandlesTypes;
import java.util.EnumSet;
import java.util.Set;

/**
 * @ClassName DemoServletContainerInitializer
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/5 15:12
 * @Version 1.0
 */
//容器启动的时候会将@HandlesTypes指定的这个类型下面的子类(实现类，子接口等)传递过来;
@HandlesTypes(DemoService.class)// 传入感兴趣的类型;
public class DemoServletContainerInitializer implements ServletContainerInitializer {
    /**
     * tomcat启动时加载应用的时候，会运行onStartup方法;
     * Set<Class<?>> arg0: 感兴趣的类型的所有子类型;
     * ServletContext arg1 :代表当前Web应用的ServletContext; 一个个Web应用一个ServletContext;
     * <p>
     * 1)、使用ServletContext注册Web组件(Servlet. Filter. Listener)
     * 2)、使用编码的方式，在项目启动的时候给ServletContext里面添加组件
     * 必须在项目启动的时候来添加;
     * 1)、ServletContainerInitializer得到的ServletContext;
     * 2)、ServletContextListener得到的ServletContext;
     */
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        //当传进来后，可以根据自己需要利用反射来创建对象等操作
        for (Class<?> aClass : set) {
            System.out.println(aClass);
        }
        //1. 注册过滤器
        FilterRegistration.Dynamic orderFilter = servletContext.addFilter("orderFilter", new OrderFilter());
        //添加Filter的映射信息，可以指定专门来拦截哪个servlet
        orderFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/");

        // 2. 注册servlet容器 参数1: servlet名称, 参数2:
        ServletRegistration.Dynamic servlet = servletContext.addServlet("orderServlet", new OrderServlet());
        //servlet的映射路径
        servlet.addMapping("/orderServlet");
        //3. 注册监听器
        servletContext.addListener(new OrderListener());
    }
}
