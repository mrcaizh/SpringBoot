package com.javaczh.thread.demo1;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName VolatileUnsafe
 * @Description 演示violate无法提供操作的原子性
 * @Author CaiZiHao
 * @Date 2020/5/27 10:55
 * @Version 1.0
 */
public class VolatileUnsafe {
    private static class VolatileVar implements Runnable {
        private volatile int a = 0;

        @Override
        public void run() {

                String threadName = Thread.currentThread().getName();
                a = a++;
                try {
                    TimeUnit.MILLISECONDS.sleep(3);
                } catch (InterruptedException e) {
                }
                System.out.println(threadName + ":======" + a);
                a = a + 1;
                System.out.println(threadName + ":======" + a);


        }
    }

    public static void main(String[] args) {
        VolatileVar v = new VolatileVar();
        Thread t1 = new Thread(v);
        Thread t2 = new Thread(v);
        Thread t3 = new Thread(v);
        Thread t4 = new Thread(v);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
