package com.javaczh.thread.demo1;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * @ClassName OnlyMain
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/25 22:59
 * @Version 1.0
 */
public class OnlyMain {
    public static void main(String[] args) {
        //虚拟机线程管理的接口
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        ThreadInfo[] threadInfos =
                threadMXBean.dumpAllThreads(false, false);
        for (ThreadInfo threadInfo : threadInfos) {
            System.out.println(" [" + threadInfo.getThreadId() + "]" + " " + threadInfo.getThreadName());


        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}