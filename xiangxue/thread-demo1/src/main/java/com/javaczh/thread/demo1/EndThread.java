package com.javaczh.thread.demo1;

/**
 * @ClassName EndThread
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/26 21:40
 * @Version 1.0
 */
public class EndThread {

    private static class UseThread extends Thread{
        public UseThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            //获取当前线程的名字
            String threadName = Thread.currentThread().getName();
            //while(!isInterrupted()) {
            while(true) {
                System.out.println(threadName+" is run!");
            }
            //System.out.println(threadName+" interrput flag is "+isInterrupted());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread endThread = new UseThread("endThread");
        //启动线程
        endThread.start();
        //线程睡眠
        Thread.sleep(20);
        //中断线程
        endThread.interrupt();

    }
}
