package com.javaczh.thread.demo1.pool;

import java.sql.Connection;
import java.util.LinkedList;

/**
 * @ClassName DBPool
 * @Description 实现一个数据库的连接池
 * @Author CaiZiHao
 * @Date 2020/5/31 22:07
 * @Version 1.0
 */
public class DBPool {
    //数据库池的容器
    private static LinkedList<Connection> pool = new LinkedList<>();

    public DBPool(int initialSize) {
        if (initialSize > 0) {
            for (int i = 0; i < initialSize; i++) {
                pool.addLast(SqlConnectImpl.fetchConnection());
            }
        }
    }

    /**
     * 在mills时间内还拿不到数据库连接，返回一个null
     *
     * @param mills
     * @return
     * @throws InterruptedException
     */
    public Connection fetchConn(long mills) throws InterruptedException {
        synchronized (pool) {
            if (mills < 0) {
                //连接池如果为空就等待
                while (pool.isEmpty()) {
                    pool.wait();
                }
                return pool.removeFirst();
            } else {
                //超时时间
                long overTime = System.currentTimeMillis() + mills;
                long remain = mills;
                while (pool.isEmpty() && remain > 0) {
                    pool.wait(mills);
                    remain = overTime - System.currentTimeMillis();
                }
                //如果等待超时返回null,否则从连接池获取一个连接对象
                Connection result = null;
                if (!pool.isEmpty()) {
                    result = pool.removeFirst();
                }
                return result;
            }


        }

    }
    //放回数据库连接
    public void releaseConn(Connection conn) {
        if(conn!=null) {
            synchronized (pool) {
                pool.addLast(conn);
                pool.notifyAll();
            }
        }
    }
}
