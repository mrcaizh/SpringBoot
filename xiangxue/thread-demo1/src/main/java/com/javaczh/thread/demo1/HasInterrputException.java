package com.javaczh.thread.demo1;

/**
 * @ClassName HasInterrputException
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/26 21:59
 * @Version 1.0
 */
public class HasInterrputException {

    private static class UseThread extends Thread {
        private volatile boolean cancel = false;

        public void cancel() {
            cancel = true;
        }

        public UseThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            while (cancel||isInterrupted()) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    System.out.println(threadName + " catch interrput flag is "
                            + isInterrupted());
                    interrupt();
                    e.printStackTrace();
                }
                System.out.println(threadName);
            }
            System.out.println(threadName + " interrput flag is " + isInterrupted());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread endThread = new UseThread("HasInterrputEx");
        endThread.start();
        Thread.sleep(800);
        endThread.interrupt();
    }
}
