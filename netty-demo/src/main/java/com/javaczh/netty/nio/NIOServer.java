package com.javaczh.netty.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

/**
 * @ClassName NIOServer
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/17 16:44
 * @Version 1.0
 */
public class NIOServer {
    public static void main(String[] args) throws IOException {
        //1. 得到一个 ServerSocketChannel 对象 老大
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //2. 得到一个 Selector 对象 间谍
        Selector selector = Selector.open();
        //3. 绑定一个端口号
        serverSocketChannel.bind(new InetSocketAddress(9999));
        //4. 设置非阻塞
        serverSocketChannel.configureBlocking(false);
        //5. 把 ServerSocketChannel 对象注册给 Selctor 对象
        //int OP_ACEPT：有新的网络连接可以 acept，值为 16
        //int OP_CONECT：代表连接已经建立，值为 8
        //int OP_READ 和 int OP_WRITE：代表了读、写操作，值为 1 和 4
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        //6 开始
        while (true) {
            //6.1 监控客户端
            //selct(long timeout)，监控所有注册的通道，当其中有 IO 操作可以进行时，
            // 将对应的 SelectionKey 加入到内部集合中并返回，参数用来设置超时间
            //nio 非阻塞的优势
            if (selector.select(2000) == 0) {
                System.out.println("Server : 没有客户端连接, 可以处理别的逻辑 ");
                continue;
            }
            //6.2 得到 SelectionKey, 判断通道里面的事件
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                /**
                 * 	  public abstract Selector selector()，得到与之关联的Selector 对象
                 *    public abstract SelectableChannel channel()，得到与之关联的通道
                 * 	 public final Object attachment()，得到与之关联的共享数据
                 * 	  public abstract SelectionKey interestOps(int ops)，设置或改变监听事件
                 * 	  public final boolean isAcceptable()，是否可以accept
                 * 	  public final boolean isReadable()，是否可以读
                 *   public final boolean isWritable()，是否可以写
                 */
                SelectionKey selectionKey = iterator.next();
                //客户端连接请求事件
                if (selectionKey.isAcceptable()) {
                    System.out.println("客户端连接请求事件:  OP_ACCEPT");
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    //设置客户端为非阻塞
                    socketChannel.configureBlocking(false);
                    //将客户端也注册进selector
                    socketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(1024));
                }
                //读取客户端数据事件
                if (selectionKey.isReadable()) {
                    SocketChannel channel = (SocketChannel) selectionKey.channel();
                    ByteBuffer attachment = (ByteBuffer) selectionKey.attachment();
                    channel.read(attachment);
                    System.out.println("客户端发来数据：" + new String(attachment.array()));
                }
                if (selectionKey.isWritable()) {

                }
                //6.3 手动从集合中移除当前key,防止重复处理
                iterator.remove();
            }
        }
    }
}
