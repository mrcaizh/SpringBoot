package com.javaczh.netty.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @ClassName NIOClient
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/18 8:53
 * @Version 1.0
 */
public class NIOClient {
    public static void main(String[] args) throws IOException {
        //1.得到网络通道
        SocketChannel socketChannel = SocketChannel.open();
        ///2. 设置非阻塞方式
        socketChannel.configureBlocking(false);
        //3. 设置服务端的IP 和端口进行连接
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 9999);
        if (!socketChannel.connect(inetSocketAddress)) {
            //nio 作为非阻塞式的优势
            while (!socketChannel.finishConnect()) {
                System.out.println("没有连接上服务端: 继续处理别的逻辑");
            }
        }
        ///5. 得到一个缓冲区并存入数据
        String message="测试数据";
        ByteBuffer byteBuffer=ByteBuffer.wrap(message.getBytes());
      //  byteBuffer.put(message.getBytes());
        //6. 通过通道将消息发送给服务端
        socketChannel.write(byteBuffer);

        System.in.read();
    }
}
