package com.javaczh.netty.nio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * @ClassName FileChanelDemo
 * @Description NIO 中的通道是从输出流对象里通过 getChanel 方法获取到的，该通道是双向的，既可
 * 以读，又可以写。在往通道里写数据之前，必须通过 put 方法把数据存到 ByteBufer 中，然
 * 后通过通道的 write 方法写数据。在 write 之前，需要调用 flip 方法翻转缓冲区，把内部重置
 * 到初始位置，这样在接下来写数据时才能把所有数据写到通道里
 * @Author CaiZiHao
 * @Date 2020/6/17 16:14
 * @Version 1.0
 */
public class FileChanelDemo {
    public static void main(String[] args) throws  Exception {
        //读取文件
        String filename = "FileChanelDemo1.txt";
        FileInputStream inputStream = new FileInputStream(filename);
        //初始化固定大小的缓冲区
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024 * 2);
        FileChannel channel = inputStream.getChannel();
        channel.read(byteBuffer);
        byte[] bytes = byteBuffer.array();
        System.out.println("通过FileChannel 读取数据: "+new String(bytes, Charset.forName("UTF-8")));


    }

    private static void write() throws IOException {
        String str = "测试 FileChanel 的读写操作";
        String filename = "FileChanelDemo1.txt";
        FileOutputStream out = new FileOutputStream(filename);
        //初始化固定大小的缓冲区
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024 * 2);
        //将数据添加到缓冲区
        byteBuffer.put(str.getBytes());
        byteBuffer.flip();
        //获取文件通道
        FileChannel channel = out.getChannel();
        //将缓冲区的数据通过文件通道写到本地
        channel.write(byteBuffer);
        channel.close();
        out.close();
    }
}
