package com.javaczh.netty.nio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * @ClassName VideoCopy
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/17 16:26
 * @Version 1.0
 */
public class VideoCopy {


    public static void main(String[] args) throws Exception {
        String fromPath = "D:\\Java\\精通Spring Boot Cloud\\1-20\\02_Spring Boot与Cloud整体介绍.mp4";
        String toPach = "F:\\nioCopy.mp4";
        FileInputStream in = new FileInputStream(fromPath);
        FileOutputStream out = new FileOutputStream(toPach);
        FileChannel inChannel = in.getChannel();
        FileChannel outChannel = out.getChannel();
        /**
         * public transferFom(ReadbleByteChanel src, long positon, long count)，从目标通道
         * 中复制数据到当前通道
         *  public transferTo(lng positon, long count, WritableByteChanel target)，把数据从当
         * 前通道复制给目标通道
         */
        outChannel.transferFrom(inChannel,0,inChannel.size());
    }

    private static void bioCopy() throws IOException {
        String fromPath = "D:\\Java\\精通Spring Boot Cloud\\1-20\\02_Spring Boot与Cloud整体介绍.mp4";
        String toPach = "F:\\Boot与Cloud.mp4";
        byte[] bytes = new byte[1024 * 2];

        FileInputStream in = new FileInputStream(fromPath);
        FileOutputStream out = new FileOutputStream(toPach);

        while (true) {
            int read = in.read(bytes);
            if (read == -1) {
                break;
            }
            out.write(bytes, 0, read);
        }

        in.close();
        out.close();
    }


}
