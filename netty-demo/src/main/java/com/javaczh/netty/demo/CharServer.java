package com.javaczh.netty.demo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * @ClassName CharServer
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/18 9:23
 * @Version 1.0
 */
public class CharServer {
    private final int PROT = 9999;
    //选择器
    private Selector selector;
    //服务器对象
    private ServerSocketChannel serverSocketChannel;

    public CharServer() {
        try {
            //得到选择器对象
            selector = Selector.open();
            //得到服务器对象
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(PROT));
            //设置非阻塞
            serverSocketChannel.configureBlocking(false);
            //将服务器注册进选择器中
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("服务器已连接!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() throws IOException {
        //不停轮询获取客户端连接
        while (true) {
            if (selector.select(2000) == 0) {
               // System.out.println("Server : 没有客户端连接, 可以处理别的逻辑 ");
                continue;
            }
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                //客户端连接事件
                if (key.isAcceptable()) {
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    socketChannel.configureBlocking(false);
                    socketChannel.register(selector, SelectionKey.OP_READ);
                    System.out.println(socketChannel.getRemoteAddress().toString().substring(1) + "上线了");
                    //将对应的channel 设置为accept, 接着准备接受其他客户端的 请求
                    key.interestOps(SelectionKey.OP_ACCEPT);
                }
                //监听到 read
                if (key.isReadable()) {
                    readMsg(key); //读取客户端发来的数据
                }
                //一定要把当前 key 删掉，防止重复处理

            }


        }

    }

    private void readMsg(SelectionKey key) {
        SocketChannel channel = null;
        try {
            //得到socketChannel通道
            channel = (SocketChannel) key.channel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            //从通道中读取数据并存储到缓冲区中
            int count = channel.read(byteBuffer);
            if (count > 0) {
                String message = new String(byteBuffer.array(), "UTF-8");
                //输出客户端发送过来的数据
                printLog(message);
                //将关联的 chanel 设置为 read，继续准备接受数据
                //key.interestOps(SelectionKey.OP_READ);
            boradCast(channel, message); //向所有客户端广播数据
        }
            byteBuffer.clear();
        } catch (IOException e) {
            //当客户端关闭 chanel 时，进行异常处理
            try {
                printLog(channel.getRemoteAddress().toString().substring(1) + " 下线了");
                key.cancel();//取消注册
                channel.close();// 关闭通道
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    public void boradCast(SocketChannel socketChannel, String message) throws IOException {
        System.out.println("发送广播....................");
        //广播数据到所有 SocketChannel 中
        for (SelectionKey key : selector.selectedKeys()) {
            Channel targetChannel = key.channel();
            //排除本身通道
            if (targetChannel instanceof SocketChannel && targetChannel != socketChannel) {
                SocketChannel dest = (SocketChannel) targetChannel;
                //把数据存储到缓冲区
                ByteBuffer byteBuffer = ByteBuffer.wrap(message.getBytes());
                //往通道写数据
                dest.write(byteBuffer);
            }
        }
    }

    public void printLog(String message) {
        System.out.println("[" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "] --> "
                + message);
    }
}
