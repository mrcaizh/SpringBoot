package com.javaczh.netty.demo;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName CharTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/18 10:50
 * @Version 1.0
 */
public class CharTest {
    public static void main(String[] args) throws IOException {



        ChatClient chatClient = new ChatClient();

        new Thread(() -> {
            try {
                chatClient.receiveMsg();
                TimeUnit.SECONDS.sleep(3);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }).start();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String next = scanner.nextLine();
            chatClient.sendMessage(next);
        }

    }
}
