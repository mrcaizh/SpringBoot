package com.javaczh.netty.demo;

import java.io.IOException;

/**
 * @ClassName CharServerTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/18 10:56
 * @Version 1.0
 */
public class CharServerTest {
    public static void main(String[] args) throws IOException {
        new CharServer().start();
    }
}
