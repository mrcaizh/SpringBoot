package com.javaczh.netty.demo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * @ClassName ChatClient
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/18 10:29
 * @Version 1.0
 */
public class ChatClient {

    private final InetSocketAddress socketAddress = new InetSocketAddress("127.0.0.1", 9999);

    private Selector selector;

    private SocketChannel socketChannel;

    private String clientAddress;

    public ChatClient() throws IOException {
        socketChannel = SocketChannel.open(socketAddress);
      //  selector=Selector.open();
        //设置非阻塞
        socketChannel.configureBlocking(false);
        //注册选择器并设置为 read
     //   socketChannel.register(selector, SelectionKey.OP_READ);
        //得到客户端 IP 地址和端口信息，作为聊天用户名使用
        clientAddress = socketChannel.getLocalAddress().toString().substring(1);
        System.out.println("---Client(" + clientAddress + ") is ready---");
    }

    public void sendMessage(String message) throws IOException {
        ///如果控制台输入 bye 就关闭通道，结束聊天
        if ("bye".equalsIgnoreCase(message)) {
            socketChannel.close();
            return;
        }
        message = new StringBuilder(clientAddress).append("说:").append(message)
                .toString();
        //往通道写数据
        socketChannel.write(ByteBuffer.wrap(message.getBytes()));
    }


    //从服务器端接收数据

    public void receiveMsg() throws  Exception{
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int size=socketChannel.read(buffer);
        if(size>0){
            String msg=new String(buffer.array());
            System.out.println(msg.trim());
        }
    }
    /*public void receiveMsg() throws IOException {
        int count = selector.select(2000);
        if (count > 0) {
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                if (key.isReadable()) {
                    //得到通道
                    SocketChannel channel = (SocketChannel) key.channel();
                    ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                    channel.read(byteBuffer);
                    String mes = new String(byteBuffer.array(), "UTF-8");
                    System.out.println(mes);
                }
                iterator.remove(); //删除当前 SelctionKey，防止重复处理
            }
        } else {
            System.out.println("没人连接");
        }
    }*/

}
