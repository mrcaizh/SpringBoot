package com.javaczh.netty.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;

/**
 * @ClassName NettyClient
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/18 13:36
 * @Version 1.0
 */
public class NettyClient {
    public static void main(String[] args) throws InterruptedException {

        //1. 创建一个线程组：接收客户端连接
        EventLoopGroup group = new NioEventLoopGroup();

        //2. 创建客户端启动助手来配置参数
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group).channel(NioSocketChannel.class)// 使用NioSocketChannel 作为客户端通道的实现
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline().addLast(new NettyClientHandler());
                    }
                });
        System.out.println("......Client is ready......");
        //启动客户端,等待连接上服务器端(非阻塞)
        ChannelFuture cf = bootstrap.connect(new InetSocketAddress("127.0.0.1", 9999)).sync();
        //等待连接关闭 非阻塞
        cf.channel().closeFuture().sync();
    }
}
