package com.javaczh.netty.bio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * @ClassName TCPClient
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/17 15:57
 * @Version 1.0
 */
public class TCPClient {
    public static void main(String[] args) throws IOException {
        //1.创建socket
        Socket socket = new Socket("127.0.0.1", 9999);
        //2.从连接中取出输出流并发消息
        OutputStream outputStream = socket.getOutputStream();
        String s = "TCPClient";
        outputStream.write(s.getBytes());
        //3. 获取服务端响应的数据
        InputStream inputStream = socket.getInputStream();
        byte[] bytes = new byte[1024 * 2];
        inputStream.read(bytes);
        String s1 = new String(bytes, Charset.forName("UTF-8"));
        System.out.println(s1);
    }
}
