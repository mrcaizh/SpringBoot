package com.javaczh.netty.bio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName TCPServer
 * @Description 建立网络连接的时候只能采用 BIO，需要先在服务端启动一个
 * ServerSocket，然后在客户端启动 Socket 来对服务端进行通信，默认情况下服务端需要对每
 * 个请求建立一个线程等待请求，而客户端发送请求后，先咨询服务端是否有线程响应，如果
 * 没有则会一直等待或者遭到拒绝，如果有的话，客户端线程会等待请求结束后才继续执行，这就是阻塞式 IO
 * @Author CaiZiHao
 * @Date 2020/6/17 15:47
 * @Version 1.0
 */
public class TCPServer {

    public static void main(String[] args) throws IOException {
        //1.创建服务器端程序，绑定端口号 9999，
        ServerSocket serverSocket = new ServerSocket(9999);
        while (true) {
            //2. 等待客户端连接, accept 方法用来监听客户端连接，如果没有客户端连接，就一直等待，程序会阻塞到这里。
            Socket accept = serverSocket.accept();
            //3.从连接中取出输入流来接收消息, 阻塞
            InputStream inputStream = accept.getInputStream();
            byte[] bytes = new byte[1024 * 2];
            //读取客户端发送过来的数据
            inputStream.read(bytes);
            //获取客户端的ip 地址
            String hostAddress = accept.getInetAddress().getHostAddress();
            System.out.println("接收客户端( " + hostAddress + ")发送的数据: " + new String(bytes, "UTF-8"));
            OutputStream outputStream = accept.getOutputStream();
            //响应客户端
            String str = new String("服务端响应数据: 收到");
            outputStream.write(str.getBytes());
            accept.close();
        }
    }
}
