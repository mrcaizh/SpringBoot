package com.javaczh.security.service.Impl;


import com.alibaba.fastjson.JSONObject;

import com.javaczh.security.contants.Constant;
import com.javaczh.security.entity.mapstruct.UserMapStruct;
import com.javaczh.security.entity.model.SysPermission;
import com.javaczh.security.entity.model.SysRole;
import com.javaczh.security.entity.model.SysUser;
import com.javaczh.security.entity.vo.UserVO;
import com.javaczh.security.exception.BusinessException;
import com.javaczh.security.exception.code.BaseResponseCode;
import com.javaczh.security.mapper.SysRoleMapper;
import com.javaczh.security.mapper.SysRolePermissionMapper;
import com.javaczh.security.mapper.SysUserMapper;
import com.javaczh.security.mapper.SysUserRoleMapper;
import com.javaczh.security.service.UserService;
import com.javaczh.security.utils.JwtTokenUtils;

import com.javaczh.security.utils.id.IdWorker;
import com.javaczh.security.utils.redis.RedisService;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName UserServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/10 12:40
 * @Version 1.0
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RedisService redisService;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = sysUserMapper.loadUserByUsername(username);
        if (null == user) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_ERROR);
        }
        System.out.println(passwordEncoder.encode("123456"));
        user.setRoles(sysUserMapper.getUserRolesByUserId(user.getId()));
        //user.setRoles()
        /*  User user = userMapper.loadUserByUsername(username);
        if (null == user) {
            throw new UsernameNotFoundException("用户不存在");
        }
        user.setRoles(userMapper.getUserRolesByUid(user.getId()));*/
        return user;
    }

    @Override
    public UserVO generatorToken(SysUser sysUser) {
        UserVO vo = UserMapStruct.INSTANCE.modelToVo(sysUser);
        //进行token签发
        Map<String, Object> claims = new HashMap<>(3);
        claims.put(Constant.ROLES_INFOS_KEY, getRolesByUserId(sysUser.getId()));
        claims.put(Constant.PERMISSIONS_INFOS_KEY, getPermissionsByUserId(sysUser.getId()));
        claims.put(Constant.JWT_USER_NAME, sysUser.getUsername());
        claims.put(Constant.JWT_USER_PWD, sysUser.getPassword());
        claims.put(Constant.JWT_USER_ROLE_LIST, JSONObject.toJSONString(sysUser.getRoles()));
        claims.put(Constant.JWT_USER_INFO_KEY, JSONObject.toJSONString(vo));
        //根据用户id生成认证token
        String accessToken = JwtTokenUtils.getAccessToken(sysUser.getId(), claims);
        //根据用户id生成刷新token,app端生成的token的失效长
        String refreshToken;
        refreshToken = JwtTokenUtils.getRefreshToken(sysUser.getId(), claims);
        /**   
         * 异地登录失效逻辑     
         */
        redisService.set(sysUser.getId(), accessToken, 60, TimeUnit.MINUTES);
        vo.setAccessToken(accessToken);
        vo.setRefreshToken(refreshToken);
        return vo;
    }

    @Override
    public SysUser selectSysUserByPrimaryKey(String userId) {
        return sysUserMapper.selectByPrimaryKey(userId);
    }

    /**
     * 主要是清空  一些缓存信息，然后就是我们系统具体业务了 把 access_token 加入黑名单、refresh_token 加入黑名单。
     *
     * @param request
     * @param response
     * @param authentication
     */
    @Override
    public void logoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        String refreshToken = request.getHeader(Constant.REFRESH_TOKEN);
        if (StringUtils.isEmpty(accessToken) || StringUtils.isEmpty(refreshToken)) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        //获取主体
        String userId = JwtTokenUtils.getUserId(accessToken);
        /**
         * 把token 加入黑名单 禁止再登录
         */
        redisService.set(Constant.JWT_ACCESS_TOKEN_BLACKLIST + accessToken, userId, JwtTokenUtils.getRemainingTime(accessToken),
                TimeUnit.MILLISECONDS);
        //把 refreshToken 加入黑名单 禁止再拿来刷新token
        redisService.set(Constant.JWT_REFRESH_TOKEN_BLACKLIST + refreshToken,
                userId, JwtTokenUtils.getRemainingTime(refreshToken), TimeUnit.MILLISECONDS);
    }

    /**
     * jwt 自动刷新
     * jwt 刷新有两种情况要考虑？
     * 1.一种是管理员修改了该用户的角色/权限(需要主动去刷新)。
     * 2.另一种是 jwt 过期要刷新。
     * 3.刷新成功后生成新的token自动再刷新当前请求接口
     *
     * @param refreshToken
     * @return
     */
    @Override
    public String refreshToken(String refreshToken) {
        //它是否过期
        //它是否被加如了黑名
        if (!JwtTokenUtils.validateToken(refreshToken) || redisService.hasKey(Constant.JWT_REFRESH_TOKEN_BLACKLIST + refreshToken)) {
            throw new BusinessException(BaseResponseCode.TOKEN_ERROR);
        }
        String userId = JwtTokenUtils.getUserId(refreshToken);
        String username = JwtTokenUtils.getUserName(refreshToken);
        Claims claimsFromToken = JwtTokenUtils.getClaimsFromToken(refreshToken);
        log.info("userId={}", userId);
        Map<String, Object> claims = new HashMap<>();
        List<SysRole> roles = sysUserMapper.getUserRolesByUserId(userId);
        claims.put(Constant.ROLES_INFOS_KEY, userId);
        claims.put(Constant.PERMISSIONS_INFOS_KEY, userId);
        claims.put(Constant.JWT_USER_NAME, username);
        claims.put(Constant.JWT_USER_PWD, claimsFromToken.get(Constant.JWT_USER_PWD));
        claims.put(Constant.JWT_USER_ROLE_LIST, JSONObject.toJSONString(roles));
        claims.put(Constant.JWT_USER_INFO_KEY, claimsFromToken.get(Constant.JWT_USER_INFO_KEY));
        String newAccessToken = JwtTokenUtils.getAccessToken(userId, claims);
        return newAccessToken;
    }

    /**
     * mock 数据
     * 通过用户id获取该用户所拥有的角色
     * 后期修改为通过操作DB获取
     *
     * @return java.util.List<java.lang.String>
     * @throws
     */
    private List<String> getRolesByUserId(String userId) {
        List<String> roles = sysUserRoleMapper.selectRoleNamesByUserId(userId);
        return roles;
    }

    /**
     * mock 数据
     * 通过用户id获取该用户所拥有的角色
     * 后期通过操作数据获取
     *
     * @param userId
     * @return java.util.List<java.lang.String>
     * @throws
     */
    private List<String> getPermissionsByUserId(String userId) {
        List<SysPermission> list = sysRolePermissionMapper.selectPermissionUrlByUserId(userId);
        List<String> permissions = new ArrayList<>(3);
        for (SysPermission sysPermission : list) {
            permissions.add(sysPermission.getUrl());
        }
        return permissions;
    }


    @Override
    @Transactional
    public void update() {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey("126844864776333312023");
        System.out.println(sysUser);
        modified(sysUser);
        RestTemplate restTemplate=new RestTemplate();
        String forObject = restTemplate.getForObject("http://localhost:8091/user/select", String.class);
        System.out.println();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void modified(SysUser sysUser) {
        sysUser.setRealName("6666666666666");
        sysUserMapper.updateByPrimaryKeySelective(sysUser);
    }

    @Override
    public void select() {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey("126844864776333312023");
        System.out.println("select:  "+sysUser);
    }
}
