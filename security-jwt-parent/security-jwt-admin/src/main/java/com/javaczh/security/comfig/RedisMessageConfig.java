package com.javaczh.security.comfig;

import com.javaczh.security.queue.consumer.RedisConsumer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * @ClassName RedisMessageConfig
 * @Description Redis 配置消息通道
 * @Author CaiZiHao
 * @Date 2020/6/15 11:24
 * @Version 1.0
 */
@Configuration
public class RedisMessageConfig {


    /**
     *  
     * <p>
     * 连接工程我们使用Spring Boot默认的RedisConnectionFactory
     * 我们将在listenerAdapter方法中定义的Bean注册为一个消息监听者，它将监听receiveMessage和updateMenusCase两个主题的消息。
     * 因为RedisConsumer类是一个POJO，要将它包装在一个消息监听者适配器（实现了MessageListener接口），
     * 这样才能被监听者容器RedisMessageListenerContainer的addMessageListener方法添加到连接工厂中。
     * 有了这个适配器，当一个消息到达时，receiveMessage()和updateMenusCase方法进行响应。
     *
     * @param connectionFactory
     * @param receiveMessageListenerAdapter
     * @param updateMenusCaseListenerAdapter
     * @return
     */
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                            MessageListenerAdapter receiveMessageListenerAdapter,
                                            MessageListenerAdapter updateMenusCaseListenerAdapter
    ) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(receiveMessageListenerAdapter, new PatternTopic("receiveMessage"));
        container.addMessageListener(updateMenusCaseListenerAdapter, new PatternTopic("updateMenusCase"));
        container.addMessageListener(updateMenusCaseListenerAdapter, new PatternTopic("updateMenusCase"));
        return container;
    }

    @Bean
    MessageListenerAdapter receiveMessageListenerAdapter(RedisConsumer redisConsumer) {
        return new MessageListenerAdapter(redisConsumer, "receiveMessage");
    }

    @Bean
    MessageListenerAdapter updateMenusCaseListenerAdapter(RedisConsumer redisConsumer) {
        return new MessageListenerAdapter(redisConsumer, "updateMenusCase");
    }
}
