package com.javaczh.security.authentication;

import com.javaczh.security.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName CustomLoginFailureHandler
 * @Description 登录失败的处理类
 * @Author CaiZiHao
 * @Date 2020/6/15 9:23
 * @Version 1.0
 */
@Slf4j
public class CustomLoginFailureHandler implements AuthenticationFailureHandler {
    /**
     * 登录失败的处理逻辑，和登录成功类似，不同的是，登录失败的回调方法里有一个AuthenticationException参数 ，通过这个异常参数
     * 可以获取登录失败的原因，进而给用户一个明确的提示
     *
     * @param request
     * @param response
     * @param e
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        int status = 401;
        String errorMessage;
        if (e instanceof LockedException) {
            errorMessage = "账户被锁定,登录失败!";
        } else if (e instanceof BadCredentialsException) {
            errorMessage = "账户名或密码输入错误,登录失败!";
            log.warn("登录失败原因: {}", errorMessage);
        } else if (e instanceof DisabledException) {
            errorMessage = "账户被禁用,登录失败!";
        } else if (e instanceof AccountExpiredException) {
            errorMessage = "账户已过期,登录失败!";
        } else if (e instanceof CredentialsExpiredException) {
            errorMessage = "密码已过期,登录失败!";
        } else {
            errorMessage = "登录失败!";
        }
        log.warn("登录失败原因: {}", errorMessage);
        ResponseUtils.simpleResponseMessage(response, status, errorMessage);
    }
}
