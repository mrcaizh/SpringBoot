package com.javaczh.security.authentication;

import com.alibaba.fastjson.JSONObject;
import com.javaczh.security.contants.Constant;
import com.javaczh.security.entity.model.SysRole;
import com.javaczh.security.entity.model.SysUser;
import com.javaczh.security.exception.BusinessException;
import com.javaczh.security.exception.code.BaseResponseCode;
import com.javaczh.security.utils.JwtTokenUtils;
import com.javaczh.security.utils.ResponseUtils;
import com.javaczh.security.utils.redis.RedisService;
import com.javaczh.security.utils.token.JsonUtils;
import io.jsonwebtoken.Claims;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName TokenVerifyFilter
 * @Description token 认证过滤器
 * @Author CaiZiHao
 * @Date 2020/6/12 16:59
 * @Version 1.0
 */
public class TokenVerifyFilter extends BasicAuthenticationFilter {

    private RedisService redisService;


    public TokenVerifyFilter(AuthenticationManager authenticationManager, RedisService redisService) {
        super(authenticationManager);
        this.redisService = redisService;
    }

    /**
     * 验证用户携带的token是否合法，并解析出用户信息，交给SpringSecurity
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        //1. 从请求头获取 认证token
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        if (StringUtils.isEmpty(accessToken)) {
            ResponseUtils.simpleResponseMessage(response, BaseResponseCode.TOKEN_NOT_NULL);
        } else {
            //2. 对token进校验
            if (tokenVerify(accessToken)) {
                //3. token 校验成功,解析token获取相关数据
                Claims claims = JwtTokenUtils.getClaimsFromToken(accessToken);
                String username = (String) claims.get(Constant.JWT_USER_NAME);
                String password = (String) claims.get(Constant.JWT_USER_PWD);
                if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
                    String roleJson = (String) claims.get(Constant.JWT_USER_ROLE_LIST);
                    List<SysRole> roles = JSONObject.parseArray(roleJson, SysRole.class);
                    List<SimpleGrantedAuthority> authorities = new ArrayList<>(5);
                    for (SysRole role : roles) {
                        authorities.add(new SimpleGrantedAuthority(role.getName()));
                    }
                    UsernamePasswordAuthenticationToken authResult = new UsernamePasswordAuthenticationToken(password, null, authorities);
                    SecurityContextHolder.getContext().setAuthentication(authResult);
                    chain.doFilter(request, response);
                }
            }
        }
    }

    /**
     * 校验
     *
     * @param accessToken
     * @return
     */
    private boolean tokenVerify(String accessToken) {
        String userId = JwtTokenUtils.getUserId(accessToken);
        /**
         * 判断用户是否被锁定
         */
        if (redisService.hasKey(Constant.ACCOUNT_LOCK_KEY.concat(userId))) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_LOCK);
        }
        /**
         * 判断用户是否被删除
         */
        if (redisService.hasKey(Constant.DELETED_USER_KEY.concat(userId))) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_HAS_DELETED_ERROR);
        }

        /**
         * 判断token 是否主动登出
         */
        if (redisService.hasKey(Constant.JWT_REFRESH_TOKEN_BLACKLIST.concat(accessToken))) {
            throw new BusinessException(BaseResponseCode.TOKEN_ERROR);
        }
        /**
         * 判断token是否通过校验
         */
        if (!JwtTokenUtils.validateToken(accessToken)) {
            throw new BusinessException(BaseResponseCode.TOKEN_PAST_DUE);
        }
        /**
         * 判断这个登录用户是否要主动去刷新
         *
         * 如果 key=Constant.JWT_REFRESH_KEY+userId大于accessToken说明是在 accessToken不是重新生成的
         * 这样就要判断它是否刷新过了/或者是否是新生成的token
         */
        if (redisService.hasKey(Constant.JWT_REFRESH_KEY.concat(accessToken)) &&
                redisService.getExpire(Constant.JWT_REFRESH_KEY.concat(userId), TimeUnit.MILLISECONDS)
                        > JwtTokenUtils.getRemainingTime(accessToken)) {
            /**
             * 是否存在刷新的标识
             */
            if (!redisService.hasKey(Constant.JWT_REFRESH_IDENTIFICATION.concat(accessToken))) {
                throw new BusinessException(BaseResponseCode.TOKEN_PAST_DUE);
            }
        }

        /**
         * 判断redis是否存在该用户的token,并且是否一致,不一致则提醒异地登录

         if (redisService.hasKey(userId)&&!accessToken.equals(redisService.get(userId))){
         throw new BusinessException(BaseResponseCode.YIDI_LOGIN_ERROR);
         }
         */
        return true;
    }

}
