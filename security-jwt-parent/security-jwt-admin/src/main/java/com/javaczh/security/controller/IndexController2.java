package com.javaczh.security.controller;

import com.javaczh.security.service.Impl.UserServiceImpl;
import com.javaczh.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * @ClassName IndexController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/21 17:40
 * @Version 1.0
 */
@RestController
public class IndexController2 {


    @RequestMapping("/admin/hello")
    public String admin() {
        return "Hello  admin";
    }

    @RequestMapping("/user/hello")
    public String user(HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        String fromPath = "D:\\Java\\精通Spring Boot Cloud\\1-20\\02_Spring Boot与Cloud整体介绍.mp4";
        String toPach = "F:\\nioCopy.mp4";
        FileInputStream in = new FileInputStream(fromPath);
        FileChannel inChannel = in.getChannel();

        /**
         * public transferFom(ReadbleByteChanel src, long positon, long count)，从目标通道
         * 中复制数据到当前通道
         *  public transferTo(lng positon, long count, WritableByteChanel target)，把数据从当
         * 前通道复制给目标通道
         */

        return "Hello  user";
    }

    @RequestMapping("/db/hello")
    public String db() {
        return "Hello  db";
    }

    @GetMapping("hello")
    public String hello() {
        return "Hello  World  Security";
    }

    @GetMapping("/role/addOrUpdateRole")
    public String addOrUpdateRole() {
        return "/role/addOrUpdateRole";
    }
    @GetMapping("/user/batchDeleteUser")
    public String batchDeleteUser() {
        return "/user/batchDeleteUser";
    }
    @Autowired
    @Qualifier("userServiceImpl")
    private UserServiceImpl userServiceImpl;
    @Autowired
    private UserService  userService;
    @GetMapping("/user/add")
    private void update(){
        userServiceImpl.update();

    }

    @GetMapping("/user/select")
    private void select(){
        userServiceImpl.select();

    }

}
