package com.javaczh.security.mapper;

import com.javaczh.security.entity.model.SysPermission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysPermissionMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysPermission record);

    int insertSelective(SysPermission record);

    SysPermission selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysPermission record);

    int updateByPrimaryKey(SysPermission record);

    List<SysPermission> selectAllPermission();

    List<SysPermission> getBtnPermissionByRole(String userId);

    List<SysPermission> selectAllPermissionAndRole();


}