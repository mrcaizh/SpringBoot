package com.javaczh.security;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.sql.DataSource;

/**
 * @ClassName SecurityApp
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/12 13:55
 * @Version 1.0
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.javaczh.security")
public class SecurityApp {
    public static void main(String[] args) {
        SpringApplication.run(SecurityApp.class, args);
    }
    @Bean
    public CommandLineRunner runner(ApplicationContext context){
        return args -> {
            System.out.println("find these beans ...");
            System.out.println("BeanDefinitionCount:" + context.getBeanDefinitionCount());
            for (String beanDefinitionName : context.getBeanDefinitionNames()) {
                System.out.println(beanDefinitionName);
            }


        };
    }
}
