package com.javaczh.security.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

/**
 * @ClassName QQSettings
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/15 17:26
 * @Version 1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "qq")
@Validated
public class QQSettings {
    //APP ID 和 APP Key
    private String appid;
    private String appkey;
    private String baseUrl;
}

