package com.javaczh.security.comfig;

import com.p6spy.engine.common.P6Util;
import com.p6spy.engine.spy.appender.MessageFormattingStrategy;

/**
 * @ClassName P6SpyLogger
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/6 16:21
 * @Version 1.0
 */
public class P6SpyLogger implements MessageFormattingStrategy {

    /**
     * @param connectionId 连接id
     * @param now          当前时间
     * @param elapsed      执行时长，包括执行 SQL 和处理结果集的时间(可以参考来调优)
     * @param category     语句分类，statement、resultset 等
     * @param prepared     查询语句。可能是 prepared statement，表现为 select * from table1 where c1=?，问号参数形式
     * @param sql          含参数值的查询语句，如 select * from from table1 where c1=7
     * @param url          数据库连接url
     */
    @Override
    public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql, String url) {
        return new StringBuilder()
                .append("\n=====================================================\n")
                .append("连接id  ").append(connectionId).append("\n")
                .append("url  ").append(url).append("\n")
                .append("当前时间：").append(now).append("\n")
                .append("类别：").append(category).append("\n")
                .append("花费时间：").append(elapsed).append(" ms\n")
                .append("预编译sql：").append(P6Util.singleLine(prepared)).append("\n")
                .append("最终执行的sql：").append(P6Util.singleLine(sql)).append("\n")
                .append("\n=====================================================\n").toString();
    }
}
