package com.javaczh.security.mapper;



import com.javaczh.security.entity.model.SysPermission;
import com.javaczh.security.entity.model.SysRolePermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRolePermissionMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRolePermission record);

    int insertSelective(SysRolePermission record);

    SysRolePermission selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRolePermission record);

    int updateByPrimaryKey(SysRolePermission record);

    int batchAddRolePermission(List<SysRolePermission> list);

    int deleteByRoleId(@Param("roleId") String roleId);

    List<SysRolePermission> selectRolePermissionByRoleId(@Param("roleId") String roleId);

    List<String> selectPermissionIdByRoleId(@Param("roleId") String roleId);

    List<String> selectPermissionIdsByRoleId(@Param("roleId") String roleId);

    List<SysPermission> selectPermissionUrlByUserId(String userId);
}