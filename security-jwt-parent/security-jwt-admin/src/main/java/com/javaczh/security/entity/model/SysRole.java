package com.javaczh.security.entity.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Accessors(chain = true)
@Data
public class SysRole implements Serializable {
    private String id;

    private String name;

    private String description;

    private String status;

    private Date createTime;

    private Date updateTime;

    private Integer deleted;

  //  private List<PermissionRespNodeVO> permissionRespNode;



}