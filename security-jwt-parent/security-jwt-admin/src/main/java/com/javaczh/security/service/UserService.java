package com.javaczh.security.service;

import com.javaczh.security.entity.model.SysUser;
import com.javaczh.security.entity.vo.UserVO;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName UserService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/10 11:09
 * @Version 1.0
 */
public interface UserService {
    /**
     * 生成Token
     *
     * @param sysUser
     * @return
     */
    UserVO generatorToken(SysUser sysUser);

    void logoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication);

    String refreshToken(String refreshToken);
      SysUser selectSysUserByPrimaryKey(String userId);

    void update();

    void select();
}
