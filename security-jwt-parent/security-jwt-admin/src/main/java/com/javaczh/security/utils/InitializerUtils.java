package com.javaczh.security.utils;

import org.springframework.stereotype.Component;

@Component
public class InitializerUtils {
    private TokenSettings tokenSettings;

    public InitializerUtils(TokenSettings tokenSettings) {
        JwtTokenUtils.setTokenSettings(tokenSettings);
    }
}
