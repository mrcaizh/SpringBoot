package com.javaczh.security.authentication;

import com.javaczh.security.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName CustomLogoutSuccessHandler
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/15 8:58
 * @Version 1.0
 */
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

    private UserService userService;

    public CustomLogoutSuccessHandler(UserService userService) {
        this.userService = userService;
    }

    /**
     * LogoutSuccessHandler 在退出成功执行的处理类
     *  退出登录后可以清除cookie或token 或者处理注销后的逻辑操作,或者跳转页面
     *
     * @param request
     * @param response
     * @param authentication
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        userService.logoutSuccess(request, response, authentication);
    }
}
