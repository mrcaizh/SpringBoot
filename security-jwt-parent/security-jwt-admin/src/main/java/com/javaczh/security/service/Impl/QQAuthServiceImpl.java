package com.javaczh.security.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.javaczh.security.contants.Constant;
import com.javaczh.security.entity.Result;
import com.javaczh.security.entity.mapstruct.OtherLoginMapStruct;
import com.javaczh.security.entity.model.OtherLogin;
import com.javaczh.security.entity.model.QQUserInfo;
import com.javaczh.security.entity.model.SysUser;
import com.javaczh.security.entity.vo.UserVO;
import com.javaczh.security.exception.code.BaseResponseCode;
import com.javaczh.security.mapper.OtherLoginMapper;
import com.javaczh.security.service.QQAuthService;
import com.javaczh.security.service.UserService;
import com.javaczh.security.utils.QQSettings;
import com.javaczh.security.utils.SimpleRestClient;
import com.javaczh.security.utils.id.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName QQAuthService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/15 21:38
 * @Version 1.0
 */
@Service
@Slf4j
public class QQAuthServiceImpl implements QQAuthService {
    @Autowired
    private QQSettings qqSettings;
    @Autowired
    private SimpleRestClient simpleRestClient;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private OtherLoginMapper otherLoginMapper;
    @Autowired
    private UserService userService;

    /**
     * 生成QQ授权回调地址
     *
     * @return
     */
    @Override
    public String generatorAuthURL() {
        //Step1：获取Authorization Code
        //QQ互联中的回调地址
        String backUrl = qqSettings.getBaseUrl().concat("/qqLoginBack");
        String authorizeURL = new StringBuilder("https://graph.qq.com/oauth2.0/authorize")
                .append("?response_type=code")
                .append("&client_id=").append(qqSettings.getAppid())
                .append("&redirect_uri=").append(backUrl).toString();
    /*    String authorizeURL = "https://graph.qq.com/oauth2.0/authorize?response_type=code" +
                "&client_id=" + APPID +
                "&redirect_uri=" + URLEncoder.encode(backUrl);*/
        return authorizeURL;
    }

    /**
     * QQ接口回调处理逻辑
     *
     * @param param
     */
    @Override
    public Result<Object> qqLoginBack(Map<String, Object> param) {
        // qq返回的信息：http://graph.qq.com/demo/index.jsp?code=9A5F************************06AF&state=test
        //获取QQ Access Token
        getQQAccessToken(param);
        getOpenID(param);
        QQUserInfo userInfo = getUserInfo(param);
        //保存用户信息
        userInfo.setOpenId((String) param.get("openid"));
        String platform = "QQ";
        Map<String,String> result=new HashMap<>(5);
        result.put("openid",(String) param.get("openid"));
        result.put("platform",platform);
        OtherLogin queryOtherLogin = otherLoginMapper.selectByOpenIdAndPlatform(userInfo.getOpenId(), platform);
        if (null != queryOtherLogin) {
            //没有与系统账号关系,则需要跳转到指定页面进行账号关联
            //已经关联到账号,直接进行生成token跳转首页
            if (Constant.ZERO.equals(queryOtherLogin.getUserId()) || StringUtils.isEmpty(queryOtherLogin.getUserId())) {
                return new Result<>(BaseResponseCode.OTHER_LOGIN_EXCEPTION,result);
            } else {
                SysUser user = userService.selectSysUserByPrimaryKey(queryOtherLogin.getUserId());
                UserVO userVO = userService.generatorToken(user);
                return new Result<>(userVO);
            }
        } else {
            OtherLogin otherLogin = OtherLoginMapStruct.INSTANCE.userInfoToOtherLogin(userInfo);
            otherLogin.setId(idWorker.nextIdToString());
            otherLogin.setPlatform(platform);
            otherLogin.setOpenid(userInfo.getOpenId());
            otherLogin.setAccessToken((String) param.get("access_token"));
            otherLogin.setNickname(userInfo.getNickname());
            otherLogin.setFigureurl1(userInfo.getFigureurl_1());
            otherLogin.setFigureurl2(userInfo.getFigureurl_2());
            otherLogin.setFigureurlqq1(userInfo.getFigureurl_qq_1());
            otherLogin.setFigureurlqq2(userInfo.getFigureurl_2());
            otherLogin.setCreatetime(new Date());
            otherLoginMapper.insertSelective(otherLogin);
        }

        return new Result<>(BaseResponseCode.OTHER_LOGIN_EXCEPTION,result);
    }

    /**
     * 1.接口说明
     * 获取登录用户在QQ空间的信息，包括昵称、头像、性别及黄钻信息（包括黄钻等级、是否年费黄钻等）。
     * <p>
     * 2.使用场景
     * 此接口主要用于网站使用QQ登录时，直接拉取用户在QQ空间的昵称、头像、性别等信息，降低用户的注册成本。
     * <p>
     * 3.接口调用说明
     * 3.1请求说明
     * url	https://graph.qq.com/user/get_user_info
     * 支持验证方式	oauth2.0
     * 格式	JSON
     * http请求方式	GET
     * 是否需要鉴权	需要
     * 接口调试	点击这里测试
     * 3.2输入参数说明
     * 各个参数请进行URL 编码，编码时请遵守 RFC 1738。
     * （1）通用参数
     * -OAuth2.0协议必须传入的通用参数，详见这里
     * <p>
     * 3.3请求示例
     * 以OAuth2.0协议为例（敏感信息都用*号进行了处理，实际请求中需要替换成真实的值）：
     * <p>
     * https://graph.qq.com/user/get_user_info?
     * access_token=*************&
     * oauth_consumer_key=12345&
     * openid=****************
     * 3.4返回参数说明
     * 参数说明	描述
     * ret	返回码
     * msg	如果ret<0，会有相应的错误信息提示，返回数据全部用UTF-8编码。
     * nickname	用户在QQ空间的昵称。
     * figureurl	大小为30×30像素的QQ空间头像URL。
     * figureurl_1	大小为50×50像素的QQ空间头像URL。
     * figureurl_2	大小为100×100像素的QQ空间头像URL。
     * figureurl_qq_1	大小为40×40像素的QQ头像URL。
     * figureurl_qq_2	大小为100×100像素的QQ头像URL。需要注意，不是所有的用户都拥有QQ的100x100的头像，但40x40像素则是一定会有。
     * gender	性别。 如果获取不到则默认返回"男"
     * 3.5返回码说明
     * 0: 正确返回
     * 其它: 失败。错误码说明详见：公共返回码说明。
     * <p>
     * 3.6正确返回示例
     * JSON示例:
     * <p>
     * {
     * "ret":0,
     * "msg":"",
     * "nickname":"Peter",
     * "figureurl":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/30",
     * "figureurl_1":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/50",
     * "figureurl_2":"http://qzapp.qlogo.cn/qzapp/111111/942FEA70050EEAFBD4DCE2C1FC775E56/100",
     * "figureurl_qq_1":"http://q.qlogo.cn/qqapp/100312990/DE1931D5330620DBD07FB4A5422917B6/40",
     * "figureurl_qq_2":"http://q.qlogo.cn/qqapp/100312990/DE1931D5330620DBD07FB4A5422917B6/100",
     * "gender":"男",
     * "is_yellow_vip":"1",
     * "vip":"1",
     * "yellow_vip_level":"7",
     * "level":"7",
     * "is_yellow_year_vip":"1"
     * }
     * <p>
     * <p>
     * 3.7错误返回示例
     * <p>
     * <p>
     * { "ret":1002, "msg":"请先登录" }
     *
     * @param param
     */
    private QQUserInfo getUserInfo(Map<String, Object> param) {
        log.info("=============== 获取QQ用户信息 。 ===============");

        //Step4：获取QQ用户信息
        String reqURL = new StringBuilder("https://graph.qq.com/user/get_user_info")
                .append("?access_token=").append(param.get("access_token"))
                .append("&oauth_consumer_key=").append(qqSettings.getAppid())
                .append("&openid=").append(param.get("openid"))
                .toString();
        // QQUserInfo forObject = simpleRestClient.getForObject(reqURL, QQUserInfo.class);
        log.info("=============== 请求URL: {}  ===============", reqURL);
        String json = simpleRestClient.getForObject(reqURL, String.class);
        //  QQUserInfo forObject = simpleRestClient.getForObject(reqURL, QQUserInfo.class);
        System.out.println(json);
        QQUserInfo qqUserInfo = JSONObject.parseObject(json, QQUserInfo.class);
        return qqUserInfo;
    }

    /**
     * 获取用户 OpenID_OAuth2.0
     * 本步骤的作用：
     * 通过输入在上一步获取的Access Token，得到对应用户身份的OpenID。
     * OpenID是此网站上或应用中唯一对应用户身份的标识，网站或应用可将此ID进行存储，便于用户下次登录时辨识其身份，或将其与用户在网站上或应用中的原有账号进行绑定。
     * 本步骤在整个流程中的位置：
     * oauth2.0_guid_4.png
     * 上一步
     * server-side模式：请参见使用Authorization_Code获取Access_Token
     * client-side模式：请参见使用Implicit_Grant方式获取Access_Token
     * <p>
     * 1 请求地址
     * PC网站：https://graph.qq.com/oauth2.0/me
     * <p>
     * 2 请求方法
     * GET
     * <p>
     * 3 请求参数
     * 请求参数请包含如下内容：
     * <p>
     * 参数	是否必须	含义
     * access_token	必须	在Step1中获取到的access token。
     * <p>
     * <p>
     * 4 返回说明
     * PC网站接入时，获取到用户OpenID，返回包如下：
     * <p>
     * callback( {"client_id":"YOUR_APPID","openid":"YOUR_OPENID"} );
     * openid是此网站上唯一对应用户身份的标识，网站可将此ID进行存储便于用户下次登录时辨识其身份，或将其与用户在网站上的原有账号进行绑定。
     * <p>
     * 5 错误码说明
     * 接口调用有错误时，会返回code和msg字段，以url参数对的形式返回，value部分会进行url编码（UTF-8）。
     * PC网站接入时，错误码详细信息请参见：100000-100031：PC网站接入时的公共返回码。
     */
    private void getOpenID(Map<String, Object> param) {
        String reqURL = new StringBuilder("https://graph.qq.com/oauth2.0/me")
                .append("?access_token=").append(param.get("access_token")).toString();
        //  reqURL2 = "https://graph.qq.com/oauth2.0/me?access_token=" + data.get("access_token");
        log.info("=============== 通过 Access Token，得到对应用户身份的 OpenID。 ===============");
        log.info("=============== 请求URL: {}  ===============", reqURL);
        String responseBody2 = simpleRestClient.getForObject(reqURL, String.class);
        JSONObject jsonObject = parseJSONP(responseBody2);
        Map<String, String> map = jsonObject.toJavaObject(Map.class);
        log.info("=============== 获取 OpenID 响应数据 : {}  ===============", map);
        param.putAll(map);
    }

    /**
     * Step2：通过Authorization Code获取Access Token
     * 请求地址：
     * PC网站：https://graph.qq.com/oauth2.0/token
     * 请求方法：
     * GET
     * 请求参数：
     * 请求参数请包含如下内容：
     * <p>
     * 参数	是否必须	含义
     * grant_type	必须	授权类型，在本步骤中，此值为“authorization_code”。
     * client_id	必须	申请QQ登录成功后，分配给网站的appid。
     * client_secret	必须	申请QQ登录成功后，分配给网站的appkey。
     * code	必须	上一步返回的authorization code。
     * 如果用户成功登录并授权，则会跳转到指定的回调地址，并在URL中带上Authorization Code。
     * 例如，回调地址为www.qq.com/my.php，则跳转到：
     * http://www.qq.com/my.php?code=520DD95263C1CFEA087******
     * 注意此code会在10分钟内过期。
     * redirect_uri	必须	与上面一步中传入的redirect_uri保持一致。
     * <p>
     * 返回说明：
     * <p>
     * 如果成功返回，即可在返回包中获取到Access Token。 如：
     * <p>
     * access_token=FE04************************CCE2&expires_in=7776000&refresh_token=88E4************************BE14
     * <p>
     * 参数说明	描述
     * access_token	授权令牌，Access_Token。
     * expires_in	该access token的有效期，单位为秒。
     * refresh_token	在授权自动续期步骤中，获取新的Access_Token时需要提供的参数。
     * 注：refresh_token仅一次有效
     * <p>
     * 错误码说明：
     * 接口调用有错误时，会返回code和msg字段，以url参数对的形式返回，value部分会进行url编码（UTF-8）。
     * PC网站接入时，错误码详细信息请参见：100000-100031：PC网站接入时的公共返回码。
     * https://wiki.connect.qq.com/%E5%85%AC%E5%85%B1%E8%BF%94%E5%9B%9E%E7%A0%81%E8%AF%B4%E6%98%8E#100000-100031.EF.BC.9APC.E7.BD.91.E7.AB.99.E6.8E.A5.E5.85.A5.E6.97.B6.E7.9A.84.E5.85.AC.E5.85.B1.E8.BF.94.E5.9B.9E.E7.A0.81
     */
    private void getQQAccessToken(Map<String, Object> param) {
        String backUrl = qqSettings.getBaseUrl() + "/qq/callback";
        String reqURL = new StringBuilder("https://graph.qq.com/oauth2.0/token")
                .append("?grant_type=authorization_code")
                .append("&client_id=").append(qqSettings.getAppid())
                .append("&client_secret=").append(qqSettings.getAppkey())
                .append("&code=").append(param.get("code"))
                .append("&redirect_uri=").append(backUrl).toString();
        log.info("=============== 通过Authorization Code 获取Access Token  ===============");
        log.info("=============== 请求URL: {}  ===============", reqURL);
        String responseBody = simpleRestClient.getForObject(reqURL, String.class);
        log.info("=============== 获取Access Token 响应数据 : {}  ===============", responseBody);
        param.putAll(splitURL(responseBody));
    }


    private static JSONObject parseJSONP(String jsonp) {
        int startIndex = jsonp.indexOf("(");
        int endIndex = jsonp.lastIndexOf(")");
        String json = jsonp.substring(startIndex + 1, endIndex);
        return JSONObject.parseObject(json);
    }

    private Map<String, String> splitURL(String url) {
        //String url = "access_token=0A927F7E6A451A6CF5DDC497532363C1&expires_in=7776000&refresh_token=FBEBB8CCB4342F2D86D841610AA6F236";
        String[] splitArr = url.split("&");
        System.out.println(Arrays.toString(splitArr));
        Map<String, String> data = new HashMap<>(splitArr.length * 2);
        for (String str : splitArr) {
            int index = str.indexOf("=");
            String key = str.substring(0, index);
            String value = str.substring(index + 1);
            data.put(key, value);
        }
        //{access_token=0A927F7E6A451A6CF5DDC497532363C1, refresh_token=FBEBB8CCB4342F2D86D841610AA6F236, expires_in=7776000}
        //  System.out.println(data);
        return data;
    }
}
