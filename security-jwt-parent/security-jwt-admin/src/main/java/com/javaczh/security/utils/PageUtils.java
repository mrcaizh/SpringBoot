package com.javaczh.security.utils;


import com.github.pagehelper.Page;
import com.javaczh.security.entity.PageResult;


import java.util.List;


public class PageUtils {
    private PageUtils() {
    }

    public static <T> PageResult<T> getPageResult(List<T> list) {
        PageResult<T> result = new PageResult<>();
        if (list instanceof Page) {
            Page<T> page = (Page<T>) list;
            result.setTotalRows(page.getTotal());
            result.setTotalPages(page.getPages());
            result.setPageNum(page.getPageNum());
            result.setCurPageSize(page.getPageSize());
            result.setPageSize(page.size());
            result.setList(page.getResult());
        }
        return result;
    }
}
