package com.javaczh.security.entity.mapstruct;

import com.javaczh.security.entity.dto.PermissionDto;
import com.javaczh.security.entity.model.SysPermission;
import com.javaczh.security.entity.vo.PermissionNodeVo;
import com.javaczh.security.entity.vo.PermissionVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @ClassName UserMapStruct
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 15:54
 * @Version 1.0
 */
@Mapper
public interface PermissionMapStruct {

    PermissionMapStruct INSTANCE = Mappers.getMapper(PermissionMapStruct.class);

    /**
     * vo转实体
     *
     * @param permissionVo
     * @return
     */

    SysPermission voToModel(PermissionVo permissionVo);

    /**
     * 实体转vo
     *
     * @param sysPermission
     * @return
     */
    PermissionVo modelToVo(SysPermission sysPermission);

    /**
     * dto转实体
     *
     * @param permissionDto
     * @return
     */
    SysPermission dtoToModel(PermissionDto permissionDto);

    /**
     * @param permissionVo
     * @return
     */
    PermissionNodeVo voToNodeVo(PermissionVo permissionVo);

    /**
     *
     * @param permissionList
     * @return
     */
    List<PermissionVo> modelListToVoList(List<SysPermission> permissionList);
}
