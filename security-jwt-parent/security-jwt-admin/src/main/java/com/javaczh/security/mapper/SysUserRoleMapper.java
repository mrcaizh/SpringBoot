package com.javaczh.security.mapper;


import com.javaczh.security.entity.model.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface SysUserRoleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);

    int bacthAddUserRole(List<SysUserRole> list);

    int deleteByUserId(String userId);

    Set<String> selectRoleIdsByUserId(String userId);

    List<String> selectRoleNamesByUserId(String userId);
}