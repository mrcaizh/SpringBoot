package com.javaczh.security.entity.mapstruct;

import com.javaczh.security.entity.dto.PermissionDto;
import com.javaczh.security.entity.model.OtherLogin;
import com.javaczh.security.entity.model.QQUserInfo;
import com.javaczh.security.entity.model.SysPermission;
import com.javaczh.security.entity.vo.PermissionNodeVo;
import com.javaczh.security.entity.vo.PermissionVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @ClassName UserMapStruct
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 15:54
 * @Version 1.0
 */
@Mapper
public interface OtherLoginMapStruct {

    OtherLoginMapStruct INSTANCE = Mappers.getMapper(OtherLoginMapStruct.class);

    OtherLogin userInfoToOtherLogin(QQUserInfo userInfo);
}
