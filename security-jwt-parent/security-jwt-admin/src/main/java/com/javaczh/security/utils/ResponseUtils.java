package com.javaczh.security.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.javaczh.security.entity.Result;
import com.javaczh.security.exception.code.BaseResponseCode;
import com.javaczh.security.exception.code.ResponseCodeInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @ClassName ResponseUtils
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/21 17:58
 * @Version 1.0
 */
@Slf4j
public class ResponseUtils {
    /**
     * 响应成功
     *
     * @param response
     * @param data
     * @throws Exception
     */
    public static void simpleResponseMessage(HttpServletResponse response, Object data) throws Exception {
        Result result = new Result(data);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        response.setStatus(200);
        out.println(new ObjectMapper().writeValueAsString(result));
        out.flush();
        out.close();
    }

    public static void simpleResponseMessage(HttpServletResponse response, Integer code, String message) {
        try (PrintWriter out = response.getWriter()) {
            Result success = Result.getResult(code, message);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding("UTF-8");
            response.setStatus(200);
            out.println(new ObjectMapper().writeValueAsString(success));
        } catch (Exception e) {
            log.error("======= simpleResponseMessage(HttpServletResponse response, Integer code, String message)========方法错误");
            e.printStackTrace();
        }
    }

    public static void simpleResponseMessage(HttpServletResponse response, ResponseCodeInterface responseCodeInterface) {
        PrintWriter out = null;
        try {
            Result success = Result.getResult(responseCodeInterface.getCode(), responseCodeInterface.getMsg());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding("UTF-8");
            out = response.getWriter();
            response.setStatus(200);
            out.println(new ObjectMapper().writeValueAsString(success));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
        }

    }
}
