package com.javaczh.security.contants;

/**
 * @Description:
 * @Author:CaiZiHao
 * @Date:2020/5/26 12:38
 */

public class Constant {
    /**
     * 图片验证码
     */
    public static final String IMAGE_CODE_VALUE = "image-code-value_";

    /**
     *  String pattern = "/select/**";
     */
    public static final String ANON_REQUEST_URL_PATTERN = "/api/**/anon/**";

    public static final String SELECT_ROLE = "select";

    /**
     * 用户名称 key
     */
    public static final String JWT_USER_NAME = "jwt-user-name-key";
    public static final String JWT_USER_PWD = "jwt-user-pwd-key";
    public static final String JWT_USER_ROLE_LIST = "jwt-user-role-key";
    public static final String JWT_USER_INFO_KEY = "jwt-user-info-key";

    /**
     * 角色信息key
     */
    public static final String ROLES_INFOS_KEY = "roles-infos-key";

    /**
     * 权限信息key
     */
    public static final String PERMISSIONS_INFOS_KEY = "permissions-infos-key";

    /**
     * refresh_token 主动退出后加入黑名单 key
     */
    public static final String JWT_REFRESH_TOKEN_BLACKLIST = "jwt-refresh-token-blacklist_";

    /**
     * access_token 主动退出后加入黑名单 key
     */
    public static final String JWT_ACCESS_TOKEN_BLACKLIST = "jwt-access-token-blacklist_";

    /**
     * 正常token
     */
    public static final String ACCESS_TOKEN = "authorization";
    /**
     * 刷新token
     */
    public static final String REFRESH_TOKEN = "refreshToken";

    /**
     * 标记用户是否已经被锁定
     */
    public static final String ACCOUNT_LOCK_KEY = "account-lock-key_";

    /**
     * 标记用户是否已经删除
     */
    public static final String DELETED_USER_KEY = "deleted-user-key_";

    /**
     * 主动去刷新 token key(适用场景 比如修改了用户的角色/权限去刷新token)
     */
    public static final String JWT_REFRESH_KEY = "jwt-refresh-key_";
    /**
     * 标记新的access_token
     */
    public static final String JWT_REFRESH_IDENTIFICATION = "jwt-refresh-identification_";

    /**
     * 部门编码key
     */
    public static final String DEPT_CODE_KEY = "dept-code-key_";

    /**
     * 用户权鉴缓存 key
     */
    public static final String IDENTIFY_CACHE_KEY = "shiro-cache:com.yingxue.lesson.shiro.CustomRealm.authorizationCache:";

    /**
     * 获取上传的文件类型key
     */
    public static final String FILE_TYPE = "file-type_";

    /**
     * 数字0
     */
    public static final String ZERO = "0";
    /**
     * 默认顶级菜单
     */
    public static final String ROOT_MENUS = "默认顶级菜单";

    /**
     * 全部菜单Key
     */
    public static final String ALL_MENUS_KEY = "all_menus_key";


    public static final String REDIS_CHANNEL_KEY_RECEIVE_MESSAGE = "receiveMessage";

    public static final String REDIS_CHANNEL_KEY_UPDATE_MENUS_CASE = "updateMenusCase";



}
