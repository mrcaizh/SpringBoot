package com.javaczh.security.authentication;


import com.javaczh.security.entity.model.SysUser;
import com.javaczh.security.entity.vo.UserVO;
import com.javaczh.security.service.UserService;
import com.javaczh.security.utils.ResponseUtils;
import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName LoginSuccessHandler
 * @Description 登陆成功
 * @Author CaiZiHao
 * @Date 2020/6/9 16:55
 * @Version 1.0
 */

public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    private UserService userService;

    public LoginSuccessHandler(UserService userService) {
        this.userService = userService;
    }

    @SneakyThrows
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {
        Object principal = authentication.getPrincipal();
        SysUser sysUser = (SysUser) authentication.getPrincipal();
        UserVO userVO = userService.generatorToken(sysUser);
        System.out.println(userVO.getAccessToken());
        ResponseUtils.simpleResponseMessage(httpServletResponse, userVO);
    }
}
