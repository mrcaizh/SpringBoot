package com.javaczh.security.service;


import com.javaczh.security.entity.dto.PermissionDto;
import com.javaczh.security.entity.model.SysPermission;
import com.javaczh.security.entity.vo.PermissionNodeVo;
import com.javaczh.security.entity.vo.PermissionVo;
import com.javaczh.security.entity.vo.RolePermissionVo;

import java.util.List;

/**
 * @ClassName PermissionService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/27 18:01
 * @Version 1.0
 */
public interface PermissionService {
    List<PermissionVo> selectAllPermission();

    /**
     * 获取所有目录菜单树接口-查到到目录
     *
     * @return
     */
    List<PermissionNodeVo> getMenusTreeToDirectory();

    /**
     * 新增菜单
     *
     * @param permissionDto
     * @return
     */
    SysPermission addOrUpdatePermission(PermissionDto permissionDto);

    List<PermissionNodeVo> permissionTreeList(String userId);

    Boolean deletePermission(List<String> ids);

    List<PermissionNodeVo> getMenusTreeToBtn();

    List<PermissionNodeVo> getMenusTreeToBtnByRoleId(String roleId);

    RolePermissionVo getMenusTreeToBtnByRoleId2(String roleId);

    /**
     * 根据角色ID获取按钮权限,主要是控制前端页面的按钮显示和隐藏
     *
     * @re
     */
    List<PermissionVo> getBtnPermissionByRole();

    /**
     * 获取全部菜单和角色
     *
     * @return
     */
    List<SysPermission>  selectAllPermissionAndRole();
}
