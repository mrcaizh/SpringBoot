package com.javaczh.security.exception;

import com.javaczh.security.entity.Result;

import com.javaczh.security.exception.code.BaseResponseCode;
import com.qq.connect.QQConnectException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName AdminGlobalException
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 14:25
 * @Version 1.0
 */
@RestControllerAdvice
@Slf4j
public class AdminGlobalException {


    @ExceptionHandler(BusinessException.class)
    public <T> Result<T> businessException(BusinessException e) {
        log.error("<==BusinessException,exception:{}==>", e);
        return new Result<>(e.getMessageCode(), e.getDetailMessage());
    }

    @ExceptionHandler(QQConnectException.class)
    public <T> Result<T> qQConnectException(QQConnectException e) {
        log.error("<==QQConnectException,exception:{}==>", e);
        return new Result<>(BaseResponseCode.QQCONNECTEXCEPTION);
    }

    @ExceptionHandler(Exception.class)
    public <T> Result<T> handleException(Exception e) {
        log.error("Exception,exception:{}", e);
        return new Result<>(BaseResponseCode.SYSTEM_ERROR);
    }

    /**
     * 数据校验失败异常
     *
     * @param e
     * @param <T>
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public <T> Result<T> methodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        List<String> errorMessage = new ArrayList<>(fieldErrors.size());
        fieldErrors.forEach(error -> {
            errorMessage.add(error.getDefaultMessage());
        });
        log.error("<===MethodArgumentNotValidException,exception===> : {}", errorMessage.toString());
        return new Result<>(BaseResponseCode.METHOD_IDENTITY_ERROR.getCode(),
                "数据校验异常".concat(errorMessage.toString()));
    }


}
