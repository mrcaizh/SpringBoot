package com.javaczh.security.authentication;

import com.alibaba.druid.util.StringUtils;
import com.javaczh.security.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName ValidateCodeFilter
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/15 13:24
 * @Version 1.0
 */
public class ValidateCodeFilter extends OncePerRequestFilter {
    @Autowired
    private LoginAuthenticationFailureHandler loginAuthenticationFailureHandler;

    //private SessionStrategy  sessionStrategy = new HttpSessionSessionStrategy();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //请求路径必须是/authentication/require 和post请求
        if (StringUtils.equals("/authentication/form", request.getRequestURI())
                && StringUtils.equalsIgnoreCase("post", request.getMethod())) {
            try {
                validate(new ServletWebRequest(request));
            } catch (BusinessException e) {
                //loginAuthenticationFailureHandler.onAuthenticationFailure(request, response, e);
                return;
            }
        }
        filterChain.doFilter(request, response);

    }

    private void validate(ServletWebRequest request) throws ServletRequestBindingException {
/*        ImageCode codeInSession = (ImageCode) sessionStrategy.getAttribute(request, CodeController.SESSION_KEY);

        String reqImageCode = ServletRequestUtils.getStringParameter(request.getRequest(), "imageCode");

        if (StringUtils.isBlank(reqImageCode)) {
            throw new ValidateCodeException("验证码不能为空");
        }
        if (codeInSession == null) {
            throw new ValidateCodeException("验证码不存在");
        }
        if (codeInSession.getExpireTime() == LocalDateTime.now().plusSeconds(0)) {
            sessionStrategy.removeAttribute(request, CodeController.SESSION_KEY);
            throw new ValidateCodeException("验证码已过期");
        }
        if (!StringUtils.equals(codeInSession.getCode(), reqImageCode)) {
            throw new ValidateCodeException("验证码不匹配");
        }
        sessionStrategy.removeAttribute(request, CodeController.SESSION_KEY);*/
    }


    public LoginAuthenticationFailureHandler getLoginAuthenticationFailureHandler() {
        return loginAuthenticationFailureHandler;
    }

    public void setLoginAuthenticationFailureHandler(LoginAuthenticationFailureHandler loginAuthenticationFailureHandler) {
        this.loginAuthenticationFailureHandler = loginAuthenticationFailureHandler;
    }
}