package com.javaczh.security.controller;

import com.javaczh.security.entity.Result;
import com.javaczh.security.entity.model.QQUserInfo;
import com.javaczh.security.service.QQAuthService;
import com.javaczh.security.utils.QQSettings;
import com.javaczh.security.utils.SimpleRestClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName QQAuthoriController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/15 16:38
 * @Version 1.0
 */
@RestController
@CrossOrigin
@Api(tags = "QQ登陆接口")
public class QQAuthController {

    @Autowired
    private QQAuthService qqAuthService;

    /**
     * 生成QQ授权回调地址
     *
     * @return
     */
    @GetMapping("/qqAuth")
    @ApiOperation(value = "生成QQ授权回调地址")
    public Result<String> qqAuth(HttpServletRequest request) {
        return new Result<>(qqAuthService.generatorAuthURL());
    }

    /**
     * QQ授权回调
     * window.location.href ="https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=******&
     * state=register&redirect_uri="+encodeURI("http://zhdydet.xyz:8085/api/qqLogin");
     * <p>
     *
     * @param request
     * @param response
     * @param httpSession
     */
    @GetMapping("/qqLoginBack")
    @ApiOperation(value = "QQ授权回调")
    public Result<Object> qqLoginBack(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        Map<String, Object> param = new HashMap<>(5);
        param.put("code", request.getParameter("code"));
        param.put("state", request.getParameter("state"));
        return qqAuthService.qqLoginBack(param);
    }

}
