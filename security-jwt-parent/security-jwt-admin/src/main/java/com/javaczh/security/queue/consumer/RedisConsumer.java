package com.javaczh.security.queue.consumer;

import com.javaczh.security.contants.Constant;
import com.javaczh.security.service.PermissionService;
import com.javaczh.security.utils.redis.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName RedisConsumer
 * @Description 这个RedisConsumer类将会被注册为一个消息监听者时。处理消息的方法我们可以任意命名，我们有相当大的灵活性。
 * @Author CaiZiHao
 * @Date 2020/6/15 11:16
 * @Version 1.0
 */
@Component
@Slf4j
public class RedisConsumer {

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RedisService redisService;

    public void receiveMessage(String message) {
        System.out.println("==================== 接收到信息: " + message + "====================");
    }

    /**
     * 刷新缓存
     *
     * @param message
     */
    public void updateMenusCase(String message) {
        log.info("================ 刷新菜单缓存,key: {} ===========================", Constant.ALL_MENUS_KEY);
        redisService.set(Constant.ALL_MENUS_KEY, permissionService.selectAllPermissionAndRole());
        log.info("================= 菜单缓存刷新成功 ===========================", Constant.ALL_MENUS_KEY);
    }
}
