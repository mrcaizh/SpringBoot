package com.javaczh.security.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClassName SecuritySettings
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 14:48
 * @Version 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "admin.security")
public class SecuritySettings {

    /**
     * 配置不需要登陆也可以访问的路径
     */
    private List<String> ignoreUrl;


}
