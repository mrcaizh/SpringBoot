package com.javaczh.security.mapper;

import com.javaczh.security.entity.model.OtherLogin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OtherLoginMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OtherLogin record);

    int insertSelective(OtherLogin record);

    OtherLogin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OtherLogin record);

    int updateByPrimaryKey(OtherLogin record);

    OtherLogin selectByOpenIdAndPlatform(@Param("openId") String openId, @Param("platform")String platform);
}