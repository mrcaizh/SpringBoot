package com.javaczh.security.entity.dto;

import lombok.Data;

import java.util.Set;

/**
 * @ClassName RolePermissionDto
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/2 17:35
 * @Version 1.0
 */
@Data
public class RolePermissionDto {
    private  String roleId;
    private Set<String> permissionIds;
}
