package com.javaczh.security.entity.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class SysRolePermission implements Serializable {
    private String id;

    private String roleId;

    private String permissionId;

    private Date createTime;


}