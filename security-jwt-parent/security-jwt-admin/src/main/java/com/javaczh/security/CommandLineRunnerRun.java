package com.javaczh.security;

import com.javaczh.security.contants.Constant;
import com.javaczh.security.service.PermissionService;
import com.javaczh.security.utils.redis.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @ClassName CommandLineRunnerRun
 * @Description 项目启动成功 ,加载部分数据进redis, 预热
 * @Author CaiZiHao
 * @Date 2020/6/12 16:01
 * @Version 1.0
 */
@Component
@Slf4j
public class CommandLineRunnerRun implements CommandLineRunner {
    @Autowired
    private RedisService redisService;
    @Autowired
    private PermissionService permissionService;

    @Override
    public void run(String... args) throws Exception {
        log.info("=================项目启动成功, 加载缓存 ===========================");
        redisService.set(Constant.ALL_MENUS_KEY, permissionService.selectAllPermissionAndRole());
        log.info("=================redis成功加载{} 菜单数据===========================", Constant.ALL_MENUS_KEY);
    }
}
