package com.javaczh.security.comfig;


import com.javaczh.security.authentication.*;
import com.javaczh.security.service.UserService;
import com.javaczh.security.utils.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

/**
 * @ClassName SecurityConfig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/21 17:49
 * @Version 1.0
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    /**
     * 配置不需要登陆也可以访问的路径
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        //ignore
        web.ignoring().antMatchers("/qqAuth", "/health", "/qqLoginBack",
                "/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**",
                "/user/add", "/user/select"
        );
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //调用  authorizeRequests()方法开启HttpSecurity的配置
//                .antMatchers("/admin/**")
//                .hasAnyRole("admin")//用户访问  /admin/**  模式的URL必须具备admin的角色
//                .antMatchers("/user/**")
//                .access("hasRole('admin')  and  hasRole('user')")//用户访问  /user/**  模式的URL必须具备admin和user的角色
//                .antMatchers("/db/**")
//                .access("hasAnyRole('admin','dba')")  //用户访问  /db/**  模式的URL必须具备admin或user的角色
                .anyRequest().authenticated()
                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                    @Override
                    public <O extends FilterSecurityInterceptor> O postProcess(O o) {
                        o.setSecurityMetadataSource(cfsm());
                        o.setAccessDecisionManager(cadm());
                        return o;
                    }
                })
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)// 前后端分离是无状态的，不用session了，直接禁用。
                .and().exceptionHandling().accessDeniedHandler(new NotPermissionAccessDeniedHandler()) //自定义处理403 异常
                .and().addFilter(new TokenVerifyFilter(super.authenticationManager(), redisService))//增加自定义验证Token认证过滤器
                //任意请求都经过认证
                //开启表单登录，开启登录页面同时配直了登录接口为/login，即可以直接调用/login接口，发起一个
                //POST请求进行登录，登录参数中用户名必须命名为username密码必须命名为password
                //配置loginProcessingUrl接口主要是方便Ajax或者移动端调用登录接口
                //。最后还配置了permitAll表示和登录相关的接口都不需要认证即可访问。
                .formLogin()
                /**
                 *  配置自定义登录页面,用户未获取授权就访问一个需要投权才能访问的接口，就会自动跳转到  loginPage登录页面
                 *usernameParameter和passwordParameter定义了认证所需的用户名和密码的参数名，默认用户名参数是usemame密码参数是password可以在这里自定义。
                 */
                //.loginPage("/loginPage")
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(new LoginSuccessHandler(userService))  //登录成功的处理类
                .failureHandler(new CustomLoginFailureHandler())//登录失败的处理类
                .and()
                .logout()
                .logoutUrl("/logout")  //退出请求url
                .clearAuthentication(true)//清除身份认证信息  默认为  true表示清除。
                .logoutSuccessHandler(new CustomLogoutSuccessHandler(userService))//成功退出处理类
                .permitAll()
                .and().csrf().disable();  //行表示关闭csrf
    }

    @Bean
    public CustomFilterInvocationSecurityMetadataSource cfsm() {
        return new CustomFilterInvocationSecurityMetadataSource();
    }

    @Bean
    public CustomAccessDecisionManager cadm() {
        return new CustomAccessDecisionManager();
    }
}
