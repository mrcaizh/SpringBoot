package com.javaczh.security.authentication;

import com.alibaba.fastjson.JSON;
import com.javaczh.security.contants.Constant;
import com.javaczh.security.entity.model.SysPermission;
import com.javaczh.security.entity.model.SysRole;
import com.javaczh.security.utils.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

/**
 * @ClassName CustomFilterinvocationSecurityMetadataSource
 * @Description 要实现动态配置权限，首先要自定义FilterlnvocationSecurityMetadataSource,
 * Spring Security中通过FilterlnvocationSecurityMetadataSource 接口中的getAttributes
 * 方法来确定一个请求需要哪些角色， FilterlnvocationSecurityMetadataSource
 * 接口的默认实现类是DefaultFilterlnvocationSecurityMetadataSource
 * ，参考DefaultFilterlnvocationSecurityMetadataSource的实现，
 * 我们可以定义自己的FilterlnvocationSecurityMetadataSource ，
 * @Author CaiZiHao
 * @Date 2020/6/12 14:12
 * @Version 1.0
 */
@Component
public class CustomFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    @Autowired
    private RedisService redisService;


    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        //AntPathMatcher，主要用来实现ant风格的URL匹配。在webSecurityConfig  配置的Bean  配置
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        //获取出当前请求的URL
        String requestUrl = ((FilterInvocation) object).getFullRequestUrl();
        //获取请求
      //  final HttpServletRequest request = ((FilterInvocation) object).getRequest();
        if (!StringUtils.isEmpty(requestUrl)){
            //http://localhost:8090/role/addOrUpdateRole
            requestUrl = requestUrl.substring(requestUrl.lastIndexOf(":") + 5);
        }

        /**
         *  从数据库中获取所有的资源信息，即本案例中的menu表以及menu  所对应的role  ,
         *  在真实项目环境中，可以将资源信息缓存在Redis  或者其他缓存数据库中．
         */
        String json = (String) redisService.get(Constant.ALL_MENUS_KEY);
        List<SysPermission> sysPermissions = JSON.parseArray(json, SysPermission.class);
       // List<SysPermission> sysPermissions = (List<SysPermission>) redisService.get(Constant.ALL_MENUS_KEY);
        /**
         *  遍历资源信息，遍历过程中获取当前请求的URL  所需要的角色信息并返回。
         *  如果当前请求的URL  在资源表中不存在相应的模式，就假设该请求登录后即可访问，即直接返回ROLE_LOGIN。
         */
        for (SysPermission permission : sysPermissions) {
            if (!StringUtils.isEmpty(permission.getUrl()) && antPathMatcher.match(permission.getUrl(), requestUrl)) {
                List<SysRole> roles = permission.getRoles();
                String[] roleArr = new String[roles.size()];
                for (int i = 0; i < roles.size(); i++) {
                    roleArr[i] = roles.get(i).getName();
                }
                return SecurityConfig.createList(roleArr);
            }
        }
        return SecurityConfig.createList("ROLE_LOGIN");
    }

    /**
     * getAllConfigAttributes方法用来返回所有定义好的权限资源，
     * Spring  Security在启动时会校验相关配置是否正确，如果不需要校验，那么该方法
     * 直接返回 null 即可
     *
     * @return
     */
    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    /**
     * supports 方法返回类对象是否支持校验。
     *
     * @param aClass
     * @return
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return FilterInvocation.class.isAssignableFrom(aClass);
    }

}
