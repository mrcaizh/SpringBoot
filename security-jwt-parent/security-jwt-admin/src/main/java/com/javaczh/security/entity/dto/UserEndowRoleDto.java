package com.javaczh.security.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @ClassName UserEndowRoleDto
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/4 17:17
 * @Version 1.0
 */
@Data
public class UserEndowRoleDto {
    @ApiModelProperty(value = "用户Id")
    @NotNull
    private String userId;
    @ApiModelProperty(value = "角色Id集合")
    @NotNull
    private Set<String> roleIds;
}
