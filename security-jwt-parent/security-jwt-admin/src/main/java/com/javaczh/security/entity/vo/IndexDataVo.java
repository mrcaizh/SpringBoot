package com.javaczh.security.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName HomeDataVo
 * @Description 首页所需要的数据
 * @Author CaiZiHao
 * @Date 2020/5/27 13:42
 * @Version 1.0
 */
@Data
public class IndexDataVo {

    @ApiModelProperty(value = "目录菜单")
    private List<PermissionNodeVo> menus;

}
