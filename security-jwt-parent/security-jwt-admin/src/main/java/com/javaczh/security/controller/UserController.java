package com.javaczh.security.controller;

import com.javaczh.security.contants.Constant;
import com.javaczh.security.entity.Result;
import com.javaczh.security.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName UserController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/10 14:09
 * @Version 1.0
 */
@RestController
@Slf4j
@CrossOrigin
@Api(tags = "组织模块-用户管理")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/user/token")
    @ApiOperation(value = "用户刷新token接口")
    public Result<String> refreshToken(HttpServletRequest request) {
        String refreshToken = request.getHeader(Constant.REFRESH_TOKEN);
        Result<String> result = Result.success();
        result.setData(userService.refreshToken(refreshToken));
        return result;
    }
}
