package com.javaczh.security.authentication;

import com.javaczh.security.exception.code.BaseResponseCode;
import com.javaczh.security.utils.ResponseUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName NotPermissionAccessDeniedHandler
 * @Description 自定义 403 处理方案
 * 使用 Spring Security 时经常会看见 403（无权限），默认情况下显示的是Servlet页面
 * @Author CaiZiHao
 * @Date 2020/6/13 13:23
 * @Version 1.0
 */
public class NotPermissionAccessDeniedHandler implements AccessDeniedHandler {
    /**
     * Handles an access denied failure.
     *
     * @param request               that resulted in an <code>AccessDeniedException</code>
     * @param response              so that the user agent can be advised of the failure
     * @param accessDeniedException that caused the invocation
     * @throws IOException      in the event of an IOException
     * @throws ServletException in the event of a ServletException
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        ResponseUtils.simpleResponseMessage(response, BaseResponseCode.NOT_PERMISSION);
    }
}
