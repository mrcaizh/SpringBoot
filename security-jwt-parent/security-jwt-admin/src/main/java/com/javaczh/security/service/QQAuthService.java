package com.javaczh.security.service;

import com.javaczh.security.entity.Result;
import com.javaczh.security.entity.model.QQUserInfo;
import com.qq.connect.QQConnectException;
import com.qq.connect.javabeans.AccessToken;
import com.qq.connect.oauth.Oauth;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @ClassName QQAuthoriController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/15 16:38
 * @Version 1.0
 */
public interface QQAuthService {

    /**
     * QQ接口回调处理逻辑
     *
     * @param param
     */
      Result<Object> qqLoginBack(Map<String, Object> param);

    /**
     * 生成QQ授权回调地址
     *
     * @return
     */
    String generatorAuthURL();

}
