package com.javaczh.security.entity.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class OtherLogin implements Serializable {
    /**
     * ID
     */
    @ApiModelProperty(value="ID")
    private String id;

    /**
     * 会员ID
     */
    @ApiModelProperty(value="会员ID")
    private String userId;

    /**
     * 第三方应用
     */
    @ApiModelProperty(value="第三方应用")
    private String platform;

    /**
     * 第三方唯一ID
     */
    @ApiModelProperty(value="第三方唯一ID")
    private String openid;

    /**
     * 第三方会员昵称
     */
    @ApiModelProperty(value="第三方会员昵称")
    private String openname;

    /**
     * AccessToken
     */
    @ApiModelProperty(value="AccessToken")
    private String accessToken;

    /**
     *
     */
    @ApiModelProperty(value="")
    private String refreshToken;

    /**
     * 省(直辖市)
     */
    @ApiModelProperty(value="省(直辖市)")
    private String province;

    /**
     * 市(直辖市区)
     */
    @ApiModelProperty(value="市(直辖市区)")
    private String city;

    /**
     * 出生年月
     */
    @ApiModelProperty(value="出生年月")
    private String year;

    /**
     * 用户在QQ空间的昵称
     */
    @ApiModelProperty(value="用户在QQ空间的昵称")
    private String nickname;

    /**
     * 大小为30×30像素的QQ空间头像URL。
     */
    @ApiModelProperty(value="大小为30×30像素的QQ空间头像URL。")
    private String figureurl;

    /**
     * 大小为50×50像素的QQ空间头像URL。
     */
    @ApiModelProperty(value="大小为50×50像素的QQ空间头像URL。")
    private String figureurl1;

    /**
     * 大小为100×100像素的QQ空间头像URL。
     */
    @ApiModelProperty(value="大小为100×100像素的QQ空间头像URL。")
    private String figureurl2;

    /**
     * 大小为40×40像素的QQ头像URL。
     */
    @ApiModelProperty(value="大小为40×40像素的QQ头像URL。")
    private String figureurlqq1;

    /**
     * 大小为100×100像素的QQ头像URL。需要注意，不是所有的用户都拥有QQ的100×100的头像，但40×40像素则是一定会有
     */
    @ApiModelProperty(value="大小为100×100像素的QQ头像URL。需要注意，不是所有的用户都拥有QQ的100×100的头像，但40×40像素则是一定会有")
    private String figureurlqq2;

    /**
     * 性别。 如果获取不到则默认返回”男”
     */
    @ApiModelProperty(value="性别。 如果获取不到则默认返回”男”")
    private String gender;

    /**
     * 有效期
     */
    @ApiModelProperty(value="有效期")
    private Date expiresIn;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date createtime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Date updatetime;

    /**
     * 登录时间
     */
    @ApiModelProperty(value="登录时间")
    private Date logintime;

    /**
     * 过期时间
     */
    @ApiModelProperty(value="过期时间")
    private Date expiretime;

}