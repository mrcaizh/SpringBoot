import com.alibaba.fastjson.JSONObject;
import com.javaczh.security.SecurityApp;
import com.javaczh.security.contants.Constant;
import com.javaczh.security.entity.model.QQUserInfo;
import com.javaczh.security.entity.model.SysPermission;
import com.javaczh.security.entity.vo.UserVO;
import com.javaczh.security.service.PermissionService;

import com.javaczh.security.utils.redis.RedisService;
import io.swagger.annotations.Api;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.AntPathMatcher;

import java.util.*;

/**
 * @ClassName SecurityTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/12 14:40
 * @Version 1.0
 */
@SpringBootTest(classes = SecurityApp.class)
public class SecurityTest {

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private RedisService redisService;

    @Test
    public void test() {
        List<SysPermission> sysPermissions = permissionService.selectAllPermissionAndRole();
        System.out.println(sysPermissions);
    }

    @Test
    public void test2() {
        UserVO userVO = new UserVO();
        userVO.setNickName("java");
        userVO.setAccessToken(UUID.randomUUID().toString());
        redisTemplate.convertAndSend("receiveMessage", userVO);
        redisTemplate.convertAndSend(Constant.REDIS_CHANNEL_KEY_UPDATE_MENUS_CASE, "开始更新缓存");
    }

    public static void main(String[] args) {
        //http://localhost:8090/role/addOrUpdateRole  /role/addOrUpdateRole
//        String requestUrl = "http://localhost:8090/role/addOrUpdateRole";
//
//        requestUrl = requestUrl.substring(requestUrl.lastIndexOf(":") + 5);
//        System.out.println("截取 " + requestUrl);
//        String url = "/role/addOrUpdateRole";
//        AntPathMatcher antPathMatcher = new AntPathMatcher();
//        System.out.println(antPathMatcher.match(url, requestUrl));
//        String anon = "/**/anon/**";
//        String reqUrl = "/api/user/sdas/asdas/asd/asd/anon/query";
//        System.out.println(antPathMatcher.match(anon, reqUrl));


        /*String url = "access_token=0A927F7E6A451A6CF5DDC497532363C1&expires_in=7776000&refresh_token=FBEBB8CCB4342F2D86D841610AA6F236";

        String[] splitArr = url.split("&");
        System.out.println(Arrays.toString(splitArr));
        Map<String, String> data = new HashMap<>(splitArr.length * 2);
        for (String str : splitArr) {
            int index = str.indexOf("=");
            String key = str.substring(0, index);
            String value = str.substring(index+1);
            data.put(key,value);
        }
        System.out.println(data);*/

        String json="{\n" +
                "    \"ret\": 0,\n" +
                "    \"msg\": \"\",\n" +
                "    \"is_lost\":0,\n" +
                "    \"nickname\": \"Lazy\",\n" +
                "    \"gender\": \"男\",\n" +
                "    \"gender_type\": 1,\n" +
                "    \"province\": \"广东\",\n" +
                "    \"city\": \"广州\",\n" +
                "    \"year\": \"1998\",\n" +
                "    \"constellation\": \"\",\n" +
                "    \"figureurl\": \"http:\\/\\/qzapp.qlogo.cn\\/qzapp\\/101806545\\/7E802DFEE788E7678D7ED1DDBCEFD2B3\\/30\",\n" +
                "    \"figureurl_1\": \"http:\\/\\/qzapp.qlogo.cn\\/qzapp\\/101806545\\/7E802DFEE788E7678D7ED1DDBCEFD2B3\\/50\",\n" +
                "    \"figureurl_2\": \"http:\\/\\/qzapp.qlogo.cn\\/qzapp\\/101806545\\/7E802DFEE788E7678D7ED1DDBCEFD2B3\\/100\",\n" +
                "    \"figureurl_qq_1\": \"http://thirdqq.qlogo.cn/g?b=oidb&k=CyiaxsQV3kl7ATFHicIz3SAg&s=40&t=1556709280\",\n" +
                "    \"figureurl_qq_2\": \"http://thirdqq.qlogo.cn/g?b=oidb&k=CyiaxsQV3kl7ATFHicIz3SAg&s=100&t=1556709280\",\n" +
                "    \"figureurl_qq\": \"http://thirdqq.qlogo.cn/g?b=oidb&k=CyiaxsQV3kl7ATFHicIz3SAg&s=640&t=1556709280\",\n" +
                "    \"figureurl_type\": \"1\",\n" +
                "    \"is_yellow_vip\": \"0\",\n" +
                "    \"vip\": \"0\",\n" +
                "    \"yellow_vip_level\": \"0\",\n" +
                "    \"level\": \"0\",\n" +
                "    \"is_yellow_year_vip\": \"0\"\n" +
                "}";

        QQUserInfo qqUserInfo = JSONObject.parseObject(json, QQUserInfo.class);
        System.out.println(qqUserInfo);
    }
}
