package com.javaczh.security.entity.mapstruct;

import com.javaczh.security.entity.dto.PermissionDto;
import com.javaczh.security.entity.model.SysPermission;
import com.javaczh.security.entity.vo.PermissionNodeVo;
import com.javaczh.security.entity.vo.PermissionVo;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-07-08T15:20:43+0800",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_202 (Oracle Corporation)"
)
public class PermissionMapStructImpl implements PermissionMapStruct {

    @Override
    public SysPermission voToModel(PermissionVo permissionVo) {
        if ( permissionVo == null ) {
            return null;
        }

        SysPermission sysPermission = new SysPermission();

        sysPermission.setId( permissionVo.getId() );
        sysPermission.setCode( permissionVo.getCode() );
        sysPermission.setName( permissionVo.getName() );
        sysPermission.setPerms( permissionVo.getPerms() );
        sysPermission.setUrl( permissionVo.getUrl() );
        sysPermission.setMethod( permissionVo.getMethod() );
        sysPermission.setPid( permissionVo.getPid() );
        sysPermission.setOrderNum( permissionVo.getOrderNum() );
        sysPermission.setType( permissionVo.getType() );
        sysPermission.setStatus( permissionVo.getStatus() );
        sysPermission.setCreateTime( permissionVo.getCreateTime() );
        sysPermission.setUpdateTime( permissionVo.getUpdateTime() );
        sysPermission.setDeleted( permissionVo.getDeleted() );
        sysPermission.setPidName( permissionVo.getPidName() );
        sysPermission.setIcon( permissionVo.getIcon() );

        return sysPermission;
    }

    @Override
    public PermissionVo modelToVo(SysPermission sysPermission) {
        if ( sysPermission == null ) {
            return null;
        }

        PermissionVo permissionVo = new PermissionVo();

        permissionVo.setId( sysPermission.getId() );
        permissionVo.setCode( sysPermission.getCode() );
        permissionVo.setName( sysPermission.getName() );
        permissionVo.setPerms( sysPermission.getPerms() );
        permissionVo.setUrl( sysPermission.getUrl() );
        permissionVo.setMethod( sysPermission.getMethod() );
        permissionVo.setPid( sysPermission.getPid() );
        permissionVo.setOrderNum( sysPermission.getOrderNum() );
        permissionVo.setType( sysPermission.getType() );
        permissionVo.setStatus( sysPermission.getStatus() );
        permissionVo.setCreateTime( sysPermission.getCreateTime() );
        permissionVo.setUpdateTime( sysPermission.getUpdateTime() );
        permissionVo.setDeleted( sysPermission.getDeleted() );
        permissionVo.setIcon( sysPermission.getIcon() );
        permissionVo.setPidName( sysPermission.getPidName() );

        return permissionVo;
    }

    @Override
    public SysPermission dtoToModel(PermissionDto permissionDto) {
        if ( permissionDto == null ) {
            return null;
        }

        SysPermission sysPermission = new SysPermission();

        sysPermission.setId( permissionDto.getId() );
        sysPermission.setCode( permissionDto.getCode() );
        sysPermission.setName( permissionDto.getName() );
        sysPermission.setPerms( permissionDto.getPerms() );
        sysPermission.setUrl( permissionDto.getUrl() );
        sysPermission.setMethod( permissionDto.getMethod() );
        sysPermission.setPid( permissionDto.getPid() );
        sysPermission.setOrderNum( permissionDto.getOrderNum() );
        sysPermission.setType( permissionDto.getType() );
        sysPermission.setStatus( permissionDto.getStatus() );
        sysPermission.setIcon( permissionDto.getIcon() );

        return sysPermission;
    }

    @Override
    public PermissionNodeVo voToNodeVo(PermissionVo permissionVo) {
        if ( permissionVo == null ) {
            return null;
        }

        PermissionNodeVo permissionNodeVo = new PermissionNodeVo();

        permissionNodeVo.setId( permissionVo.getId() );
        permissionNodeVo.setUrl( permissionVo.getUrl() );
        permissionNodeVo.setIcon( permissionVo.getIcon() );

        return permissionNodeVo;
    }

    @Override
    public List<PermissionVo> modelListToVoList(List<SysPermission> permissionList) {
        if ( permissionList == null ) {
            return null;
        }

        List<PermissionVo> list = new ArrayList<PermissionVo>( permissionList.size() );
        for ( SysPermission sysPermission : permissionList ) {
            list.add( modelToVo( sysPermission ) );
        }

        return list;
    }
}
