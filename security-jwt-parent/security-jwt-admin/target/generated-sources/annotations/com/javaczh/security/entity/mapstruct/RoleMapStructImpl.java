package com.javaczh.security.entity.mapstruct;

import com.javaczh.security.entity.PageResult;
import com.javaczh.security.entity.dto.RoleDto;
import com.javaczh.security.entity.dto.RoleQueryDto;
import com.javaczh.security.entity.model.SysRole;
import com.javaczh.security.entity.vo.RoleVo;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-07-08T15:20:43+0800",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_202 (Oracle Corporation)"
)
public class RoleMapStructImpl implements RoleMapStruct {

    @Override
    public SysRole voToModel(RoleVo roleVo) {
        if ( roleVo == null ) {
            return null;
        }

        SysRole sysRole = new SysRole();

        sysRole.setId( roleVo.getId() );
        sysRole.setName( roleVo.getName() );
        sysRole.setDescription( roleVo.getDescription() );
        if ( roleVo.getStatus() != null ) {
            sysRole.setStatus( String.valueOf( roleVo.getStatus() ) );
        }
        sysRole.setCreateTime( roleVo.getCreateTime() );
        sysRole.setUpdateTime( roleVo.getUpdateTime() );
        sysRole.setDeleted( roleVo.getDeleted() );

        return sysRole;
    }

    @Override
    public RoleVo modelToVo(SysRole sysRole) {
        if ( sysRole == null ) {
            return null;
        }

        RoleVo roleVo = new RoleVo();

        roleVo.setId( sysRole.getId() );
        roleVo.setName( sysRole.getName() );
        roleVo.setDescription( sysRole.getDescription() );
        if ( sysRole.getStatus() != null ) {
            roleVo.setStatus( Integer.parseInt( sysRole.getStatus() ) );
        }
        roleVo.setCreateTime( sysRole.getCreateTime() );
        roleVo.setUpdateTime( sysRole.getUpdateTime() );
        roleVo.setDeleted( sysRole.getDeleted() );

        return roleVo;
    }

    @Override
    public SysRole dtoToModel(RoleDto roleDto) {
        if ( roleDto == null ) {
            return null;
        }

        SysRole sysRole = new SysRole();

        sysRole.setId( roleDto.getId() );
        sysRole.setName( roleDto.getName() );
        sysRole.setDescription( roleDto.getDescription() );
        sysRole.setStatus( roleDto.getStatus() );

        return sysRole;
    }

    @Override
    public RoleVo queryDtoToModel(RoleQueryDto roleQueryDto) {
        if ( roleQueryDto == null ) {
            return null;
        }

        RoleVo roleVo = new RoleVo();

        if ( roleQueryDto.getStatus() != null ) {
            roleVo.setStatus( Integer.parseInt( roleQueryDto.getStatus() ) );
        }

        return roleVo;
    }

    @Override
    public PageResult<RoleVo> pageResultToVo(PageResult<SysRole> pageResult) {
        if ( pageResult == null ) {
            return null;
        }

        PageResult<RoleVo> pageResult1 = new PageResult<RoleVo>();

        pageResult1.setTotalRows( pageResult.getTotalRows() );
        pageResult1.setTotalPages( pageResult.getTotalPages() );
        pageResult1.setPageNum( pageResult.getPageNum() );
        pageResult1.setPageSize( pageResult.getPageSize() );
        pageResult1.setCurPageSize( pageResult.getCurPageSize() );
        pageResult1.setList( modelListToVoList( pageResult.getList() ) );

        return pageResult1;
    }

    @Override
    public List<RoleVo> modelListToVoList(List<SysRole> roleList) {
        if ( roleList == null ) {
            return null;
        }

        List<RoleVo> list = new ArrayList<RoleVo>( roleList.size() );
        for ( SysRole sysRole : roleList ) {
            list.add( modelToVo( sysRole ) );
        }

        return list;
    }
}
