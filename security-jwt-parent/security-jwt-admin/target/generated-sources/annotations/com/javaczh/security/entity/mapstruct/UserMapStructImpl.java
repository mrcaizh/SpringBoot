package com.javaczh.security.entity.mapstruct;

import com.javaczh.security.entity.PageResult;
import com.javaczh.security.entity.dto.UserDto;
import com.javaczh.security.entity.model.SysUser;
import com.javaczh.security.entity.vo.UserVO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-07-08T15:20:43+0800",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_202 (Oracle Corporation)"
)
public class UserMapStructImpl implements UserMapStruct {

    @Override
    public SysUser voToModel(UserVO userVO) {
        if ( userVO == null ) {
            return null;
        }

        SysUser sysUser = new SysUser();

        sysUser.setId( userVO.getId() );
        sysUser.setUsername( userVO.getUsername() );
        sysUser.setPhone( userVO.getPhone() );
        sysUser.setDeptId( userVO.getDeptId() );
        sysUser.setRealName( userVO.getRealName() );
        sysUser.setNickName( userVO.getNickName() );
        sysUser.setEmail( userVO.getEmail() );
        sysUser.setStatus( userVO.getStatus() );
        sysUser.setSex( userVO.getSex() );
        sysUser.setDeleted( userVO.getDeleted() );
        sysUser.setCreateId( userVO.getCreateId() );
        sysUser.setUpdateId( userVO.getUpdateId() );
        sysUser.setCreateWhere( userVO.getCreateWhere() );
        sysUser.setCreateTime( userVO.getCreateTime() );
        sysUser.setUpdateTime( userVO.getUpdateTime() );

        return sysUser;
    }

    @Override
    public UserVO modelToVo(SysUser sysUser) {
        if ( sysUser == null ) {
            return null;
        }

        UserVO userVO = new UserVO();

        userVO.setUsername( sysUser.getUsername() );
        userVO.setId( sysUser.getId() );
        userVO.setPhone( sysUser.getPhone() );
        userVO.setDeptId( sysUser.getDeptId() );
        userVO.setRealName( sysUser.getRealName() );
        userVO.setNickName( sysUser.getNickName() );
        userVO.setEmail( sysUser.getEmail() );
        userVO.setStatus( sysUser.getStatus() );
        userVO.setSex( sysUser.getSex() );
        userVO.setDeleted( sysUser.getDeleted() );
        userVO.setCreateId( sysUser.getCreateId() );
        userVO.setUpdateId( sysUser.getUpdateId() );
        userVO.setCreateWhere( sysUser.getCreateWhere() );
        userVO.setCreateTime( sysUser.getCreateTime() );
        userVO.setUpdateTime( sysUser.getUpdateTime() );

        return userVO;
    }

    @Override
    public SysUser dtoToModel(UserDto userDto) {
        if ( userDto == null ) {
            return null;
        }

        SysUser sysUser = new SysUser();

        sysUser.setId( userDto.getId() );
        sysUser.setUsername( userDto.getUsername() );
        sysUser.setPassword( userDto.getPassword() );
        sysUser.setPhone( userDto.getPhone() );
        sysUser.setDeptId( userDto.getDeptId() );
        sysUser.setRealName( userDto.getRealName() );
        sysUser.setNickName( userDto.getNickName() );
        sysUser.setEmail( userDto.getEmail() );
        sysUser.setStatus( userDto.getStatus() );
        sysUser.setSex( userDto.getSex() );
        sysUser.setDeleted( userDto.getDeleted() );
        sysUser.setCreateId( userDto.getCreateId() );
        sysUser.setUpdateId( userDto.getUpdateId() );
        sysUser.setCreateWhere( userDto.getCreateWhere() );
        sysUser.setCreateTime( userDto.getCreateTime() );
        sysUser.setUpdateTime( userDto.getUpdateTime() );

        return sysUser;
    }

    @Override
    public PageResult<UserVO> pageResultToVo(PageResult<SysUser> pageResult) {
        if ( pageResult == null ) {
            return null;
        }

        PageResult<UserVO> pageResult1 = new PageResult<UserVO>();

        pageResult1.setTotalRows( pageResult.getTotalRows() );
        pageResult1.setTotalPages( pageResult.getTotalPages() );
        pageResult1.setPageNum( pageResult.getPageNum() );
        pageResult1.setPageSize( pageResult.getPageSize() );
        pageResult1.setCurPageSize( pageResult.getCurPageSize() );
        pageResult1.setList( sysUserListToUserVOList( pageResult.getList() ) );

        return pageResult1;
    }

    protected List<UserVO> sysUserListToUserVOList(List<SysUser> list) {
        if ( list == null ) {
            return null;
        }

        List<UserVO> list1 = new ArrayList<UserVO>( list.size() );
        for ( SysUser sysUser : list ) {
            list1.add( modelToVo( sysUser ) );
        }

        return list1;
    }
}
