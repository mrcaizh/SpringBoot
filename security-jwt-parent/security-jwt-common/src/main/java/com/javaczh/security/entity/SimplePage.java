package com.javaczh.security.entity;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName SimplePage
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/25 11:06
 * @Version 1.0
 */
@Data
public class SimplePage {
    @ApiModelProperty(value = "当前第几页")
    private Integer pageNum = 1;
    @ApiModelProperty(value = "当前页数量")
    private Integer pageSize = 10;
}
