package com.javaczh.security.serializer;

import com.alibaba.fastjson.JSONObject;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName SimpleRedisSerializer
 * @Description 自定义Redis序列化
 * @Author CaiZiHao
 * @Date 2020/5/20 14:17
 * @Version 1.0
 */
public class SimpleRedisSerializer implements RedisSerializer<Object> {
    private final Charset charset;


    public static final SimpleRedisSerializer US_ASCII = new SimpleRedisSerializer(StandardCharsets.US_ASCII);


    public static final SimpleRedisSerializer ISO_8859_1 = new SimpleRedisSerializer(StandardCharsets.ISO_8859_1);


    public static final SimpleRedisSerializer UTF_8 = new SimpleRedisSerializer(StandardCharsets.UTF_8);

    public SimpleRedisSerializer() {
        this(StandardCharsets.UTF_8);
    }

    public SimpleRedisSerializer(Charset charset) {
        Assert.notNull(charset, "Charset must not be null!");
        this.charset = charset;
    }

    /**
     * 主要重写的方法
     */
    @Override
    public byte[] serialize(@Nullable Object object) throws SerializationException {
        if (object == null) {
            return new byte[0];
        }
        if (object instanceof String) {
            return ((String) object).getBytes(charset);

        } else {
            return JSONObject.toJSONString(object).getBytes(charset);
        }
    }

    @Override
    public String deserialize(@Nullable byte[] bytes) {
        return (bytes == null ? null : new String(bytes, charset));
    }


    @Override
    public Class<?> getTargetType() {
        return String.class;
    }

}
