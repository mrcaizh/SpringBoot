package com.javaczh.security.exception.code;


/**
 * @Description:
 * @Author:CaiZiHao
 * @Date:2020/5/20 15:26
 */
public interface ResponseCodeInterface {
    Integer getCode();

    String getMsg();
}
