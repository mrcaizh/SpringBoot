package com.javaczh.security.entity;


import com.javaczh.security.exception.code.BaseResponseCode;
import com.javaczh.security.exception.code.ResponseCodeInterface;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName Result
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/20 15:28
 * @Version 1.0
 */
@Data
public class Result<T> {
    /**
     * 请求响应code， 0表示请求成功 其它表示失败
     */
    @ApiModelProperty(value = "请求响应code，0为成功 其他为失败")
    private int code;

    /**
     * 响应客户端的提示
     */
    @ApiModelProperty(value = "响应异常码详细信息")
    private String msg;

    /**
     * 响应客户端内容
     */
    @ApiModelProperty(value = "响应客户端内容")
    private T data;

    public Result(int code, T data) {
        this.code = code;
        this.data = data;
        this.msg = null;
    }

    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    public Result() {
        this.code = BaseResponseCode.SUCCESS.getCode();
        this.msg = BaseResponseCode.SUCCESS.getMsg();
        this.data = null;
    }

    public Result(T data) {
        this.data = data;
        this.code = BaseResponseCode.SUCCESS.getCode();
        this.msg = BaseResponseCode.SUCCESS.getMsg();
    }

    public Result(ResponseCodeInterface responseCodeInterface) {
        this.data = null;
        this.code = responseCodeInterface.getCode();
        this.msg = responseCodeInterface.getMsg();
    }

    public Result(ResponseCodeInterface responseCodeInterface, T data) {
        this.data = data;
        this.code = responseCodeInterface.getCode();
        this.msg = responseCodeInterface.getMsg();
    }

    /**
     * 操作成功 data为null
     */
    public static <T> Result success() {
        return new <T>Result();
    }

    /**
     * 操作成功 data 不为null
     */
    public static <T> Result success(T data) {
        return new <T>Result(data);
    }

    /**
     * 自定义 返回操作 data 可控
     */
    public static <T> Result getResult(int code, String msg, T data) {
        return new <T>Result(code, msg, data);
    }

    /**
     * 自定义返回  data为null
     */
    public static <T> Result getResult(int code, String msg) {
        return new <T>Result(code, msg);
    }

    /**
     * 自定义返回 入参一般是异常code枚举 data为空
     */
    public static <T> Result getResult(BaseResponseCode responseCode) {
        return new <T>Result(responseCode);
    }

    /**
     * 自定义返回 入参一般是异常code枚举 data 可控
     */
    public static <T> Result getResult(BaseResponseCode responseCode, T data) {
        return new <T>Result(responseCode, data);
    }
}
