package com.javaczh.security.utils;

import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * @ClassName LocalDateTimeUtils
 * @Description 时间工具类
 * LocalDate ：表示日期，包含年月日，格式为 2019-10-16
 * LocalTime ：表示时间，包含时分秒，格式为 16:38:54.158549300
 * LocalDateTime ：表示日期时间，包含年月日，时分秒，格式为 2018-09-06T15:33:56.750
 * DateTimeFormatter ：日期时间格式化类。
 * Instant：时间戳，表示一个特定的时间瞬间。
 * Duration：用于计算2个时间(LocalTime，时分秒)的距离
 * Period：用于计算2个日期(LocalDate，年月日)的距离
 * ZonedDateTime ：包含时区的时间
 * Java中使用的历法是ISO 8601日历系统，它是世界民用历法，也就是我们所说的公历。平年有365天，闰年是366
 * 天。此外Java 8还提供了4套其他历法，分别是：
 * ThaiBuddhistDate：泰国佛教历
 * MinguoDate：中华民国历
 * JapaneseDate：日本历
 * HijrahDate：伊斯兰历
 * @Author CaiZiHao
 * @Date 2020/6/22 9:35
 * @Version 1.0
 */
public class LocalDateTimeUtils {

    private final static DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * LocalDateTime 转字符串
     * 2020-06-22T09:41:06.282 转成 2020-06-22 09:41:06
     *
     * @param localDateTime
     * @return
     */
    public static String toString(LocalDateTime localDateTime) {
        return DEFAULT_FORMATTER.format(localDateTime);
    }

    /**
     * LocalDateTime指定规则转字符串
     * 2020-06-22T09:41:06.282 转成 2020-06-22 09:41:06
     *
     * @param localDateTime
     * @param style
     * @return
     */
    public static String toString(LocalDateTime localDateTime, DateTimeStyle style) {
        isNotNull(localDateTime, "参数:LocalDateTime 不能为空!");
        return style == null ? DEFAULT_FORMATTER.format(localDateTime) :
                DateTimeFormatter.ofPattern(style.getValue()).format(localDateTime);
    }

    /**
     * 字符串转 LocalDateTime
     *
     * @param dateStr
     * @return
     */
    private static LocalDateTime toLocalDateTime(String dateStr) {
        isNotNull(dateStr, "参数:dateStr字符串不能为空!");
        return LocalDateTime.parse(dateStr);
    }

    /**
     * 字符串转 LocalDateTime
     *
     * @param dateStr
     * @return
     */
    private static LocalDateTime toLocalDateTime(String dateStr, DateTimeStyle style) {
        isNotNull(dateStr, "参数:dateStr字符串不能为空!");
        return style == null ? LocalDateTime.parse(dateStr, DEFAULT_FORMATTER) :
                LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(style.getValue()));
    }

    /**
     * LocalDateTime转换为Date
     * 1.使用atZone（）方法将LocalDateTime转换为ZonedDateTime
     * 2.将ZonedDateTime转换为Instant，并从中获取Date
     *
     * @return
     */
    public static Date toDate(LocalDateTime localDateTime) {
        isNotNull(localDateTime, "参数:LocalDateTime 不能为空!");
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneOffset.ofHours(8));
        return Date.from(zonedDateTime.toInstant());
    }

    /**
     * Date转换为LocalDateTime
     * 1.从日期获取ZonedDateTime并使用其方法toLocalDateTime（）获取LocalDateTime
     * 2.使用LocalDateTime的Instant（）工厂方法
     * 注: Timestamp 转 LocalDateTime 也是同样的思路
     *
     * @param date
     * @return
     */
    private static LocalDateTime toLocalDateTime(Date date) {
        isNotNull(date, "参数:Date 不能为空!");
        return date.toInstant().atZone(ZoneOffset.ofHours(8)).toLocalDateTime();
    }

    public static void isNotNull(Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }


    public static void main(String[] args) {
/*        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        LocalDateTime localDateTime = now.plusHours(5);
        System.out.println(toString(now));
        System.out.println(toString(localDateTime));
        //   System.out.println(localDateTimeToString(now, DateTimeStyle.YYYY_MM_DD_CN));
        System.out.println(Objects.isNull(null));*/


     /*   LocalDateTime date = toLocalDateTime("2020/08/01",DateTimeStyle.YYYY_MM);
        LocalDateTime localDateTime = date.plusMonths(1);
        System.out.println(toString(localDateTime));
*/


            //获取一个月的时间
            Calendar rightNow = Calendar.getInstance();
            rightNow.setTime(new Date());
            rightNow.add(Calendar.MONTH, 1);

            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date returnDate = rightNow.getTime()  ;



        System.out.println(returnDate);
       /* String format = formatter.format(LocalDateTime.now());
        System.out.println(format);

        String dateStr = "2019-05-20 10:02:16";
        LocalDateTime time = LocalDateTime.parse(dateStr, formatter);
        System.out.println(time);

        //获取具体的年月日
        System.out.println("年份:" + time.getYear());
        System.out.println("月份: " + time.getMonthValue());
        System.out.println("日: " + time.getDayOfMonth());

        //在当前年分添加一年一月一日
        System.out.println(
                "当前年分添加一年一月一日: " +
                        formatter.format(time.plusYears(1).plusMonths(1).plusDays(1)));*/
    }

    /**
     * @Description: 时间格式化类型
     * @Author:CaiZiHao
     * @Date:2020/6/22 10:03
     */
    public enum DateTimeStyle {
        MM_DD("MM-dd"),
        YYYY_MM("yyyy-MM"),
        YYYY_MM_DD("yyyy-MM-dd"),
        MM_DD_HH_MM("MM-dd HH:mm"),
        MM_DD_HH_MM_SS("MM-dd HH:mm:ss"),
        YYYY_MM_DD_HH_MM("yyyy-MM-dd HH:mm"),
        YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss"),
        YYYYMMDD("yyyyMMdd"),
        YYYYMMDDHHMMSS("yyyy-MM-dd HH:mm:ss"),
        YYYYMMDDHHMMSSSSSS("yyyyMMddHHmmssssss"),

        MM_DD_EN("MM/dd"),
        YYYY_MM_EN("yyyy/MM"),
        YYYY_MM_DD_EN("yyyy/MM/dd"),
        MM_DD_HH_MM_EN("MM/dd HH:mm"),
        MM_DD_HH_MM_SS_EN("MM/dd HH:mm:ss"),
        YYYY_MM_DD_HH_MM_EN("yyyy/MM/dd HH:mm"),
        YYYY_MM_DD_HH_MM_SS_EN("yyyy/MM/dd HH:mm:ss"),

        MM_DD_CN("MM月dd日"),
        YYYY_MM_CN("yyyy年MM月"),
        YYYY_MM_DD_CN("yyyy年MM月dd日"),
        MM_DD_HH_MM_CN("MM月dd日 HH:mm"),
        MM_DD_HH_MM_SS_CN("MM月dd日 HH:mm:ss"),
        YYYY_MM_DD_HH_MM_CN("yyyy年MM月dd日 HH:mm"),
        YYYY_MM_DD_HH_MM_SS_CN("yyyy年MM月dd日 HH:mm:ss"),

        HH_MM("HH:mm"),
        HH_MM_SS("HH:mm:ss"),

        //会计期间格式
        YYYYMM("yyyyMM");

        private String value;

        DateTimeStyle(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
