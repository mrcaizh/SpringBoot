package com.javaczh.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName JspApp
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/21 15:45
 * @Version 1.0
 */
@SpringBootApplication
public class JspApp {
    public static void main(String[] args) {
        SpringApplication.run(JspApp.class, args);
    }
}
