package com.javaczh.admin.comtroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @ClassName IndexController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/21 15:47
 * @Version 1.0
 */
@Controller
public class IndexController {
    @GetMapping("index")
    public String index() {
        return "test";
    }
}
