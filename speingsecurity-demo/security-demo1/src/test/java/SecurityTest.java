import com.javaczh.system.security.SecurityApp;
import com.javaczh.system.security.service.PermissionService2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ClassName SecurityTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/12 14:40
 * @Version 1.0
 */
@SpringBootTest(classes = SecurityApp.class)
public class SecurityTest {

    @Autowired
    private PermissionService2 permissionService;
    @Test
    public void test(){
        System.out.println(permissionService.selectAllPermission());
    }

}
