package com.javaczh.system.security.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import java.time.Duration;

/**
 * @ClassName TokenSettings
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 14:36
 * @Version 1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "jwt")
@Validated
public class TokenSettings {
    private String secretKey;
    private Duration accessTokenExpireTime;
    private Duration refreshTokenExpireTime;
    private Duration refreshTokenExpireAppTime;
    private String issuer;
}
