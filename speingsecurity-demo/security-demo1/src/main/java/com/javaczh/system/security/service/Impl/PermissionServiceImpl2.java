package com.javaczh.system.security.service.Impl;

import com.javaczh.system.common.exception.BusinessException;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import com.javaczh.system.common.utils.id.IdWorker;
import com.javaczh.system.security.contants.Constant;
import com.javaczh.system.security.entity.dto.PermissionDto;
import com.javaczh.system.security.entity.mapstruct.PermissionMapStruct;
import com.javaczh.system.security.entity.model.SysPermission;
import com.javaczh.system.security.entity.model.UserInfo;
import com.javaczh.system.security.entity.vo.PermissionNodeVo;
import com.javaczh.system.security.entity.vo.PermissionVo;
import com.javaczh.system.security.entity.vo.RolePermissionVo;
import com.javaczh.system.security.mapper.SysPermissionMapper;
import com.javaczh.system.security.mapper.SysRolePermissionMapper;
import com.javaczh.system.security.service.PermissionService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName PermissionServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/27 18:07
 * @Version 1.0
 */


@Service
@Transactional
public class PermissionServiceImpl2 implements PermissionService2 {
    @Autowired
    private SysPermissionMapper sysPermissionMapper;
    @Autowired
    private IdWorker idWorker;

    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

    /**
     * 查询全部权限
     *
     * @return
     */
    @Override
    public List<PermissionVo> selectAllPermission() {
        List<PermissionVo> permissionVoList = new ArrayList<>(1);
        List<SysPermission> permissionList = sysPermissionMapper.selectAllPermission();
        if (null != permissionList && !permissionList.isEmpty()) {
            for (SysPermission permission : permissionList) {
                SysPermission parent = sysPermissionMapper.selectByPrimaryKey(permission.getPid());
                if (parent != null) {
                    permission.setPidName(parent.getName());
                }
                //设置默认顶级菜单
                if (Constant.ZERO.equals(permission.getId())) {
                    parent.setPidName(Constant.ROOT_MENUS);
                }
                PermissionVo permissionVo = PermissionMapStruct.INSTANCE.modelToVo(permission);
                permissionVo.setOpen(true);
                permissionVoList.add(permissionVo);
            }
        }
        return permissionVoList;
    }

    /**
     * 获取所有目录菜单树接口-查询到按钮
     *
     * @return
     */
    @Override
    public List<PermissionNodeVo> getMenusTreeToBtn() {
        List<PermissionVo> permissionVos = selectAllPermission();
        return getChildrenTree(permissionVos, false, new ArrayList<>(1));
    }

    /**
     * 根据角色id查询权限
     *
     * @param roleId
     * @return
     */
    @Override
    public List<PermissionNodeVo> getMenusTreeToBtnByRoleId(String roleId) {
        //  List<SysRolePermission> list = sysRolePermissionMapper.selectRolePermissionByRoleId(roleId);
        List<String> list = sysRolePermissionMapper.selectPermissionIdByRoleId(roleId);
        List<PermissionVo> permissionVos = selectAllPermission();
        //List<PermissionNodeVo> childrenTree = getChildrenTree(permissionVos, false, list);
        return getChildrenTree(permissionVos, false, list);
    }


    @Override
    public RolePermissionVo getMenusTreeToBtnByRoleId2(String roleId) {
        RolePermissionVo rolePermissionVo = new RolePermissionVo();
        //当前角色已经拥有的菜单id
        List<String> checkedIds = new ArrayList<>(1);
        rolePermissionVo.setCheckedIds(checkedIds);
        if (!StringUtils.isEmpty(roleId)) {
            //根据角色id查询菜单
            checkedIds.addAll(sysRolePermissionMapper.selectPermissionIdsByRoleId(roleId));
        }
        rolePermissionVo.setPermissionNodeVos(getChildrenTree(selectAllPermission(),
                false, new ArrayList<>(1)));
        return rolePermissionVo;
    }

    /**
     * 获取所有目录菜单树接口-查到到目录
     *
     * @return
     */
    @Override
    public List<PermissionNodeVo> getMenusTreeToDirectory() {
        //查询全部菜单
        List<PermissionVo> permissionVos = selectAllPermission();
        List<PermissionNodeVo> result = new ArrayList<>(2);
        //设置顶级菜单,方便新增的时候可以选择一级目录
        PermissionNodeVo root = new PermissionNodeVo();
        root.setId(Constant.ZERO);
        root.setTitle(Constant.ROOT_MENUS);
        //获取子菜单
        root.setChildren(getChildrenTree(permissionVos, true, new ArrayList<>(1)));
        result.add(root);
        return result;
    }

    /**
     * 递归获取子菜单树
     * 修改菜单权限树递归方法，新增一个参数flag：true(只查询目录和菜单) false(查询目录、菜单、按钮权限)
     *
     * @param allMenus
     * @return
     */
    public List<PermissionNodeVo> getChildrenTree(List<PermissionVo> allMenus, boolean flag, List<String> list) {
        List<PermissionNodeVo> childMenuVos = new ArrayList<>(1);
        if (null == allMenus || allMenus.isEmpty()) {
            return childMenuVos;
        }
        for (PermissionVo child : allMenus) {
            if (Constant.ZERO.equals(child.getPid())) {
                PermissionNodeVo childVo = new PermissionNodeVo();
                childVo.setId(child.getId());
                childVo.setIcon(child.getIcon());
                childVo.setTitle(child.getName());
                //修改数据查询回显,其他查询忽略
                if (list.contains(child.getId())) {
                    childVo.setChecked(true);
                }

                //true 只查询目录和菜单
                if (flag) {
                    childVo.setChildren(getChildExcBtn(child.getId(), allMenus));
                } else {
                    childVo.setChildren(getChildAll(child.getId(), allMenus, list));
                }
                childMenuVos.add(childVo);
            }
        }
        return childMenuVos;
    }

    /**
     * 递归遍历所以菜单,包括按钮
     *
     * @param id
     * @param allMenus
     * @return
     */
    private List<PermissionNodeVo> getChildAll(String id, List<PermissionVo> allMenus, List<String> list) {
        List<PermissionNodeVo> result = new ArrayList<>(5);
        for (PermissionVo menu : allMenus) {
            if (menu.getPid().equals(id)) {
                PermissionNodeVo childMenu = PermissionMapStruct.INSTANCE.voToNodeVo(menu);
                childMenu.setTitle(menu.getName());
                if (list.contains(childMenu.getId())) {
                    if ("1268357539595161600".equals(childMenu.getId())) {
                        childMenu.setChecked(false);
                    } else {
                        childMenu.setChecked(true);
                    }

                }
                childMenu.setChildren(getChildAll(menu.getId(), allMenus, list));
                result.add(childMenu);
            }
        }
        return result;
    }

    /**
     * 只递归获取目录和菜单,因为按钮是无法做父级菜单所以要排除按钮菜单
     *
     * @param allMenus
     * @return
     */
    public List<PermissionNodeVo> getChildExcBtn(String id, List<PermissionVo> allMenus) {
        List<PermissionNodeVo> result = new ArrayList<>(5);
        for (PermissionVo menu : allMenus) {
            if (menu.getPid().equals(id) && menu.getType() != 3) {
                //   PermissionNodeVo childMenu = new PermissionNodeVo();
                PermissionNodeVo childMenu = PermissionMapStruct.INSTANCE.voToNodeVo(menu);
                childMenu.setTitle(menu.getName());
                childMenu.setChildren(getChildExcBtn(menu.getId(), allMenus));
                result.add(childMenu);
            }
        }
        return result;
    }

    /**
     * 新增或者菜单
     *
     * @param permissionDto
     * @return
     */
    @Override
    public SysPermission addOrUpdatePermission(PermissionDto permissionDto) {
        verifyForm(permissionDto);
        int result;
        SysPermission sysPermission = PermissionMapStruct.INSTANCE.dtoToModel(permissionDto);
        if (StringUtils.isEmpty(sysPermission.getId())) {
            sysPermission.setId(idWorker.nextIdToString());
            sysPermission.setCreateTime(new Date());
            sysPermission.setUpdateTime(new Date());
            result = sysPermissionMapper.insertSelective(sysPermission);
            if (result != 1) {
                throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
            }
        } else {
            sysPermission.setUpdateTime(new Date());
            result = sysPermissionMapper.updateByPrimaryKeySelective(sysPermission);
            if (result != 1) {
                throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
            }
        }
        return sysPermission;
    }


    /**
     * 操作后的菜单类型是目录的时候  父级必须为目录
     * 操作后的菜单类型是菜单的时候，父类必须为目录类型
     * 操作后的菜单类型是按钮的时候  父类必须为菜单类型
     * (1:目录;2:菜单;3:按钮)
     *
     * @param permissionDto
     */
    private void verifyForm(PermissionDto permissionDto) {
        SysPermission parentPermission = sysPermissionMapper.selectByPrimaryKey(permissionDto.getPid());
        switch (permissionDto.getType()) {
            case 1:
                if (parentPermission != null) {
                    if (parentPermission.getType() != 1) {
                        throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_CATALOG_ERROR);
                    } else if (!Constant.ZERO.equals(permissionDto.getPid())) {
                        throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_CATALOG_ERROR);
                    }
                }
                break;
            case 2:
                //菜单的上一级必须是目录
                if (parentPermission == null || parentPermission.getType() != 1) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_MENU_ERROR);
                }
                if (StringUtils.isEmpty(permissionDto.getUrl())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_NOT_NULL);
                }
                break;
            case 3:
                //按钮的的上一级必须是菜单
                if (parentPermission == null || parentPermission.getType() != 2) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_BTN_ERROR);
                }
                if (StringUtils.isEmpty(permissionDto.getPerms())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_PERMS_NULL);
                }
                if (StringUtils.isEmpty(permissionDto.getUrl())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_NOT_NULL);
                }
                if (StringUtils.isEmpty(permissionDto.getMethod())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_METHOD_NULL);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 根据用户id获取首页导航
     *
     * @param userId
     * @return
     */
    @Override
    public List<PermissionNodeVo> permissionTreeList(String userId) {
        return getChildrenTree(selectAllPermission(), true, new ArrayList<>(1));
    }


    @Override
    public Boolean deletePermission(List<String> ids) {
        ids.forEach(id -> {
            sysPermissionMapper.deleteByPrimaryKey(id);
        });
        return true;
    }


    /**
     * 根据角色ID获取按钮权限,主要是控制前端页面的按钮显示和隐藏
     *
     * @param
     * @return
     */
    @Override
    public List<PermissionVo> getBtnPermissionByRole() {
        UserInfo loginUser = null;// UserContext.getLoginUser();
        List<SysPermission> list = sysPermissionMapper.getBtnPermissionByRole(loginUser.getId());
        List<PermissionVo> permissionVos = PermissionMapStruct.INSTANCE.modelListToVoList(list);
        return permissionVos;
    }


    @Override
    public  List<SysPermission>  getAllPermission() {
        List<SysPermission> list = sysPermissionMapper.selectAllPermissionAndRole();
        return list;
    }


}
