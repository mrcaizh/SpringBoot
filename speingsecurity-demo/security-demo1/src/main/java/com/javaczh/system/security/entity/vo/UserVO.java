package com.javaczh.system.security.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @ClassName LoginRespVO
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 15:45
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class UserVO {
    @ApiModelProperty(value = "token")
    private String accessToken;
    @ApiModelProperty(value = "刷新token")
    private String refreshToken;

    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "用户id")
    private String id;
    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "部门Id")
    private String deptId;
    @ApiModelProperty(value = "'真实名称'")
    private String realName;
    @ApiModelProperty(value = "''昵称''")
    private String nickName;
    @ApiModelProperty(value = "邮箱唯一")
    private String email;
    @ApiModelProperty(value = "账户状态(1.正常 2.锁定)")
    private Integer status;
    @ApiModelProperty(value = "性别")
    private Integer sex;
    @ApiModelProperty(value = "删除标记, 是否删除(0未删除；1已删除)")
    private Integer deleted;
    @ApiModelProperty(value = "'创建人'")
    private String createId;

    @ApiModelProperty(value = "更新人")
    private String updateId;
    @ApiModelProperty(value = "创建来源(1.web 2.android 3.ios )")
    private Integer createWhere;
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
    @ApiModelProperty(value = "部门名称")
    private String deptName;
}
