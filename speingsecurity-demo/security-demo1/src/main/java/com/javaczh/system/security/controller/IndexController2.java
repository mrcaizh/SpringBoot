package com.javaczh.system.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName IndexController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/21 17:40
 * @Version 1.0
 */
@RestController
public class IndexController2 {


    @RequestMapping("/admin/hello")
    public String admin() {
        return "Hello  admin";
    }

    @RequestMapping("/user/hello")
    public String user() {
        return "Hello  user";
    }

    @RequestMapping("/db/hello")
    public String db() {
        return "Hello  db";
    }

    @GetMapping("hello")
    public String hello() {
        return "Hello  World  Security";
    }

}
