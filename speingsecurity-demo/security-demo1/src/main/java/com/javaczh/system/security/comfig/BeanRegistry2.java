package com.javaczh.system.security.comfig;

import com.javaczh.system.common.utils.id.IdWorker;
import com.javaczh.system.common.utils.redis.RedisService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName BeanRegistry
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/1 15:20
 * @Version 1.0
 */
@Configuration
public class BeanRegistry2 {

    @Bean
    public IdWorker idWorker() {
        return new IdWorker(1, 1);
    }

}
