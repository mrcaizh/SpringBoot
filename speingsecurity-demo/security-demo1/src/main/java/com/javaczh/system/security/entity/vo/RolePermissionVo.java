package com.javaczh.system.security.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * @ClassName RolePermissionVo
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/5 0:28
 * @Version 1.0
 */
@Data
public class RolePermissionVo {

    /**
     * 当前角色已经拥有的菜单id
     */
    private List<String> checkedIds;

    /**
     * 全部菜单树
     */
    List<PermissionNodeVo> permissionNodeVos;
}
