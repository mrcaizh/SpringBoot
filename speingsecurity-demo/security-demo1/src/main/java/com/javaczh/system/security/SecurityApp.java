package com.javaczh.system.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName SecurityApp
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/12 13:55
 * @Version 1.0
 */
@SpringBootApplication
public class SecurityApp {
    public static void main(String[] args) {
        SpringApplication.run(SecurityApp.class, args);
    }
}
