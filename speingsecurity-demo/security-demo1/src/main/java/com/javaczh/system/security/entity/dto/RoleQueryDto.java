package com.javaczh.system.security.entity.dto;

import com.javaczh.system.common.entity.SimplePage;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @ClassName RoleQueryDto
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/2 14:21
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class RoleQueryDto {

    private String roleId;
    private String roleName;
    private String status;
    private Date startTime;
    private Date endTime;
    private SimplePage simplePage=new SimplePage();
//    pageNum:1,
//    pageSize:10,
//    roleId:null,
//    roleName:null,
//    status:null,
//    startTime:null,
//    endTime:null
}
