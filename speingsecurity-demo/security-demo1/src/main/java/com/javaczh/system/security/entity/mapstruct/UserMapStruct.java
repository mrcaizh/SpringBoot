package com.javaczh.system.security.entity.mapstruct;


import com.javaczh.system.security.entity.dto.UserDto;
import com.javaczh.system.security.entity.model.SysUser;
import com.javaczh.system.security.entity.vo.UserVO;
import com.javaczh.system.common.entity.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @ClassName UserMapStruct
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 15:54
 * @Version 1.0
 */
@Mapper
public interface UserMapStruct {

    UserMapStruct INSTANCE = Mappers.getMapper(UserMapStruct.class);

    /**
     * vo转实体
     *
     * @param userVO
     * @return
     */

    SysUser voToModel(UserVO userVO);

    /**
     * 实体转vo
     *
     * @param sysUser
     * @return
     */
    UserVO modelToVo(SysUser sysUser);

    /**
     * dto转实体
     *
     * @param userDto
     * @return
     */
    SysUser dtoToModel(UserDto userDto);

    PageResult<UserVO> pageResultToVo(PageResult<SysUser> pageResult);
}
