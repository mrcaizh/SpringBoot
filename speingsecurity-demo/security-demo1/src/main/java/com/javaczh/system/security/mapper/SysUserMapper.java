package com.javaczh.system.security.mapper;


import com.javaczh.system.security.entity.model.SysRole;
import com.javaczh.system.security.entity.model.SysUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    SysUser loadUserByUsername(String username);

    List<SysRole> getUserRolesByUserId(String id);
}