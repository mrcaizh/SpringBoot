package com.javaczh.system.security.service.Impl;


import com.alibaba.fastjson.JSONObject;
import com.javaczh.system.security.contants.Constant;
import com.javaczh.system.security.entity.mapstruct.UserMapStruct;
import com.javaczh.system.security.entity.model.SysPermission;
import com.javaczh.system.security.entity.model.SysUser;
import com.javaczh.system.security.entity.vo.UserVO;
import com.javaczh.system.security.mapper.SysRoleMapper;
import com.javaczh.system.security.mapper.SysRolePermissionMapper;
import com.javaczh.system.security.mapper.SysUserMapper;
import com.javaczh.system.security.mapper.SysUserRoleMapper;
import com.javaczh.system.security.service.UserService2;
import com.javaczh.system.security.utils.JwtTokenUtils;
import com.javaczh.system.common.exception.BusinessException;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import com.javaczh.system.common.utils.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName UserServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/10 12:40
 * @Version 1.0
 */
@Service
public class UserServiceImpl2 implements UserService2, UserDetailsService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RedisService redisService;
//    @Autowired
//    private IdWorker idWorker;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = sysUserMapper.loadUserByUsername(username);
        if (null == user) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_ERROR);
        }
        System.out.println(passwordEncoder.encode("123456"));
        user.setRoles(sysUserMapper.getUserRolesByUserId(user.getId()));
        //user.setRoles()
        /*  User user = userMapper.loadUserByUsername(username);
        if (null == user) {
            throw new UsernameNotFoundException("用户不存在");
        }
        user.setRoles(userMapper.getUserRolesByUid(user.getId()));*/
        return user;
    }

    @Override
    public UserVO generatorToken(SysUser sysUser) {
        UserVO vo = UserMapStruct.INSTANCE.modelToVo(sysUser);
        //进行token签发
        Map<String, Object> claims = new HashMap<>(3);
        claims.put(Constant.ROLES_INFOS_KEY, getRolesByUserId(sysUser.getId()));
        claims.put(Constant.PERMISSIONS_INFOS_KEY, getPermissionsByUserId(sysUser.getId()));
        claims.put(Constant.JWT_USER_NAME, sysUser.getUsername());
        claims.put(Constant.JWT_USER_INFO_KEY, JSONObject.toJSONString(sysUser));
        //根据用户id生成认证token
        String accessToken = JwtTokenUtils.getAccessToken(sysUser.getId(), claims);
        //根据用户id生成刷新token,app端生成的token的失效长
        String refreshToken;
        refreshToken = JwtTokenUtils.getRefreshToken(sysUser.getId(), claims);
        /**   
         * 异地登录失效逻辑     
         */
        redisService.set(sysUser.getId(), accessToken, 60, TimeUnit.MINUTES);
        vo.setAccessToken(accessToken);
        vo.setRefreshToken(refreshToken);
        return vo;
    }

    /**
     * mock 数据
     * 通过用户id获取该用户所拥有的角色
     * 后期修改为通过操作DB获取
     *
     * @return java.util.List<java.lang.String>
     * @throws
     */
    private List<String> getRolesByUserId(String userId) {
        List<String> roles = sysUserRoleMapper.selectRoleNamesByUserId(userId);
        return roles;
    }

    /**
     * mock 数据
     * 通过用户id获取该用户所拥有的角色
     * 后期通过操作数据获取
     *
     * @param userId
     * @return java.util.List<java.lang.String>
     * @throws
     */
    private List<String> getPermissionsByUserId(String userId) {
        List<SysPermission> list = sysRolePermissionMapper.selectPermissionUrlByUserId(userId);
        List<String> permissions = new ArrayList<>(3);
        for (SysPermission sysPermission : list) {
            permissions.add(sysPermission.getUrl());
        }
        return permissions;

    }


}
