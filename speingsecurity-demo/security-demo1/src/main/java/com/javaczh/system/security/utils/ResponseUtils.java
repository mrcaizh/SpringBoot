package com.javaczh.system.security.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaczh.system.common.entity.Result;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @ClassName ResponseUtils
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/21 17:58
 * @Version 1.0
 */
public class ResponseUtils {

    public static void simpleResponseMessage(HttpServletResponse response, Object data) throws Exception {
        Result result = new Result(data);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        response.setStatus(200);
        out.println(new ObjectMapper().writeValueAsString(result));
        out.flush();
        out.close();
    }

    public static void simpleResponseMessage(HttpServletResponse response, Integer code, String message) throws Exception {
        Result success = Result.getResult(code, message);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        response.setStatus(200);
        out.println(new ObjectMapper().writeValueAsString(success));
        out.flush();
        out.close();
    }
}
