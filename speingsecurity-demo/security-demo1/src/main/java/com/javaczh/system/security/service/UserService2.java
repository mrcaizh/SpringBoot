package com.javaczh.system.security.service;

import com.javaczh.system.security.entity.model.SysUser;
import com.javaczh.system.security.entity.vo.UserVO;

/**
 * @ClassName UserService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/10 11:09
 * @Version 1.0
 */
public interface UserService2 {
    /**
     * 生成Token
     *
     * @param sysUser
     * @return
     */
    UserVO generatorToken(SysUser sysUser);
}
