package com.javaczh.system.security.entity.mapstruct;

import com.javaczh.system.security.entity.dto.RoleDto;
import com.javaczh.system.security.entity.dto.RoleQueryDto;
import com.javaczh.system.security.entity.model.SysRole;
import com.javaczh.system.security.entity.vo.RoleVo;
import com.javaczh.system.common.entity.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @ClassName UserMapStruct
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 15:54
 * @Version 1.0
 */
@Mapper
public interface RoleMapStruct {

    RoleMapStruct INSTANCE = Mappers.getMapper(RoleMapStruct.class);

    /**
     * vo转实体
     *
     * @param roleVo
     * @return
     */

    SysRole voToModel(RoleVo roleVo);

    /**
     * 实体转vo
     *
     * @param sysRole
     * @return
     */
    RoleVo modelToVo(SysRole sysRole);
    /**
     * dto转实体
     *
     * @param roleDto
     * @return
     */
    SysRole dtoToModel(RoleDto roleDto);

    /**
     * dto转实体
     *
     * @param roleQueryDto
     * @return
     */
    RoleVo queryDtoToModel(RoleQueryDto roleQueryDto);

    PageResult<RoleVo> pageResultToVo(PageResult<SysRole> pageResult);

    List<RoleVo> modelListToVoList(List<SysRole> roleList);

}
