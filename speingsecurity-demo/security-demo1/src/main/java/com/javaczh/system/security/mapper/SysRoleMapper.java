package com.javaczh.system.security.mapper;


import com.javaczh.system.security.entity.dto.RoleQueryDto;
import com.javaczh.system.security.entity.model.SysRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysRoleMapper {
    int insert(SysRole record);

    int insertSelective(SysRole record);

    List<SysRole> selectRolesByParam(RoleQueryDto roleQueryDto);

    int updateByPrimaryKeySelective(SysRole sysRole);

    int deleteRoleByIds(List<String> ids);

    List<SysRole> selectAllRoles();

    List<String> selectRoleByUserId(String userId);
}