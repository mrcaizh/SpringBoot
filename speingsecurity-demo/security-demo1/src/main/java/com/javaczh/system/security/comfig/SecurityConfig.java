package com.javaczh.system.security.comfig;

import com.javaczh.system.security.authentication.LoginSuccessHandler;
import com.javaczh.system.security.service.UserService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @ClassName SecurityConfig
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/21 17:49
 * @Version 1.0
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService2 userService2;

    @Bean
    public PasswordEncoder passwordEncoder() {
        // return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder(10);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()//调用  authorizeRequests()方法开启HttpSecurity的配置
                .antMatchers("/admin/**")
                .hasAnyRole("admin")//用户访问  /admin/**  模式的URL必须具备admin的角色
                .antMatchers("/user/**")
                .access("hasRole('admin')  and  hasRole('user')")//用户访问  /user/**  模式的URL必须具备admin和user的角色
                .antMatchers("/db/**")
                .access("hasAnyRole('admin','dba')")  //用户访问  /db/**  模式的URL必须具备admin或user的角色
                .anyRequest().authenticated()  //任意请求都经过认证
                //开启表单登录，开启登录页面同时配直了登录接口为/login，即可以直接调用/login接口，发起一个
                //POST请求进行登录，登录参数中用户名必须命名为username密码必须命名为password
                //配置loginProcessingUrl接口主要是方便Ajax或者移动端调用登录接口
                //。最后还配置了permitAll表示和登录相关的接口都不需要认证即可访问。
                .and().formLogin()
                /**
                 *  配置自定义登录页面,用户未获取授权就访问一个需要投权才能访问的接口，就会自动跳转到  loginPage登录页面
                 *usernameParameter和passwordParameter定义了认证所需的用户名和密码的参数名，默认用户名参数是usemame密码参数是password可以在这里自定义。
                 */
                //.loginPage("/loginPage")
                .loginProcessingUrl("/login")
//                .usernameParameter("user")
//                .passwordParameter("passwd")
                .successHandler(new LoginSuccessHandler(userService2))  //登录成功的处理类
                //  .failureHandler(loginFailureHandler)//登录失败的处理类
                .permitAll()
                .and().csrf().disable();  //行表示关闭csrf

    }
}
