package com.javaczh.bootseata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName BootSeataApp
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 19:30
 * @Version 1.0
 */
@SpringBootApplication
public class BootSeataApp {
    public static void main(String[] args) {
        SpringApplication.run(BootSeataApp.class, args);
    }
}
