package com.javaczh.bootseata.service;

/**
 * @ClassName OrderService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 19:35
 * @Version 1.0
 */
public interface OrderService {
    Integer createOrder(Long userId, Long productId, Integer price) throws Exception;
}
