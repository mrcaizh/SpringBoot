package com.javaczh.bootseata.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.javaczh.bootseata.entity.Order;
import com.javaczh.bootseata.mapper.OrderMapper;
import com.javaczh.bootseata.service.AmountService;
import com.javaczh.bootseata.service.OrderService;
import com.javaczh.bootseata.service.ProductService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName OrderServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 19:35
 * @Version 1.0
 */
@Service
public class OrderServiceImpl implements OrderService {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private AmountService amountService;
    @Autowired
    private ProductService productService;

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 创建订单  先扣钱,在扣库存,然后生成订单
     * 创建 seata_order、seata_storage、seata_amount 三个库
     *该方法中，调用 ProductService 扣除商品的库存，调用 AccountService 扣除账户的余额。虽然说，调用是 JVM 进程内的，
     * 但是 ProductService 操作的是 product-ds 商品数据源，AccountService 操作的是 account-ds 账户数据源。
     * @param userId    用户编号
     * @param productId 产品编号
     * @param price     价格
     * @return 订单编号
     * @throws Exception 创建订单失败，抛出异常
     * @DS(value = "order-ds") 设置使用 order-ds 订单数据源。
     * @GlobalTransactional 注解，使用Seata声明全局事务。
     */
    @Override
    @DS(value = "order-ds")
    @GlobalTransactional
    public Integer createOrder(Long userId, Long productId, Integer price) throws Exception {
        Integer amount = 1; // 购买数量，暂时设置为 1。
        logger.info("[createOrder] 当前 XID: {}", RootContext.getXID());

        // <3> 扣减库存
        productService.reduceStock(productId, amount);
        amountService.reduceBalance(productId, amount);
        Order order = new Order();
        order.setId(8);
        order.setUserId(userId);
        order.setProductId(productId);
        order.setPayAmount(amount * price);
        orderMapper.saveOrder(order);
        return null;
    }
}
