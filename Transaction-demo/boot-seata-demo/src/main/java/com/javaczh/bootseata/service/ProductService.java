package com.javaczh.bootseata.service;

/**
 * @ClassName ProductService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 19:41
 * @Version 1.0
 */
public interface ProductService {
    void reduceStock(Long productId, Integer amount) throws Exception;
}
