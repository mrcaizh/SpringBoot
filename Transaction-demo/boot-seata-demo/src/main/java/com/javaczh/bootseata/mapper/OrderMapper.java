package com.javaczh.bootseata.mapper;

import com.javaczh.bootseata.entity.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

/**
 * @ClassName OrderMapper
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 19:47
 * @Version 1.0
 */
@Mapper
@Repository
public interface OrderMapper {

    /**
     * 插入订单记录
     *
     * @param order 订单
     * @return 影响记录数量
     */
    @Insert("INSERT INTO orders (user_id, product_id, pay_amount) VALUES (#{userId}, #{productId}, #{payAmount})")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    int saveOrder(Order order);

}