package com.javaczh.bootseata.service;

/**
 * @ClassName AmountService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 19:39
 * @Version 1.0
 */
public interface AmountService {
    void reduceBalance(Long productId, Integer amount) throws Exception;
}
