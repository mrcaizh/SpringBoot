package com.javaczh.seata.bank1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName SeataBank1App
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/8 13:04
 * @Version 1.0
 */
@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
public class SeataBank1App {
    public static void main(String[] args) {
        SpringApplication.run(SeataBank1App.class, args);
    }

}
