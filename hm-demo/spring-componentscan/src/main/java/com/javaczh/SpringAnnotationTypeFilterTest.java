package com.javaczh;


import com.javaczh.config.SpringConfiguration2;
import com.javaczh.service.DistrictPercentage;
import com.javaczh.service.DistrictPerformance;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringAnnotationTypeFilterTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(SpringConfiguration2.class);
        DistrictPerformance districtPerformance = ac.getBean("districtPerformance", DistrictPerformance.class);
        districtPerformance.calcPerformance("SUV");
        DistrictPercentage districtPercentage = ac.getBean("districtPercentage", DistrictPercentage.class);
        districtPercentage.salePercentage("car");
    }
}
