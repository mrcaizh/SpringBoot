package com.javaczh.config;

import com.javaczh.anno.DistrictTypeFilter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;

/**
 * @ClassName SpringConfiguration2
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/28 16:44
 * @Version 1.0
 */
@Configuration
@PropertySource(value = "classpath:district.properties")
@ComponentScan(value = "com.javaczh",
        excludeFilters = @ComponentScan.Filter(
                type = FilterType.CUSTOM,
                classes = DistrictTypeFilter.class))
public class SpringConfiguration2 {

}
