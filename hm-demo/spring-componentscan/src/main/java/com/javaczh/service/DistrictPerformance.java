package com.javaczh.service;

/**
 * @ClassName DistrictPerformance
 * @Description 绩效计算桥接接口
 * @Author CaiZiHao
 * @Date 2020/6/28 16:37
 * @Version 1.0
 */
public  interface DistrictPerformance {
    /**
     * 计算绩效
     * @param carType
     */
    void calcPerformance(String carType);

}
