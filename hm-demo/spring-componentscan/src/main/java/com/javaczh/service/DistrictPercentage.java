package com.javaczh.service;

/**
 * @ClassName DistrictPercentage
 * @Description 销售分成的桥接接口
 * @Author CaiZiHao
 * @Date 2020/6/28 16:37
 * @Version 1.0
 */
public interface DistrictPercentage {
    /**
     * 不同车型提成
     *
     * @param carType
     */
    void salePercentage(String carType);
}
