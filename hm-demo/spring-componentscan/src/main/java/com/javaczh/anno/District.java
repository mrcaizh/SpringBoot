package com.javaczh.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName District
 * @Description 区域的注解
 * @Author CaiZiHao
 * @Date 2020/6/28 16:35
 * @Version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface District {
    /**
     * 指定区域的名称
     *
     * @return
     */
    String value() default "";

}
