package com.javaczh;

import com.javaczh.bean.LazyBean;
import com.javaczh.config.SpringConfiguration;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @ClassName SpringTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/23 15:34
 * @Version 1.0
 */

public class SpringTest {


    @Test
    public void test2() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        for (String beanDefinitionName : app.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        LazyBean lazyBean = app.getBean("lazyBean", LazyBean.class);
        System.out.println(lazyBean);

    }

    @Test
    public void test1() {
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        JdbcTemplate jdbcTemplate = app.getBean("jdbcTemplate", JdbcTemplate.class);
        System.out.println(jdbcTemplate);
  /*      int result = jdbcTemplate.update("insert into account(name,money) values(?,?)", "test", 12345);
        System.out.println(result);*/
    }
}
