package com.javaczh.config;

import org.springframework.context.annotation.*;

/**
 * @ClassName SpringConfiguration
 * @Description spring的配置类, 用于替代xml配置
 * @Author CaiZiHao
 * @Date 2020/6/23 15:24
 * @Version 1.0
 */
@Configuration
@Import(JdbcConfig.class)
@ComponentScan("com.javaczh")
public class SpringConfiguration {


}
