package com.javaczh.config.anno;

/**
 * @ClassName District
 * @Description 区域的注解
 * @Author CaiZiHao
 * @Date 2020/6/28 16:35
 * @Version 1.0
 */
public @interface District {
    /**
     * 指定区域的名称
     *
     * @return
     */
    String value() default "";

}
