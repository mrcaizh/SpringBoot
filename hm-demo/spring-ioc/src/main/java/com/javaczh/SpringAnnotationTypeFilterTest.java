package com.javaczh;

import com.javaczh.config.SpringConfiguration2;
import com.javaczh.service.DistrictPercentage;
import com.javaczh.service.DistrictPerformance;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @ClassName SpringAnnotationTypeFilterTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/28 16:50
 * @Version 1.0
 */
public class SpringAnnotationTypeFilterTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(SpringConfiguration2.class);
        DistrictPerformance districtPerformance = ac.getBean("districtPerformance", DistrictPerformance.class);
        districtPerformance.calcPerformance("SUV");
        DistrictPercentage districtPercentage = ac.getBean("districtPercentage", DistrictPercentage.class);
        districtPercentage.salePercentage("car");
    }
}
