package com.javaczh.bean;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

/**
 * @ClassName Customer
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/23 16:17
 * @Version 1.0
 * <p>
 * spring中没有特定bean的加载顺序
 * 使用@DependsOn注解则可指定bean的加载顺序。(在基于注解配置中，是按照类中方法的书写顺序决定的)
 * 例如Customer 需要依赖customerListener这个类才能创建
 */
//@Component
//@DependsOn("customerListener")
public class Customer {
    public Customer() {
        System.out.println("事件源创建了。。。");
    }
}
