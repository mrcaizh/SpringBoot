package com.javaczh.bean;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @ClassName LazyBean
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/23 16:46
 * @Version 1.0
 */
@Component
@Lazy

public class LazyBean {
    public LazyBean(){
        System.out.println("LazyBean 创建了");
    }
}
