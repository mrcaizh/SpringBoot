package com.javaczh.bean;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @ClassName CustomerListener
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/23 16:16
 * @Version 1.0
 */
//@Component
//@Lazy
public class CustomerListener {
    public CustomerListener() {
        System.out.println("监听器创建了。。。");
    }
}
