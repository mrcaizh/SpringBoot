package com.javaczh.service.impl;

import com.javaczh.config.anno.District;
import com.javaczh.service.DistrictPercentage;
import org.springframework.stereotype.Component;

/**
 * @ClassName NorthDistrictPercentage
 * @Description 华北区销售分成具体实现
 * @Author CaiZiHao
 * @Date 2020/6/28 16:40
 * @Version 1.0
 */
@Component("districtPercentage")
@District("north")
public class NorthDistrictPercentage implements DistrictPercentage {
    @Override
    public void salePercentage(String carType) {
        if ("SUV".equalsIgnoreCase(carType)) {
            System.out.println("华北区" + carType + "提成1%");
        } else if ("car".equalsIgnoreCase(carType)) {
            System.out.println("华北区" + carType + "提成0.5%");
        }
    }
}