package com.javaczh.service.impl;

import com.javaczh.service.AccountService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @ClassName AccountServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/24 9:43
 * @Version 1.0
 */
@Service
public class AccountServiceImpl1 implements AccountService {
    @Override
    public String toString() {
        return  this.getClass().getSimpleName();
    }
}
