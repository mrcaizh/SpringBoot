package com.javaczh.service.impl;

import com.javaczh.config.anno.District;
import com.javaczh.service.DistrictPerformance;
import org.springframework.stereotype.Component;

/**
 * @ClassName SouthwestDistrictPerformance
 * @Description 西南区绩效计算具体实现
 * @Author CaiZiHao
 * @Date 2020/6/28 16:43
 * @Version 1.0
 */

@Component("districtPerformance")
@District("southwest")
public class SouthwestDistrictPerformance implements DistrictPerformance {
    @Override
    public void calcPerformance(String carType) {
        if ("SUV".equalsIgnoreCase(carType)) {
            System.out.println("西南区" + carType + "绩效5");
        } else if ("car".equalsIgnoreCase(carType)) {
            System.out.println("西南区" + carType + "绩效3");
        }
    }
}