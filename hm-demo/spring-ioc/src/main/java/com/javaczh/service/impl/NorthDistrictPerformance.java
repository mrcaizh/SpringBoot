package com.javaczh.service.impl;

import com.javaczh.config.anno.District;
import com.javaczh.service.DistrictPerformance;
import org.springframework.stereotype.Component;

/**
 * @ClassName NorthDistrictPerformance
 * @Description 华北区销售绩效具体实现
 * @Author CaiZiHao
 * @Date 2020/6/28 16:41
 * @Version 1.0
 */


@Component("districtPerformance")
@District("north")
public class NorthDistrictPerformance implements DistrictPerformance {
    @Override
    public void calcPerformance(String carType) {
        if ("SUV".equalsIgnoreCase(carType)) {
            System.out.println("华北区" + carType + "绩效3");
        } else if ("car".equalsIgnoreCase(carType)) {
            System.out.println("华北区" + carType + "绩效5");
        }
    }
}
