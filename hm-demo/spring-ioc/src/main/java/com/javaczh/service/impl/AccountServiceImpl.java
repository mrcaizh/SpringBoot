package com.javaczh.service.impl;

import com.javaczh.service.AccountService;
import org.springframework.stereotype.Service;

/**
 * @ClassName AccountServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/24 9:43
 * @Version 1.0
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Override
    public String toString() {
        return  this.getClass().getSimpleName();
    }
}
