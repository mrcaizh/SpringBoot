package com.javaczh;

import com.javaczh.config.SpringConfiguration;
import com.javaczh.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @ClassName SpringTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/23 15:34
 * @Version 1.0
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfiguration.class})
public class SpringTest2 {

    @Autowired
    private AccountService accountService;

    @Test
    public void test2() {
        System.out.println(accountService);
    }
}
