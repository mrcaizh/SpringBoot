package com.javaczh.imports;

import com.javaczh.imports.config.SpringConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @ClassName SpringAnnotationDrivenTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 13:14
 * @Version 1.0
 */
public class SpringAnnotationDrivenTest {
    public static void main(String[] args) {
        ApplicationContext ac = new
                AnnotationConfigApplicationContext(SpringConfiguration.class);
        JdbcTemplate jdbcTemplate = ac.getBean("jdbcTemplate", JdbcTemplate.class);
        System.out.println(jdbcTemplate);
    }

}
