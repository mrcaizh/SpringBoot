package com.javaczh.imports.config;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * @ClassName CustomerPropertySourceFactory
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 13:10
 * @Version 1.0
 */
public class CustomerPropertySourceFactory implements PropertySourceFactory {
    /**
     * @param name
     * @param resource
     * @return
     * @throws IOException
     */
    @Override
    public PropertySource<?> createPropertySource(String name, EncodedResource resource) throws IOException {
        //1.创建yaml文件解析工厂
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        //2.设置资源内容
        yaml.setResources(resource.getResource());
        //3.解析成properties文件
        Properties properties = yaml.getObject();
        //4.返回符合spring的PropertySource对象
        return name != null ? new PropertiesPropertySource(name, properties) :
                new PropertiesPropertySource(resource.getResource().getFilename(), properties);

    }
}
