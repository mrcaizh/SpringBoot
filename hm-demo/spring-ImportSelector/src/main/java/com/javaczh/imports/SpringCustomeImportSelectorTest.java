package com.javaczh.imports;

import com.javaczh.imports.config.SpringConfiguration;
import com.javaczh.imports.service.SpringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @ClassName SpringCustomeImportSelectorTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 9:39
 * @Version 1.0
 */
public class SpringCustomeImportSelectorTest {
    public static void main(String[] args) {
       AnnotationConfigApplicationContext ac = new
                AnnotationConfigApplicationContext(SpringConfiguration.class);
       /* String[] names = ac.getBeanDefinitionNames();
        for (String beanName : names) {
            Object obj = ac.getBean(beanName);
            System.out.println(beanName + "============ " + obj);
        }*/

        ApplicationContext applicationContext = SpringUtils.getApplicationContext();
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();

        for (String beanName : beanDefinitionNames) {
            Object obj = applicationContext.getBean(beanName);
            System.out.println(beanName + "============ " + obj);
        }

    }
}
