package com.javaczh.imports.config;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.context.annotation.*;

/**
 * @ClassName SpringConfiguration
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 9:27
 * @Version 1.0
 */
@Configuration
@ComponentScan(value = "com.javaczh.imports.service")
@Import({CustomeImportDefinitionRegistrar.class,JdbcConfig.class})
@PropertySource(value =
        "classpath:db.yml", factory = CustomerPropertySourceFactory.class)
public class SpringConfiguration {


}
/*
@Configuration
@Import(JdbcConfig.class)
@PropertySource(value =
        "classpath:jdbc.yml"
        ,factory =
        CustomerPropertySourceFactory.class)
@ComponentScan("com.itheima")
*/
