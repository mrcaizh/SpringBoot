package com.javaczh.imports.config;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AspectJTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @ClassName CustomeImportSelector
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 9:28
 * @Version 1.0
 */
public class CustomeImportSelector implements ImportSelector {
    private String expression;

    /**
     * customeimport.properties配置文件中的内容：
     * custome.importselector.expression= com.javaczh.imports.com.javaczh.servlet.service.impl.*
     */
    public CustomeImportSelector() {
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("customeimport.properties");
            expression = properties.getProperty("custome.importselector.expression");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 生成要导入的bean全限定类名数组
     *
     * @param metadata
     */
    @Override
    public String[] selectImports(AnnotationMetadata metadata) {

        //1.定义扫描包的名称
        String[] basePackages = null;
        //2.判断有@Import注解的类上是否有@ComponentScan注解
        if (metadata.hasAnnotation(ComponentScan.class.getName())) {
            //3.取出@ComponentScan注解的属性
            Map<String, Object> annotationAttributes =
                    metadata.getAnnotationAttributes(ComponentScan.class.getName());
            //4.取出属性名称为basePackages属性的值
            basePackages = (String[]) annotationAttributes.get("basePackages");
        }
        //5.判断是否有此属性（如果没有ComponentScan注解则属性值为null，如果有ComponentScan注解，则basePackages默认为空数组）
        if (null == basePackages || 0 == basePackages.length) {
            String basePackage = null;
            try {
                //6.取出包含@Import注解类的包名
                basePackage = Class.forName(metadata.getClassName()).getPackage().getName();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            //7.存入数组中
            basePackages = new String[]{basePackage};
        }
        //8.创建类路径扫描器
        ClassPathScanningCandidateComponentProvider scanner = new
                ClassPathScanningCandidateComponentProvider(false);
        //9.创建类型过滤器(此处使用切入点表达式类型过滤器)
        TypeFilter typeFilter = new
                AspectJTypeFilter(expression, this.getClass().getClassLoader());
        //10.给扫描器加入类型过滤器
        scanner.addIncludeFilter(typeFilter);
        //11.创建存放全限定类名的集合
        Set<String> classes = new HashSet<>();
        //12.填充集合数据
        for (String basePackage : basePackages) {
            for (BeanDefinition beanDefinition : scanner.findCandidateComponents(basePackage)) {
                System.out.println(beanDefinition.getBeanClassName());
                classes.add(beanDefinition.getBeanClassName());
            }
           /* scanner.findCandidateComponents(basePackage).forEach(
                    beanDefinition ->
                            classes.add(beanDefinition.getBeanClassName()));*/
        }
        //13.按照规则返回
        return classes.toArray(new String[classes.size()]);
    }
}
