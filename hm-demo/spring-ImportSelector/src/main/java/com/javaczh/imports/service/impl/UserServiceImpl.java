package com.javaczh.imports.service.impl;

import com.javaczh.imports.service.UserService;

/**
 * @ClassName UserServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 9:22
 * @Version 1.0
 */
public class UserServiceImpl implements UserService {
    @Override
    public void save() {
        System.out.println("保存");
    }
}
