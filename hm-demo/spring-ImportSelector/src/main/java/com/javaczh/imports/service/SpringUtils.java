package com.javaczh.imports.service;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringValueResolver;

/**
 * @ClassName SpringUtils
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 12:18
 * @Version 1.0
 */
@Component
public class SpringUtils implements
        ApplicationContextAware, BeanNameAware, EmbeddedValueResolverAware, EnvironmentAware {
    private static ApplicationContext applicationContext;
    private static StringValueResolver stringValueResolver;
    private static Environment environment;

    /**
     * @param resolver
     */
    @Override
    public void setEmbeddedValueResolver(StringValueResolver resolver) {
        if (SpringUtils.stringValueResolver == null) {
            SpringUtils.stringValueResolver = resolver;
            System.out.println("StringValueResolver:  "+resolver.resolveStringValue("${jdbc.username}"));
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        if (SpringUtils.applicationContext == null) {
            SpringUtils.applicationContext = applicationContext;
        }
    }


    @Override
    public void setBeanName(String name) {
        System.out.println(name);
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static <T> T getBean(String name) {
        return (T) applicationContext.getBean(name);
    }

    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        return applicationContext.getBean(name, clazz);
    }



    @Override
    public void setEnvironment(Environment environment) {
        if (SpringUtils.environment == null) {
            SpringUtils.environment = environment;
        }
        System.out.println(environment.getDefaultProfiles());
    }


}
