package com.javaczh.imports.service;
 /**
 * @ClassName UserService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 9:22
 * @Version 1.0
 */
public interface UserService {
    void save();
}
