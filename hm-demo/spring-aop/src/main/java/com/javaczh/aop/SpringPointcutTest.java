package com.javaczh.aop;

import com.javaczh.aop.config.SpringConfiguration;
import com.javaczh.aop.domain.User;
import com.javaczh.aop.service.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @ClassName SpringPointcutTest
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 14:29
 * @Version 1.0
 *//**
 * 有两种触发方式：
 * 第一种触发方式：在使用时自行强转新引入接口类型，然后调用方法。例如：测试
 类中的代码
 * 第二种触发方式：在通知类中，使用this关键字，引入新目标类对象，调用方法
 触发。例如：切面类
 * @author 黑马程序员
 * @Company http://www.itheima.com
 */

public class SpringPointcutTest {
    public static void main(String[] args) {
//1.创建容器
        AnnotationConfigApplicationContext ac = new
                AnnotationConfigApplicationContext(SpringConfiguration.class);
//2.获取对象
        UserService userService =
                ac.getBean("userService"
                        , UserService.class);
        //3.执行方法
        User user = new User();
        user.setId("1");
        user.setUsername("test");
        user.setNickname("孙子1");
        //第一种触发方式

        // ValidateService validateService = (ValidateService)userService;
        // boolean check = validateService.checkUser(user);
        // if(check) {
                userService.saveUser(user);
        // }
    }
}
