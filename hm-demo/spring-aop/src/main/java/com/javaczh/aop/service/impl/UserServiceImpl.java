package com.javaczh.aop.service.impl;


import com.javaczh.aop.domain.User;
import com.javaczh.aop.service.UserService;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Override
    public void saveUser(User user) {
        System.out.println("执行了保存用户" + user);
    }
}
