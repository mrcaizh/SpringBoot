package com.javaczh.aop.service;

import com.javaczh.aop.domain.User;

/**
 * @ClassName ValidateService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 14:23
 * @Version 1.0
 */
public interface ValidateService {
    //需要加入的新方法
    boolean checkUser(User user);
}
