package com.javaczh.aop.service;


import com.javaczh.aop.domain.User;


public interface UserService {

    /**
     * 模拟保存用户
     * @param user
     */
    void saveUser(User user);
}
