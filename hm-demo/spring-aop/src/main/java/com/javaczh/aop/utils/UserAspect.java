package com.javaczh.aop.utils;

import com.javaczh.aop.domain.User;
import com.javaczh.aop.service.ValidateService;
import com.javaczh.aop.service.impl.ValidateServiceImpl;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.DeclareParents;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @ClassName UserAspect
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 14:24
 * @Version 1.0
 */
@Component
@Aspect
public class UserAspect {
    @DeclareParents(value =
            "com.javaczh.aop.service.UserService+", defaultImpl = ValidateServiceImpl.class)
    private ValidateService validateService;

    /**
     * 用于定义通用的切入点表达式
     */
    @Pointcut(value = "execution(* com.javaczh.aop.service.impl.*.*(..))")
    public void pointcut1() {
    }

    /**
     * 用于配置当前方法是一个前置通知
     */
    @Before(value = "pointcut1() && args(user) && this(validateService)")
    public void printLog(User user, ValidateService validateService) {
        //第二种触发方式
        boolean check = validateService.checkUser(user);
        if (check) {
            System.out.println("执行打印日志的功能");
        } else {
            throw new IllegalStateException("名称非法");
        }
    }


/*    @Before(value =
            "com.itheima.pointcuts.MyPointcut.pointcut1() &&
            args(user) && this(validateService)")
            public void printLog(User user, ValidateService validateService) {
//第二种触发方式
        boolean check = validateService.checkUser(user);
        if (check) {
            System.out.println("执行打印日志的功能");
        } else {
            throw new IllegalStateException("名称非法");
        }
    }
}*/

}
