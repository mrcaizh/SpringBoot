package com.javaczh.aop.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


@Configuration
@ComponentScan("com.javaczh.aop")
@EnableAspectJAutoProxy//开启spring注解aop配置的支持
public class SpringConfiguration {
}
