package com.javaczh.aop.service.impl;

import com.javaczh.aop.domain.User;
import com.javaczh.aop.service.ValidateService;

/**
 * @ClassName ValidateServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/29 14:23
 * @Version 1.0
 */
public class ValidateServiceImpl implements ValidateService {
    @Override
    public boolean checkUser(User user) {
        if(user.getNickname().contains("孙子")){
            return false;
        }
        return true;
    }
}
