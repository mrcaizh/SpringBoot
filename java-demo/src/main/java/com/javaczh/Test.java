package com.javaczh;

import java.util.Arrays;

/**
 * @ClassName Test
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/24 17:12
 * @Version 1.0
 */
public class Test {
    public static void main(String[] args) {
        User user=new User();
        user.setName("java序列化测试");
        user.setAge("10");
        JavaSerializable serializable=new JavaSerializable();
        byte[] bytes = serializable.serializable(user);
        System.out.println(Arrays.toString(bytes));
        User derializable = serializable.derializable(bytes, User.class);
        System.out.println(derializable);
    }
}
