package com.javaczh;

import java.io.Serializable;
import java.util.StringJoiner;

/**
 * @ClassName demo
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/24 17:04
 * @Version 1.0
 */
public class User implements Serializable {


    private String name;
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("age='" + age + "'")
                .toString();
    }
}
