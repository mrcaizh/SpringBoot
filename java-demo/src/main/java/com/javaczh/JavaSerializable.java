package com.javaczh;

import java.io.*;

/**
 * @ClassName JavaSerializable
 * @Description JDK 提 供 了 Java 对 象 的 序 列 化 方 式 ， 主 要 通 过 输 出 流
 * java.io.ObjectOutputStream 和对象输入流 java.io.ObjectInputStream
 * 来实现。 ，被序列化的对象需要实现 java.io.Serializable 接口
 * @Author CaiZiHao
 * @Date 2020/6/24 17:06
 * @Version 1.0
 */
public class JavaSerializable implements ISerializable {
    /**
     * 序列化
     *
     * @param obj
     * @return
     */
    @Override
    public <T> byte[] serializable(T obj) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream out = new ObjectOutputStream(byteArrayOutputStream);
            out.writeObject(obj);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * 反序列化
     *
     * @param data
     * @param clazz
     * @return
     */
    @Override
    public <T> T derializable(byte[] data, Class<T> clazz) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
        try {
            ObjectInputStream inputStream = new ObjectInputStream(byteArrayInputStream);
            return (T) inputStream.readObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
