package com.javaczh;

/**
 * @ClassName ISerializable
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/24 17:04
 * @Version 1.0
 */
public interface ISerializable {
    /**
     * 序列化
     *
     * @param obj
     * @param <T>
     * @return
     */
    <T> byte[] serializable(T obj);

    /**
     * 反序列化
     *
     * @param data
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T derializable(byte[] data, Class<T> clazz);
}
