package com.javaczh.report;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName ReportApp
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/2 17:45
 * @Version 1.0
 */
@SpringBootApplication
public class ReportApp {
    public static void main(String[] args) {
        SpringApplication.run(ReportApp.class, args);
    }
}
