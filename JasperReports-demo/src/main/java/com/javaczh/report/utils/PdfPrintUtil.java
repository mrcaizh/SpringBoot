package com.javaczh.report.utils;

import javax.print.DocFlavor;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.Sides;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;

/**
 * @ClassName PdfPrintUtil
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/2 20:00
 * @Version 1.0
 */
public class PdfPrintUtil {
   /* public void printPdf(byte[] pdfByte, String printerName) {
        PDDocument document = null;
        try {
            document = PDDocument.load(pdfByte);
            PrinterJob job = setPrinterJonParas(document, printerName);
            job.print();
        } catch (IOException | PrinterException e) {
            System.out.println("打印pdf文件出错");
            e.printStackTrace();
        } finally {
            if (document != null) {
                try {
                    document.close();
                } catch (IOException e) {
                    System.out.println("关闭pdf-document出错");
                    e.printStackTrace();
                }
            }
        }
    }

    *//***
     *
     * @param document
     * @param printerName
     *            打印机名称
     * @return 没有指定则找默认的打印机
     * @throws PrinterException
     *//*
    private PrinterJob setPrinterJonParas(PDDocument document, String printerName) throws PrinterException {
        // 设置纸张及缩放
        PDFPrintable pdfPrintable = new PDFPrintable(document, Scaling.ACTUAL_SIZE);
        PageFormat pageFormat = new PageFormat();
        // 设置打印方向(横向)
        pageFormat.setOrientation(PageFormat.LANDSCAPE);
        // 设置纸张属性
        pageFormat.setPaper(this.getPaper());
        Book book = new Book();
        book.append(pdfPrintable, pageFormat, document.getNumberOfPages());
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPrintService(this.getPrinter(printerName));
        job.setPageable(book);
        // 设置打印份数
        job.setCopies(1);
        return job;
    }

    private Paper getPaper() {
        Paper paper = new Paper();
        // 默认为A4纸张，对应像素宽和高分别为595,842
        int width = 595;
        int height = 842;
        // 设置边距，单位是像素
        int marginLeft = 0, marginRight = 0, marginTop = 0, marginBottom = 0;
        paper.setSize(width, height);
        paper.setImageableArea(marginLeft, marginTop, width - (marginLeft + marginRight),
                height - (marginTop + marginBottom));
        return paper;
    }

    private PrintService getPrinter(String printerName) {
        DocFlavor psInFormat = DocFlavor.INPUT_STREAM.PDF;
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        // A4纸
        pras.add(MediaSizeName.ISO_A4);
        // 单页
        pras.add(Sides.ONE_SIDED);
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(psInFormat, pras);
        PrintService myPrinter = null;
        for (PrintService printService : printServices) {
            System.out.println("打印机名字" + printService.getName());
            if (printService.getName().contains(printerName)) {
                myPrinter = printService;
                break;
            }
        }
        if (myPrinter == null) {
            myPrinter = PrintServiceLookup.lookupDefaultPrintService();
        }
        return myPrinter;
    }*/

}
