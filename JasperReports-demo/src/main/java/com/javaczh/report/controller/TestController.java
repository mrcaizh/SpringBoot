package com.javaczh.report.controller;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.printing.PDFPrintable;
import org.apache.pdfbox.printing.Scaling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * @ClassName TestController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/7/2 17:59
 * @Version 1.0
 */
@Controller
public class TestController {
     

    @GetMapping("/testJasper")
    public void createPDF(HttpServletResponse response) throws Exception {
        //1. 引入jasper文件。由JRXML模板编译生成的二进制文件，用于代码填充数据
        Resource resource = new ClassPathResource("templates/test2.jasper");
        //2.加载jasper文件创建inputStream
        FileInputStream inputStream = new FileInputStream(resource.getFile());

        InputStream in = resource.getInputStream();
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(in);


        /**
         * fis: jasper 文件输入流
         * new HashMap ：向模板中输入的参数
         * JasperDataSource：数据源（和数据库数据源不同）
         *              填充模板的数据来源（connection，javaBean，Map）
         *               填充空数据来源：JREmptyDataSource
         */
        ServletOutputStream outputStream = response.getOutputStream();
        try {
            JasperPrint print = JasperFillManager.fillReport(inputStream, new HashMap<>(), new JREmptyDataSource());
            int pageWidth = print.getPageWidth();
            int pageHeight = print.getPageHeight();
            JasperExportManager.exportReportToPdfStream(print, outputStream);

            byte[] bytes = JasperExportManager.exportReportToPdf(print);


            //3.将JasperPrint已PDF的形式输出
            //  JasperExportManager.exportReportToPdfStream(print, outputStream);
            JasperPrintManager.printReport(print, false);

            // 设置指定打印机
            PrintService[] pss = PrinterJob.lookupPrintServices();
            PrintService ps = null;
            for (int i = 0; i < pss.length; i++) {
                String sps = pss[i].toString();
                //如果打印机名称相同
                if (sps.equalsIgnoreCase("OneNote (Desktop)")) {
                    ps = pss[i];
                }
            }

            JRAbstractExporter je = new JRPrintServiceExporter();
            je.setParameter(JRExporterParameter.JASPER_PRINT, print);
            //设置指定打印机
            je.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, ps);
            je.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, false);
            je.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, false);
            //打印
            je.exportReport();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            outputStream.flush();
        }
    }
    @GetMapping("/request")
    public void request(HttpServletResponse response, HttpServletRequest request) throws Exception {
        System.out.println(request.getRemoteAddr());
        System.out.println(request.getRemoteHost());
        System.out.println(request.getRemotePort());

    }

    @GetMapping("/testJasper2")
    public void testJasper(HttpServletResponse response, HttpServletRequest request) throws Exception {
        System.out.println(request.getRemoteAddr());
        //1. 引入jasper文件。由JRXML模板编译生成的二进制文件，用于代码填充数据
        Resource resource = new ClassPathResource("templates/test2.jasper");
        //2.加载jasper文件创建inputStream
        FileInputStream inputStream = new FileInputStream(resource.getFile());

        InputStream in = resource.getInputStream();
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(in);
        JasperPrint print = JasperFillManager.fillReport(inputStream, new HashMap<>(), new JREmptyDataSource());
        /*        JasperExportManager.exportReportToPdfStream(print, outputStream);*/
        byte[] bytes = JasperExportManager.exportReportToPdf(print);

        // 读取pdf文件
        PDDocument document = PDDocument.load(bytes);

        // 创建打印任务
        PrinterJob job = PrinterJob.getPrinterJob();
        // 遍历所有打印机的名称
        for (PrintService ps : PrinterJob.lookupPrintServices()) {
            String psName = ps.toString();
            System.out.println(psName);
            // 选用指定打印机
            if (psName.equals("Win32 Printer : OneNote (Desktop)")) {
                job.setPrintService(ps);
                break;
            }
        }

        job.setPageable(new PDFPageable(document));
        Paper paper = new Paper();
        // 设置打印纸张大小
        paper.setSize(598, 842); // 1/72 inch
        // 设置打印位置 坐标
        paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
        // no margins
        // custom page format
        PageFormat pageFormat = new PageFormat();
        pageFormat.setPaper(paper);
        // override the page format
        Book book = new Book();
        // append all pages 设置一些属性 是否缩放 打印张数等
        book.append(new PDFPrintable(document, Scaling.ACTUAL_SIZE), pageFormat, 1);
        job.setPageable(book);
        for (int i = 0; i < 10; i++) {
            // 开始打印
            job.print();
        }

    }


    public static void main(String[] args) throws Exception {
        String urlStr = "http://localhost:8090/jpreport/reports/CurrencyStandard.jasper?format=pdf&SEG_NO=00202&PRINT_BATCH_ID=KC2020070310428&USER_ID=dev";
        HttpURLConnection conn=null;
        URL url = new URL(urlStr);
        conn = (HttpURLConnection) new URL(urlStr).openConnection();
        conn.setDoInput(true);
        conn.setRequestMethod("GET");
        conn.setConnectTimeout(6000);
        if (conn.getResponseCode() == 200) {
        // 读取pdf文件
        PDDocument document = PDDocument.load(conn.getInputStream());

        // 创建打印任务
        PrinterJob job = PrinterJob.getPrinterJob();
        // 遍历所有打印机的名称
        for (PrintService ps : PrinterJob.lookupPrintServices()) {
            String psName = ps.toString();
            System.out.println(psName);
            // 选用指定打印机
            if (psName.equals("Win32 Printer : OneNote (Desktop)")) {
                job.setPrintService(ps);
                break;
            }
        }

        job.setPageable(new PDFPageable(document));
        Paper paper = new Paper();
        // 设置打印纸张大小
        paper.setSize(598, 842); // 1/72 inch
        // 设置打印位置 坐标
        paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
        // no margins
        // custom page format
        PageFormat pageFormat = new PageFormat();
        pageFormat.setPaper(paper);
        // override the page format
        Book book = new Book();
        // append all pages 设置一些属性 是否缩放 打印张数等
        book.append(new PDFPrintable(document, Scaling.ACTUAL_SIZE), pageFormat, 1);
        job.setPageable(book);
        // 开始打印
        job.print();

        }
    }






 /*   public static void print() {
        JasperPrint jasperPrint = getJasperPrint();
        String selectedPrinter = AllPrinter.getDepartmentPrinter("Admin");

        PrinterJob printerJob = PrinterJob.getPrinterJob();
        PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
        PrintService selectedService = null;

        if(services.length != 0 || services != null)
        {
            for(PrintService com.javaczh.servlet.service : services){
                String existingPrinter = com.javaczh.servlet.service.getName().toLowerCase();
                if(existingPrinter.equals(selectedPrinter))
                {
                    selectedService = com.javaczh.servlet.service;
                    break;
                }
            }

            if(selectedService != null)
            {
                printerJob.setPrintService(selectedService);
                boolean printSucceed = JasperPrintManager.printReport(mainPrint, false);
            }
        }

        private static JasperPrint getJasperPrint(){
            return JasperPrinterCreator.getJasperprint();
        }*/


    /**
     * 1. 加载模板
     *
     * JasperPrint jasperPrint = JasperFillManager.fillReport("WebRoot/report/test.jasper", new HashMap(),new JREmptyDataSource());
     * //false/true 表示在打印的时候是否显示打印机设置
     * JasperPrintManager.printReport(jasperPrint, false);
     *
     *
     *
     * 2. 设置指定打印机
     *
     * PrintService[] pss = PrinterJob.lookupPrintServices();
     * PrintService ps = null;
     * for (int i = 0; i < PSs.length; i++) {
     * String sps = PSs[i].toString();
     * //如果打印机名称相同
     * if(sps.equalsIgnoreCase("Win32 Printer : pdfFactory Pro")){
     * ps = PSs[i];
     * }
     * }
     * JRAbstractExporter je = new JRPrintServiceExporter();
     * je.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
     * //设置指定打印机
     * je.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, ps);
     * je.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, false);
     * je.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, false);
     * //打印
     * je.exportReport();
     */
}
