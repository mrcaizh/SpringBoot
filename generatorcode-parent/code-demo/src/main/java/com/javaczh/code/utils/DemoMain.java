package com.javaczh.code.utils;

import com.javaczh.code.CodeUtils;
import com.javaczh.code.entity.Table;

import java.sql.*;
import java.util.List;

/**
 * @ClassName DemoMain
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/19 13:31
 * @Version 1.0
 */
public class DemoMain {
    private final static String URL = "jdbc:oracle:thin:@10.70.76.13:1521:dbtestim";
    //oracle.jdbc.driver.OracleDriver
    private final static String DRIVER_NAME = "oracle.jdbc.OracleDriver";
    private final static String USERNAME = "steeltrade";
    private final static String PASSWORD = "zaq1xsw2cde3";


    public static void main(String[] args) throws Exception {
        //getMetaDdata(metaData);
     //   getTable();

        List<Table> dbInfo = CodeUtils.getDbInfo();
        System.out.println(dbInfo);
    }

    private static void getTable() throws Exception {
        DatabaseMetaData metaData = getConnection().getMetaData();
        ResultSet schemas = metaData.getSchemas();
      /*  while(schemas.next()){
            System.out.println(schemas.getString(1));
        }*/
        /**
                  * String catalog, String schemaPattern,String tableNamePattern, String types[]
                  *
                  *  catalog:当前操作的数据库
                  *      mysql:
                  *          :ihrm
                  *          catalog
                  *      oralce:
                  *          xxx:1521:orcl
                  *          catalog
                  *   schema：
                  *      mysql：
                  *          ：null
                  *      oracle：
                  *          ：用户名（大写）
                  *
                  *    tableNamePattern：
                  *      null：查询所有表
                  *      为空：查询目标表
                  *
                  *    types：类型
                  *      TABLE：表
                  *      VIEW：视图
                  *
                  */
        ResultSet tablers = metaData.getTables("dbtestim", "JK",
                "T_MESSAGE_TEMPLATE_D", new String[]{"TABLE"});
        System.out.println("所属数据库\t\t所属schema\t\t表名\t\t\t\t数据库表类型 \t\t\t数据库表备注");
        while (tablers.next()) {
            System.out.println(tablers.getString(1) + "\t\t\t\t" + tablers.getString(2) +
                    "\t\t" + tablers.getString(3) + "\t\t\t\t" + tablers.getString(4) + "\t\t\t\t" + tablers.getString(5)
            );
        }
    }

    private static void getMetaDdata() throws Exception {
        DatabaseMetaData metaData = getConnection().getMetaData();
        System.out.println("元数据metaData:" + metaData.toString());
        //获取数据库产品名称
        String databaseProductName = metaData.getDatabaseProductName();
        System.out.println("数据库产品名称:" + databaseProductName);
        //获取数据库版本号
        String databaseMajorVersion = metaData.getDatabaseProductVersion();
        System.out.println("数据库版本号:" + databaseMajorVersion);
        //获取数据库用户名
        String userName = metaData.getUserName();
        System.out.println("数据库用户名:" + userName);
        //获取数据库连接URL
        String url = metaData.getURL();
        System.out.println("数据库连接URL:" + url);
        //获取数据库驱动
        String driverName = metaData.getDriverName();
        System.out.println("数据库驱动:" + driverName);
        //获取数据库驱动版本号
        String driverVersion = metaData.getDriverVersion();
        System.out.println("数据库驱动版本号:" + driverVersion);
        //查看数据库是否允许读操作
        boolean readOnly = metaData.isReadOnly();
        System.out.println("数据库是否允许读操作:" + readOnly);
        //查看数据库是否支持事务操作
        boolean transactions = metaData.supportsTransactions();
        System.out.println("数据库是否支持事务操作:" + transactions);
    }

    public static Connection getConnection() throws Exception {
        //1.加载驱动
        Class.forName(DRIVER_NAME);
        //2.获取连接对象
        return DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }
}
