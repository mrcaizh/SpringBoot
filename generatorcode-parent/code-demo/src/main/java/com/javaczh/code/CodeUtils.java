package com.javaczh.code;

import com.javaczh.code.entity.Column;
import com.javaczh.code.entity.Table;
import com.javaczh.code.utils.StringUtils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.*;

/**
 * @ClassName CodeUtils
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/19 14:13
 * @Version 1.0
 */
public class CodeUtils {
    private final static String URL = "jdbc:oracle:thin:@10.70.76.13:1521:dbtestim";
    //oracle.jdbc.driver.OracleDriver
    private final static String DRIVER_NAME = "oracle.jdbc.OracleDriver";
    private final static String USERNAME = "steeltrade";
    private final static String PASSWORD = "zaq1xsw2cde3";

    public static Connection getConnection() throws Exception {
        //1.加载驱动
        Class.forName(DRIVER_NAME);
        Properties props = new Properties();

        props.setProperty("user", USERNAME);
        props.setProperty("password", PASSWORD);
        props.setProperty("remarks", "true");
        return DriverManager.getConnection(URL, props);

        //2.获取连接对象
        // return DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }

    public static List<Table> getDbInfo() throws Exception {
        //1.获取连接
        Connection connection = getConnection();
        //2.获取元数据
        DatabaseMetaData metaData = connection.getMetaData();
        //3.获取当前数据库中的所有表
        // ResultSet tables = metaData.getTables(null, null, "pe_permission", new String[]{"TABLE"});
        // XS.T_S_CONTRACT_D
        ResultSet tables = metaData.getTables("dbtestim", "XS",
                "T_S_CONTRACT_D", new String[]{"TABLE"});
        List<Table> list = new ArrayList<>();
        while (tables.next()) {
            Table tab = new Table();
            //i.表名
            String tableName = tables.getString("TABLE_NAME"); //bs_user  User
            //ii.类名
            String className = tableName;// removePrefix(tableName);
            //iii.描述
            String remarks = tables.getString("REMARKS");
            //iiii.主键
            ResultSet primaryKeys = metaData.getPrimaryKeys(null, null, tableName);
            String keys = "";
            while (primaryKeys.next()) {
                String keyname = primaryKeys.getString("COLUMN_NAME");
                keys += keyname + ",";
            }
            tab.setName(tableName);
            tab.setName2(className);
            tab.setComment(remarks);
            tab.setKey(keys);
            //处理表中的所有字段

            ResultSet columns = metaData.getColumns(null, null, tableName, null);
            List<Column> columnList = new ArrayList<>();
            while (columns.next()) {
                Column cn = new Column();
                //构造Column对象
                //列名称
                String columnName = columns.getString("COLUMN_NAME"); //user_id  userId , create_time createTime
                cn.setColumnName(columnName);
                //属性名
                String attName = StringUtils.toJavaVariableName(columnName);
                cn.setColumnName2(attName);
                //java类型和数据库类型
                String dbType = columns.getString("TYPE_NAME");//VARCHAR,DATETIME
                cn.setColumnDbType(dbType);
                String javaType = dbTypeToJavaType(dbType);
                cn.setColumnType(javaType);
                //备注
                String columnRemark = columns.getString("REMARKS");//VARCHAR,DATETIME

                cn.setColumnComment(columnRemark);
                //是否主键
                String pri = null;
                if (StringUtils.contains(columnName, keys.split(","))) {
                    pri = "PRI";
                }
                cn.setColumnKey(pri);
                columnList.add(cn);
            }
            columns.close();
            tab.setColumns(columnList);
            list.add(tab);
        }
        tables.close();
        connection.close();
        return list;
    }

    /**
     * SQL类型和java类型替换规则
     *
     * @return
     */
    private static String dbTypeToJavaType(String dbType) {
        Map<String, String> data = new HashMap<>(10);
        data.put("VARCHAR", "String");
        data.put("BIGINT", "Long");
        data.put("INT", "Integer");
        data.put("DATE", "java.util.Date");
        data.put("DATETIME", "java.util.Date");
        data.put("DOUBLE", "Double");
        data.put("TEXT", "String");
        data.put("VARCHAR2", "String");
        data.put("NVARCHAR2", "String");
        //data.put("NUMBER", "Long");
        data.put("MEDIUMTEXT", "String");
        data.put("TINYINT", "Integer");
        data.put("LONGTEXT", "String");
        // NUMBER(18,6)   NUMBER
        data.put("NUMBER", "java.math.BigDecimal");
        String lowerCase = dbType.toUpperCase();
        if (data.containsKey(lowerCase)){
            return data.get(lowerCase);
        }
        return "String";
    }


}
