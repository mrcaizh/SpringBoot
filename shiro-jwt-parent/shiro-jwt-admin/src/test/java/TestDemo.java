import com.javaczh.system.AdminApp;
import com.javaczh.system.common.utils.PathMatcherUtils;
import com.javaczh.system.common.utils.redis.RedisService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.AntPathMatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Test
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 16:03
 * @Version 1.0
 */
@SpringBootTest(classes = AdminApp.class)
public class TestDemo {
    public static void main(String[] args) {
//        List<String> name=new ArrayList<>(2);
//        name.add("12345");
//        name.add("54321");
//        name.add("123");
//        System.out.println(name.contains("12345"));
//        System.out.println(name.contains("123425"));

     /*   SysUser user = new SysUser();
        user.setUsername("zs");
        user.setId("12345");
        user.setPhone("123456789");
        UserVO vo = new UserVO();
        UserVO vo1 = UserMapStruct.INSTANCE.voToModel(user);
        System.out.println(vo1);
*/
        //System.out.println(PathMatcherUtils.match("/api/add", "/api/add/"));


        AntPathMatcher antPathMatcher = new AntPathMatcher();
// 可根据需求做动态匹配
        String pattern = "/api/**/anon/**";
//        if (antPathMatcher.match(pattern, url)) {
//            // 根据入参url和pattern匹配上返回ture,否则false.
//            return true;
//        }
//        return false;
        //System.out.println(antPathMatcher.match(pattern, "/app2/demo/ll"));
        System.out.println(PathMatcherUtils.match(pattern, "/api/demo/55/anon/ll"));

    }

    @Autowired
    private RedisService redisService;

    @Test
    public void test1() {
        redisService.set("de","sd");
        System.out.println(redisService.hasKey("de"));
      /*  SysUser user = new SysUser();
        user.setUsername("zs");
        user.setId("12345");
        user.setPhone("123456789");
        UserVO vo = new UserVO();
        UserVO vo1 = UserMapStruct.INSTANCE.voToModel(user);
        System.out.println(vo1);*/
    }

}
