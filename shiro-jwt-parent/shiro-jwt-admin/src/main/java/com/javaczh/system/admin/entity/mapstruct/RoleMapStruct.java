package com.javaczh.system.admin.entity.mapstruct;

import com.javaczh.system.admin.entity.dto.RoleDto;
import com.javaczh.system.admin.entity.dto.RoleQueryDto;
import com.javaczh.system.admin.entity.dto.UserDto;
import com.javaczh.system.admin.entity.model.SysRole;
import com.javaczh.system.admin.entity.model.SysUser;
import com.javaczh.system.admin.entity.vo.RoleVo;
import com.javaczh.system.admin.entity.vo.UserVO;
import com.javaczh.system.common.entity.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @ClassName UserMapStruct
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 15:54
 * @Version 1.0
 */
@Mapper
public interface RoleMapStruct {

    RoleMapStruct INSTANCE = Mappers.getMapper(RoleMapStruct.class);

    /**
     * vo转实体
     *
     * @param roleVo
     * @return
     */

    SysRole voToModel(RoleVo roleVo);

    /**
     * 实体转vo
     *
     * @param sysRole
     * @return
     */
    RoleVo modelToVo(SysRole sysRole);
    /**
     * dto转实体
     *
     * @param roleDto
     * @return
     */
    SysRole dtoToModel(RoleDto roleDto);

    /**
     * dto转实体
     *
     * @param roleQueryDto
     * @return
     */
    RoleVo queryDtoToModel(RoleQueryDto roleQueryDto);

    PageResult<RoleVo> pageResultToVo(PageResult<SysRole> pageResult);

    List<RoleVo> modelListToVoList(List<SysRole> roleList);

}
