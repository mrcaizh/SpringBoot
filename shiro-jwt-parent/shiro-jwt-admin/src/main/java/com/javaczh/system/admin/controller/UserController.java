package com.javaczh.system.admin.controller;

import com.javaczh.system.admin.contants.Constant;
import com.javaczh.system.admin.entity.dto.*;
import com.javaczh.system.admin.entity.vo.RoleVo;
import com.javaczh.system.admin.entity.vo.UserRoleDetailsVo;
import com.javaczh.system.admin.entity.vo.UserVO;
import com.javaczh.system.admin.service.UserService;
import com.javaczh.system.admin.utils.CreateImageCode;
import com.javaczh.system.common.entity.PageResult;
import com.javaczh.system.common.entity.Result;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * @ClassName UserController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/25 10:53
 * @Version 1.0
 */
@RestController
@Api(tags = "组织模块-用户管理")
@RequestMapping("/api")
@Slf4j
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/user/login")
    @ApiOperation(value = "用户登录接口")
    public Result<UserVO> login(@RequestBody UserLoginDto userLoginDto) {
        return Result.success(userService.login(userLoginDto));
    }

    @GetMapping("/user/logout")
    @ApiOperation(value = "用户登出接口")
    public Result logout(HttpServletRequest request) {
        try {
            userService.logout(request.getHeader(Constant.ACCESS_TOKEN), request.getHeader(Constant.REFRESH_TOKEN));
        } catch (Exception e) {
            log.error("logout error{}", e);
            throw e;
        }
        return Result.success();
    }

    @GetMapping("/user/unLogin")
    @ApiOperation(value = "引导客户端去登录")
    public Result unLogin() {
        return Result.getResult(BaseResponseCode.TOKEN_ERROR);
    }


    @GetMapping("/user/getImageCode")
    @ApiOperation(value = "验证码")
    public void getImageCode(HttpServletResponse response) throws IOException {
        // 设置响应的类型格式为图片格式
        response.setContentType("image/jpeg");
        //禁止图像缓存。
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        CreateImageCode vCode = new CreateImageCode(100, 30, 5, 10);
        //  session.setAttribute("code", vCode.getCode());
        userService.saveImageCodeValue(vCode.getCode());
        vCode.write(response.getOutputStream());
    }

    @PostMapping("/user/selectUserByParam")
    @ApiOperation(value = "根据参数分页查询用户")
    public Result<PageResult<UserVO>> selectUserByParam(UserQueryDto userQueryDto) {
        return Result.success(userService.selectUserByParam(userQueryDto));
    }

    @PostMapping("/user/addOrUpdateUser")
    @ApiOperation(value = "新增或修改用户")
    public Result<Boolean> addOrUpdateUser(@RequestBody @Valid UserDto userDto) {
        return new Result<>(userService.addOrUpdateUser(userDto));
    }

    @PostMapping("/user/batchDeleteUser")
    @ApiOperation(value = "新增或修改用户")
    public Result<Boolean> batchDeleteUser(@RequestBody List<String> ids) {
        return new Result<>(userService.batchDeleteUser(ids));
    }

    @GetMapping("/user/getUserRoleDetails")
    @ApiOperation(value = "赋予角色---查询已拥有和未拥有的角色信息")
    public Result<UserRoleDetailsVo> getUserRoleDetails(@RequestParam("userId") String userId) {
        return new Result<>(userService.getUserRoleDetails(userId));
    }

    @PutMapping("/user/endowRoles")
    @ApiOperation(value = "赋予角色---批量新增中间表数据")
    public Result<Boolean> endowRoles(@RequestBody UserEndowRoleDto userEndowRoleDto) {
        return new Result<>(userService.endowRoles(userEndowRoleDto));
    }

    @GetMapping("/user/token")
    @ApiOperation(value = "用户刷新token接口")
    public Result<String> refreshToken(HttpServletRequest request){
        String refreshToken=request.getHeader(Constant.REFRESH_TOKEN);
        return new Result<>(userService.refreshToken(refreshToken));
   }







}
