package com.javaczh.system.admin.controller;

import com.javaczh.system.admin.entity.dto.RoleDto;
import com.javaczh.system.admin.entity.dto.RoleQueryDto;
import com.javaczh.system.admin.entity.vo.RoleVo;
import com.javaczh.system.admin.service.RoleService;
import com.javaczh.system.common.entity.PageResult;
import com.javaczh.system.common.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName RoleController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/2 14:36
 * @Version 1.0
 */

@RestController
@RequestMapping("/api")
@Api(tags = "组织模块-角色")
@CrossOrigin
public class RoleController {
    @Autowired
    private RoleService roleService;

    @PostMapping("/role/selectRolesByParam")
    @ApiOperation(value = "根据参数分页查询角色")
    public Result<PageResult<RoleVo>> selectRolesByParam(@RequestBody RoleQueryDto roleQueryDto) {
        return new Result<>(roleService.selectRolesByParam(roleQueryDto));
    }

    @PostMapping("/role/addOrUpdateRole")
    @ApiOperation(value = "新增或修改角色")
    public Result<Boolean> addOrUpdateRole(@RequestBody RoleDto roleDto) {
        return new Result<>(roleService.addOrUpdateRole(roleDto));
    }


    @PostMapping("/role/deleteRole")
    @ApiOperation(value = "删除角色")
    public Result<Boolean> deleteRole(@RequestBody List<String> ids) {
        return new Result<>(roleService.deleteRole(ids));
    }

}