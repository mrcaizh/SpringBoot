package com.javaczh.system.admin.utils;

import com.alibaba.fastjson.JSON;
import com.javaczh.system.common.entity.Result;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.*;

/**
 * @ClassName ResponseUtils
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/25 12:50
 * @Version 1.0
 */
@Slf4j
public class ResponseUtils {

    private ResponseUtils() {

    }

    /**
     * 自定义异常的类，用户返回给客户端相应的JSON格式的信息
     *
     * @param code
     * @param mes
     */
    public static void simpleResponseMessage(ServletResponse response, int code, String mes) {
        try {
            Result result = Result.getResult(code, mes);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding("UTF-8");
            String jsonMessage = JSON.toJSONString(result);
            OutputStream out = response.getOutputStream();
            out.write(jsonMessage.getBytes("UTF-8"));
            out.flush();
        } catch (IOException e) {
            log.error("eror={}", e);
        }
    }

    public static void simpleResponseMessage(ServletResponse response, BaseResponseCode baseResponseCode) {
        try {
            Result result = Result.getResult(baseResponseCode.getCode(), baseResponseCode.getMsg());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding("UTF-8");
            String jsonMessage = JSON.toJSONString(result);
            OutputStream out = response.getOutputStream();
            out.write(jsonMessage.getBytes("UTF-8"));
            out.flush();
        } catch (IOException e) {
            log.error("eror={}", e);
        }
    }
}
