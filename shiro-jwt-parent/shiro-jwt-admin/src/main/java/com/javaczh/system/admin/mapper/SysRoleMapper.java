package com.javaczh.system.admin.mapper;

import com.javaczh.system.admin.entity.dto.RoleQueryDto;
import com.javaczh.system.admin.entity.model.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleMapper {
    int insert(SysRole record);

    int insertSelective(SysRole record);

    List<SysRole> selectRolesByParam(RoleQueryDto roleQueryDto);

    int updateByPrimaryKeySelective(SysRole sysRole);

    int deleteRoleByIds(List<String> ids);

    List<SysRole> selectAllRoles();

    List<String> selectRoleByUserId(String userId);
}