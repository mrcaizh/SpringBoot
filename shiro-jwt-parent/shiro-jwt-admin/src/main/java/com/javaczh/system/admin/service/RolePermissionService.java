package com.javaczh.system.admin.service;

import com.javaczh.system.admin.entity.dto.RolePermissionDto;

/**
 * @ClassName RolePermissionService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/2 17:33
 * @Version 1.0
 */
public interface RolePermissionService {

    int addRolePermission(RolePermissionDto dto);

    int updateRolePermission(RolePermissionDto dto);
}
