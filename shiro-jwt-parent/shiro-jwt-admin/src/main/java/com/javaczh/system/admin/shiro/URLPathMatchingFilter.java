package com.javaczh.system.admin.shiro;

import com.javaczh.system.admin.contants.Constant;
import com.javaczh.system.admin.utils.JwtTokenUtils;
import com.javaczh.system.admin.utils.ResponseUtils;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import com.javaczh.system.common.utils.PathMatcherUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.springframework.util.StringUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName URLPathMatchingFilter
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/5 17:38
 * @Version 1.0
 */
@Slf4j
public class URLPathMatchingFilter extends PathMatchingFilter {


    @Override
    protected boolean onPreHandle(ServletRequest servletRequest, ServletResponse response, Object mappedValue) throws Exception {

     /*   //请求的url
        String requestURL = getPathWithinApplication(servletRequest);
        System.out.println("请求的url :" + requestURL);
        Subject subject = SecurityUtils.getSubject();
        if (!subject.isAuthenticated()) {
            // 如果没有登录, 直接返回true 进入登录流程
            return true;
        }
        String token = (String) SecurityUtils.getSubject().getPrincipal();
        System.out.println(token);
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        if (StringUtils.isEmpty(accessToken)) {
            ResponseUtils.simpleResponseMessage(response, BaseResponseCode.TOKEN_NOT_NULL.getCode(), BaseResponseCode.TOKEN_NOT_NULL.getMsg());
            return false;
        }

        Claims claims = JwtTokenUtils.getClaimsFromToken(accessToken);
        //  /anon/**只需要select角色即可访问,其他路径都需要进行其他校验    Constant.ROLES_INFOS_KEY
        if (PathMatcherUtils.match(Constant.ANON_REQUEST_URL_PATTERN, requestURL)) {
            List<String> roleIds = (List<String>) claims.get(Constant.ROLES_INFOS_KEY);
            if (!roleIds.contains(Constant.SELECT_ROLE)) {
                ResponseUtils.simpleResponseMessage(response, BaseResponseCode.NOT_PERMISSION);
                return false;
            }
        }else {
            //根据用户token获取用户访问资源的权限
            List<String> permission = (List<String>) claims.get(Constant.PERMISSIONS_INFOS_KEY);
            if (PathMatcherUtils.matches(permission, requestURL)) {
                return true;
            } else {
                *//**
                 *没有权限访问可以跳转到为授权页面,要在shrio配置 shiroFilterFactoryBean.setUnauthorizedUrl("/unauthorized");未授权界面
                 * UnauthorizedException ex = new UnauthorizedException("当前用户没有访问路径" + requestURL + "的权限");
                 *  WebUtils.issueRedirect(request, response, "/unauthorized");
                 *  也可以抛出异常,前台处理
                 *//*
                ResponseUtils.simpleResponseMessage(response, BaseResponseCode.NOT_PERMISSION);
                return false;
            }

        }
//        boolean hasPermission = false;
//        for (String s : permission) {
//            if (s.equals(requestURL)) {
//                hasPermission = true;
//                break;
//            }
//        }*/
       return true;
    }
}