package com.javaczh.system.admin.shiro;

import com.google.code.kaptcha.Constants;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @ClassName ImageCodeValidateFilter
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/26 16:36
 * @Version 1.0
 */
public class ImageCodeValidateFilter extends FormAuthenticationFilter {
    //原FormAuthenticationF ilter的认证方法
    @Override
    protected boolean onAccessDenied(ServletRequest request,
                                     ServletResponse response) throws Exception {
//在这里进行验证码的校验
        //从session获取正确验证码
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();
//取出session的验证码(正确的验证码)
        String validateCode = (String) session.getAttribute("validateCode");
//取出页面的验证码
        //输入的验证和sess ion中的验证进行对比
        String randomcode = httpServletRequest.getParameter("imageCode");
      /*  if (randomcode != null && validateCode != null && !randomcode.equals(validateCode)) {
//如果校验失败，将验证码错误失败信息，通过shiroLoginFailure设置到request中
            httpServletRequest.setAttribute(" shiroLoginFailure", "randomCodeError");
//拒绝访问，不再校验账号和密码
            return true;
        }*/
        return super.onAccessDenied(request, response);


  /*  private String captchaParam = "imageCode"; //前台提交的验证码参数名

    private String failureKeyAttribute = "shiroLoginFailure";  //验证失败后存储到的属性名

    public String getCaptchaCode(ServletRequest request) {
        return WebUtils.getCleanParam(request, getCaptchaParam());
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
            throws Exception {


        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        // 从session获取正确的验证码
        Session session = SecurityUtils.getSubject().getSession();
        //页面输入的验证码
        String captchaCode = getCaptchaCode(request);
        String validateCode = (String)session.getAttribute(Constants.KAPTCHA_SESSION_KEY);

        HttpServletRequest httpServletRequest = WebUtils.toHttp(request);
        //判断验证码是否表单提交（允许访问）
        if ( !"post".equalsIgnoreCase(httpServletRequest.getMethod())) {
            return true;
        }

        request.setCharacterEncoding("UTF-8");
        // 通过字节流方法获取表单数据
        ServletInputStream sis = request.getInputStream();
        int len = 0;
        byte[] b = new byte[1024];
        while( (len= sis.read(b)) != -1) {
            System.out.println(new String(b, "UTF-8"));
        }
        return true;
    }

    public String getCaptchaParam() {
        return captchaParam;
    }

    public void setCaptchaParam(String captchaParam) {
        this.captchaParam = captchaParam;
    }*/
    }
}
