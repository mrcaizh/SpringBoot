package com.javaczh.system.admin.service.impl;

import com.alibaba.fastjson.JSON;
import com.javaczh.system.admin.entity.mapstruct.PermissionMapStruct;
import com.javaczh.system.admin.entity.vo.IndexDataVo;
import com.javaczh.system.admin.entity.vo.PermissionNodeVo;
import com.javaczh.system.admin.service.IndexService;
import com.javaczh.system.admin.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ClassName IndexServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/27 13:44
 * @Version 1.0
 */
@Service
@Transactional
public class IndexServiceImpl implements IndexService {
    @Autowired
    private PermissionService permissionService;

    @Override
    public IndexDataVo getIndexDataInfo(String userId) {
        IndexDataVo indexDataVo = new IndexDataVo();
        //菜单数据
        //  List<PermissionNodeVo> menus = JSON.parseArray(getMenus(), PermissionNodeVo.class);
        List<PermissionNodeVo> menus = permissionService.permissionTreeList(userId);
        indexDataVo.setMenus(menus);
        return indexDataVo;
    }


    /**
     * 模拟数据
     *
     * @return
     */
    private String getMenus() {
        String home = "[\n" +
                "    {\n" +
                "        \"children\": [\n" +
                "            {\n" +
                "                \"children\": [\n" +
                "                    {\n" +
                "                        \"children\": [\n" +
                "                            {\n" +
                "                                \"children\": [\n" +
                "                                    {\n" +
                "                                        \"children\": [],\n" +
                "                                        \"id\": \"6\",\n" +
                "                                        \"title\": \"五级类目5-6\",\n" +
                "                                        \"url\": \"http://www.layui.com\"\n" +
                "                                    }\n" +
                "                                    {\n" +
                "                                        \"children\": [],\n" +
                "                                        \"id\": \"6\",\n" +
                "                                        \"title\": \"菜单\",\n" +
                "                                        \"url\": \"component/authority/menus.html\"\n" +
                "                                    }\n" +


                "                                ],\n" +
                "                                \"id\": \"5\",\n" +
                "                                \"title\": \"四级类目4- 5\",\n" +
                "                                \"url\": \"string\"\n" +
                "                            }\n" +
                "                        ],\n" +
                "                        \"id\": \"4\",\n" +
                "                        \"title\": \"三级类目3- 4\",\n" +
                "                        \"url\": \"string\"\n" +
                "                    }\n" +
                "                ],\n" +
                "                \"id\": \"3\",\n" +
                "                \"title\": \"二级类目2- 3\",\n" +
                "                \"url\": \"string\"\n" +
                "            }\n" +
                "        ],\n" +
                "        \"id\": \"1\",\n" +
                "        \"icon\": \"layui-icon-user\",\n" +
                "        \"title\": \"类目1\",\n" +
                "        \"url\": \"string\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"children\": [],\n" +
                "        \"id\": \"2\",\n" +
                "        \"icon\": \"layui-icon-app\",\n" +
                "        \"title\": \"类目2\",\n" +
                "        \"url\": \"string\"\n" +
                "    }\n" +
                "]";
        return home;

    }


}
