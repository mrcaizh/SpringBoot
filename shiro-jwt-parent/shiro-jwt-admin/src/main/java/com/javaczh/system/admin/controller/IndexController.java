package com.javaczh.system.admin.controller;

import com.javaczh.system.admin.contants.Constant;
import com.javaczh.system.admin.entity.vo.IndexDataVo;
import com.javaczh.system.admin.service.IndexService;
import com.javaczh.system.admin.utils.JwtTokenUtils;
import com.javaczh.system.common.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName IndexController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/27 13:42
 * @Version 1.0
 */
@RestController
@Api(tags = "首页数据")
@RequestMapping("/api")
@Slf4j
@CrossOrigin
public class IndexController {
    @Autowired
    private IndexService indexService;

    @GetMapping("/index")
    @ApiOperation(value = "获取首页数据接口")
    public Result<IndexDataVo> getHomeInfo(HttpServletRequest request) {
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        /**
         * 通过access_token拿userId
         */
        String userId = JwtTokenUtils.getUserId(accessToken);
        return new Result<>(indexService.getIndexDataInfo(userId));
    }
}
