package com.javaczh.system.admin.service;

import com.javaczh.system.admin.entity.dto.RoleDto;
import com.javaczh.system.admin.entity.dto.RoleQueryDto;
import com.javaczh.system.admin.entity.vo.RoleVo;
import com.javaczh.system.common.entity.PageResult;

import java.util.List;

/**
 * @ClassName RoleService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/2 14:41
 * @Version 1.0
 */
public interface RoleService {
    PageResult<RoleVo> selectRolesByParam(RoleQueryDto roleQueryDto);

    Boolean addOrUpdateRole(RoleDto roleDto);

    Boolean deleteRole(List<String> ids);
}
