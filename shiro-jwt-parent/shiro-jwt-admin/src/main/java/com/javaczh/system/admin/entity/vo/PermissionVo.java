package com.javaczh.system.admin.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @ClassName PermissionVo
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/27 18:08
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class PermissionVo {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "菜单权限编码")
    private String code;
    @ApiModelProperty(value = "菜单权限名称")
    private String name;
    @ApiModelProperty(value = "授权(如：sys:user:add)")
    private String perms;
    @ApiModelProperty(value = "请求URL")
    private String url;
    @ApiModelProperty(value = "请求方式")
    private String method;
    @ApiModelProperty(value = "父目录Id")
    private String pid;
    @ApiModelProperty(value = "排序",example = "1")
    private Integer orderNum;
    @ApiModelProperty(value = "类型(1:目录;2:菜单;3:按钮)",example = "1")
    private Integer type;
    @ApiModelProperty(value = "状态",example = "1")
    private Integer status;
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;
    @ApiModelProperty(value = "删除标记",example = "1")
    private Integer deleted;

    @ApiModelProperty(value = "图标样式")
    private String icon;
    /**
     * 父目录名称
     */
    @ApiModelProperty(value = "父目录名称")
    private String pidName;
    /**
     * 默认展开; 后期加入数据库
     */
    @ApiModelProperty(value = "Tree树展开标记")
    private boolean   open;

}
