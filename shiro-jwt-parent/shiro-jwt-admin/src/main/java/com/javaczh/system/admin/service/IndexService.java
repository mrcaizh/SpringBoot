package com.javaczh.system.admin.service;

import com.javaczh.system.admin.entity.vo.IndexDataVo;

/**
 * @ClassName IndexService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/27 13:44
 * @Version 1.0
 */
public interface IndexService {


    public IndexDataVo getIndexDataInfo(String userId);

}
