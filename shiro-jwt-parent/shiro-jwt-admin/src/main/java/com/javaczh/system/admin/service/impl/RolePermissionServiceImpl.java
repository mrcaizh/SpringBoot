package com.javaczh.system.admin.service.impl;

import com.javaczh.system.admin.entity.dto.RolePermissionDto;
import com.javaczh.system.admin.entity.model.SysRolePermission;
import com.javaczh.system.admin.mapper.SysRolePermissionMapper;
import com.javaczh.system.admin.service.RolePermissionService;
import com.javaczh.system.common.exception.BusinessException;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import com.javaczh.system.common.utils.id.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @ClassName RolePermissionServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/2 17:33
 * @Version 1.0
 */
@Service
@Transactional
public class RolePermissionServiceImpl implements RolePermissionService {
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

    /**
     * 新增角色权限中间表数据
     *
     * @param dto
     * @return
     */
    @Override
    public int addRolePermission(RolePermissionDto dto) {
        List<SysRolePermission> list = new ArrayList<>(5);
        Set<String> permissionIds = dto.getPermissionIds();
        Iterator<String> iterator = permissionIds.iterator();
        Date createDate = new Date();
        while (iterator.hasNext()) {
            SysRolePermission model = new SysRolePermission();
            model.setId(idWorker.nextIdToString());
            model.setRoleId(dto.getRoleId());
            model.setCreateTime(createDate);
            model.setPermissionId(iterator.next());
            list.add(model);
        }
        int count = sysRolePermissionMapper.batchAddRolePermission(list);
        if (count == 0) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return count;
    }

    @Override
    public int updateRolePermission(RolePermissionDto dto) {
        sysRolePermissionMapper.deleteByRoleId(dto.getRoleId());
        return addRolePermission(dto);
    }
}
