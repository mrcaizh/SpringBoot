package com.javaczh.system.admin.entity.vo;

import lombok.Data;
import org.aspectj.weaver.ast.Not;

import java.util.List;

/**
 * @ClassName UserRoleDetils
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/4 16:11
 * @Version 1.0
 */
@Data
public class UserRoleDetailsVo {
    /**
     * 全部角色信息
     */
    private List<RoleVo> allRoles;
    /**
     * 已拥有的角色信息
     */
    private List<RoleVo> alreadyOwnedRoles;

    /**
     * 已拥有的角色的Id信息
     */
    private List<String> alreadyOwnedRoleIds;

}
