package com.javaczh.system.admin.shiro;

import com.javaczh.system.admin.contants.Constant;
import com.javaczh.system.admin.utils.JwtTokenUtils;
import io.jsonwebtoken.Claims;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.List;

/**
 * @ClassName CustomRealm
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/25 15:46
 * @Version 1.0
 */
public class CustomRealm extends AuthorizingRealm {
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof CustomUsernamePasswordToken;
    }

    /**
     * doGetAuthenticationInfo(主要用户用户的认证，以前是验证用户名密码这里我们会改造成验证 token 一般来说客户端只需登录
     * 一次后续的访问用 token来维护登录的状态，所以我们这里改造成验证 token
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        CustomUsernamePasswordToken token = (CustomUsernamePasswordToken) authenticationToken;
        return new SimpleAuthenticationInfo(
                token.getPrincipal(),
                token.getCredentials(),
                CustomRealm.class.getName());
    }

    /**
     * 授权
     * doGetAuthorizationInfo(主要用于用户授权，就是设置用户所拥有的角色/权限)
     *
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        //获取token
        String token = (String) principalCollection.getPrimaryPrincipal();
        Claims claims = JwtTokenUtils.getClaimsFromToken(token);
        authorizationInfo.addRoles((List<String>) claims.get(Constant.ROLES_INFOS_KEY));
        authorizationInfo.addStringPermissions((List<String>) claims.get(Constant.PERMISSIONS_INFOS_KEY));
        return authorizationInfo;
    }
}
