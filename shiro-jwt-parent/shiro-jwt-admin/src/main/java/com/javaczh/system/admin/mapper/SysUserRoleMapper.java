package com.javaczh.system.admin.mapper;

import com.javaczh.system.admin.entity.model.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Mapper
public interface SysUserRoleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);

    int bacthAddUserRole(List<SysUserRole> list);

    int deleteByUserId( String userId);

    Set<String> selectRoleIdsByUserId(String userId);

    List<String> selectRoleNamesByUserId(String userId);
}