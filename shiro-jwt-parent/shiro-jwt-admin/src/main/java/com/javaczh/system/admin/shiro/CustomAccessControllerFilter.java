package com.javaczh.system.admin.shiro;

import com.javaczh.system.admin.contants.Constant;
import com.javaczh.system.admin.utils.ResponseUtils;
import com.javaczh.system.common.exception.BusinessException;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.springframework.util.StringUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName CustomAccessControllerFilter
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 15:17
 * @Version 1.0
 */
@Slf4j
public class CustomAccessControllerFilter extends AccessControlFilter {
    /**
     * 是否允许访问
     * true 允许,交给下一个Filter处理
     * false: 则往下执行onAccessDenied 方法
     *
     * @param servletRequest
     * @param servletResponse
     * @param o
     * @return
     * @throws Exception
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        return false;
    }

    /**
     * 表示访问拒绝是否自己处理
     * 如果返回true表示自己不处理且继续拦截器链执行， 
     * 返回false表示自己已经处理了（比如重定向到另一个页面）。
     *
     * @param servletRequest
     * @param servletResponse
     * @return
     * @throws Exception
     */
    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        log.info("========================  请求方法: {}=================================", request.getMethod());
        log.info("========================  请求路径: {}=================================", request.getRequestURI());
        //获取认证token
        String access_token = request.getHeader(Constant.ACCESS_TOKEN);

        if (StringUtils.isEmpty(access_token)) {
            ResponseUtils.simpleResponseMessage(servletResponse, BaseResponseCode.TOKEN_NOT_NULL);
            return false;
        }
        try {
            CustomUsernamePasswordToken customUsernamePasswordToken = new CustomUsernamePasswordToken(access_token);
            getSubject(servletRequest, servletResponse).login(customUsernamePasswordToken);
        } catch (BusinessException e) {
            ResponseUtils.simpleResponseMessage(servletResponse, e.getMessageCode(), e.getDetailMessage());
            return false;
        } catch (AuthenticationException e) {
            if (e.getCause() instanceof BusinessException) {
                BusinessException exception = (BusinessException) e.getCause();
                ResponseUtils.simpleResponseMessage(servletResponse, exception.getMessageCode(), exception.getDetailMessage());
            } else {
                ResponseUtils.simpleResponseMessage(servletResponse, BaseResponseCode.SHIRO_AUTHENTICATION_ERROR);
            }
            return false;
        } catch (Exception e) {
            ResponseUtils.simpleResponseMessage(servletResponse, BaseResponseCode.SYSTEM_ERROR);
            return false;
        }
        return true;
    }
}
