package com.javaczh.system.admin.entity.dto;

import com.javaczh.system.common.entity.SimplePage;
import lombok.Data;

/**
 * @ClassName UserQueryDto
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/25 11:07
 * @Version 1.0
 */
@Data
public class UserQueryDto {
    private SimplePage simplePage = new SimplePage();
}
