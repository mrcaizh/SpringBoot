package com.javaczh.system.admin.shiro.user;

import com.javaczh.system.admin.entity.model.UserInfo;

/**
 * @ClassName UserContext
 * @Description 用户对象保存
 * @Author CaiZiHao
 * @Date 2020/6/4 11:42
 * @Version 1.0
 */
public class UserContext {
    private static ThreadLocal<UserInfo> userHolder = new ThreadLocal<>();

    public static void setUser(UserInfo loginUser) {
        userHolder.set(loginUser);
    }

    public static UserInfo getLoginUser() {
        return userHolder.get();
    }
}
