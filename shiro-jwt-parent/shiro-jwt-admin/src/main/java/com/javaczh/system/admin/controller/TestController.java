package com.javaczh.system.admin.controller;


import com.javaczh.system.admin.mapper.SysUserMapper;
import com.javaczh.system.admin.service.UserService;
import com.javaczh.system.common.entity.Result;
import com.javaczh.system.common.exception.BusinessException;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@RestController
@RequestMapping("/test")
@Api(tags = "测试相关模块")
public class TestController {


    @GetMapping("/index")
    @ApiOperation(value = "Swagger 测试 index 接口")
    public String testResult() throws IOException {

        return "Hello World";
    }

    @GetMapping("/home")
    @ApiOperation(value = "测试统一返回格式接口")
    public Result getHome() {
//        Result result=Result.success();
        Result result = Result.getResult(BaseResponseCode.SUCCESS);

        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        result.setData(list);
        int i = 1 / 0;

        return result;
    }

    @GetMapping("/business/error")
    @ApiOperation(value = "测试主动抛出业务异常接口")
    public Result<String> testBusinessError(@RequestParam String type) {
        if (!type.equals("1") || type.equals("2") || type.equals("3")) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        Result<String> result = new Result(0, type);
        return result;
    }




}
