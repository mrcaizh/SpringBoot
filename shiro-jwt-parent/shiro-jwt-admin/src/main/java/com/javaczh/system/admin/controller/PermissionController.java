package com.javaczh.system.admin.controller;

/**
 * @ClassName PermissionController
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/28 13:27
 * @Version 1.0
 */

import com.javaczh.system.admin.entity.dto.PermissionDto;
import com.javaczh.system.admin.entity.model.SysPermission;
import com.javaczh.system.admin.entity.vo.PermissionNodeVo;
import com.javaczh.system.admin.entity.vo.PermissionVo;
import com.javaczh.system.admin.entity.vo.RolePermissionVo;
import com.javaczh.system.admin.service.PermissionService;
import com.javaczh.system.common.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api")
@Api(tags = "组织模块-菜单权限管理")
@CrossOrigin
public class PermissionController {
    @Autowired
    private PermissionService permissionService;

    @GetMapping("/permission/permissions")
    @ApiOperation(value = "获取所有菜单权限接口")
    public Result<List<PermissionVo>> getAllMenusPermission() {
        return new Result<>(permissionService.selectAllPermission());
    }

    //directory
    @GetMapping("/permission/getMenusTreeToDirectory")
    @ApiOperation(value = "获取所有目录菜单树接口-查询到目录")
    public Result<List<PermissionNodeVo>> getMenusTreeToDirectory() {
        return new Result<>(permissionService.getMenusTreeToDirectory());
    }

    @GetMapping("/permission/getMenusTreeToBtn")
    @ApiOperation(value = "获取所有目录菜单树接口-查询到按钮")
    public Result<List<PermissionNodeVo>> getMenusTreeToBtn() {
        return new Result<>(permissionService.getMenusTreeToBtn());
    }

    @GetMapping("/permission/getMenusTreeToBtnByRoleId/{roleId}")
    @ApiOperation(value = "根据角色ID获取所有目录菜单树接口-查询到按钮, 主要是修改数据回显复选框打勾")
    public Result<List<PermissionNodeVo>> getMenusTreeToBtnByRoleId(@PathVariable("roleId") String roleId) {
        return new Result<>(permissionService.getMenusTreeToBtnByRoleId(roleId));
    }

    @GetMapping("/permission/getMenusTreeToBtnByRoleId2/{roleId}")
    @ApiOperation(value = "根据角色ID获取所有目录菜单树接口-查询到按钮, 主要是修改数据回显复选框打勾")
    public Result<RolePermissionVo> getMenusTreeToBtnByRoleId2(@PathVariable("roleId") String roleId) {
        return new Result<>(permissionService.getMenusTreeToBtnByRoleId2(roleId));
    }


    @PostMapping("/permission/addOrUpdatePermission")
    @ApiOperation(value = "新增或修改菜单权限接口")
    public Result<SysPermission> addOrUpdatePermission(@RequestBody @Valid PermissionDto permissionDto) {
        return new Result<>(permissionService.addOrUpdatePermission(permissionDto));
    }

    @PostMapping("/permission/deletePermission")
    @ApiOperation(value = "删除菜单权限接口")
    public Result<Boolean> deletePermission(@RequestBody List<String> ids) {
        return new Result<>(permissionService.deletePermission(ids));
    }


    @GetMapping("/permission/getBtnPermissionByRole")
    @ApiOperation(value = "根据用户ID 获取角色拥有的按钮权限,主要是控制前端页面的按钮显示和隐藏")
    public Result<List<PermissionVo>> getBtnPermissionByRole() {
        return new Result<>(permissionService.getBtnPermissionByRole());
    }



}
