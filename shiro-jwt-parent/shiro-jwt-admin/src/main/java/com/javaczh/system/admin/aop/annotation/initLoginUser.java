package com.javaczh.system.admin.aop.annotation;

import java.lang.annotation.*;

/**
 * @ClassName UserInfo
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/4 11:47
 * @Version 1.0
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface initLoginUser {
}
