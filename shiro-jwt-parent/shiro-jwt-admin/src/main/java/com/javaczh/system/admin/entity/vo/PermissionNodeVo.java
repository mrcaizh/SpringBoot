package com.javaczh.system.admin.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName PermissionRespNodeVo
 * @Description 首页菜单
 * @Author CaiZiHao
 * @Date 2020/5/27 13:40
 * @Version 1.0
 */
@Data
public class PermissionNodeVo {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "菜单权限名称")
    private String title;
    @ApiModelProperty(value = "接口地址")
    private String url;
    @ApiModelProperty(value = "图标样式")
    private String icon;
    @ApiModelProperty(value = "是否展开默认不展开(false)")
    private boolean spread = true;

    @ApiModelProperty(value = "复选框是否勾选(false)")
    private boolean checked = false;
    @ApiModelProperty(value = "子菜单")
    private List<?> children;
}
