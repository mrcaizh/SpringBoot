package com.javaczh.system.admin.shiro.cache;

import com.javaczh.system.common.utils.redis.RedisService;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName RedisCacheManager
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/26 11:11
 * @Version 1.0
 */
public class RedisCacheManager implements CacheManager {
    @Autowired
    private RedisService redisService;
    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        return new RedisCache<>(s,redisService);
    }
}
