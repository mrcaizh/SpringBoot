package com.javaczh.system.admin.shiro;

import com.javaczh.system.admin.contants.Constant;

import com.javaczh.system.admin.utils.JwtTokenUtils;
import com.javaczh.system.common.exception.BusinessException;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import com.javaczh.system.common.utils.redis.RedisService;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @Author:CaiZiHao
 * @Date:2020/5/7 10:29
 */

public class CustomHashedCredentialsMatcher extends HashedCredentialsMatcher {
    @Autowired
    private RedisService redisService;

    /**
     * 以前是校验密码是否合法,现在继承HashedCredentialsMatcher 重新doCredentialsMatch校验token的合法性
     *
     * @param token
     * @param info
     * @return
     */
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        CustomUsernamePasswordToken customUsernamePasswordToken = (CustomUsernamePasswordToken) token;
        String accessToken = (String) customUsernamePasswordToken.getPrincipal();
        String userId = JwtTokenUtils.getUserId(accessToken);
        /**
         * 判断用户是否被锁定
         */
        if (redisService.hasKey(Constant.ACCOUNT_LOCK_KEY.concat(userId))) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_LOCK);
        }
        /**
         * 判断用户是否被删除
         */
        if (redisService.hasKey(Constant.DELETED_USER_KEY.concat(userId))) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_HAS_DELETED_ERROR);
        }

        /**
         * 判断token 是否主动登出
         */
        if (redisService.hasKey(Constant.JWT_REFRESH_TOKEN_BLACKLIST.concat(accessToken))) {
            throw new BusinessException(BaseResponseCode.TOKEN_ERROR);
        }
        /**
         * 判断token是否通过校验
         */
        if (!JwtTokenUtils.validateToken(accessToken)) {
            throw new BusinessException(BaseResponseCode.TOKEN_PAST_DUE);
        }
        /**
         * 判断这个登录用户是否要主动去刷新
         *
         * 如果 key=Constant.JWT_REFRESH_KEY+userId大于accessToken说明是在 accessToken不是重新生成的
         * 这样就要判断它是否刷新过了/或者是否是新生成的token
         */
        if (redisService.hasKey(Constant.JWT_REFRESH_KEY.concat(accessToken)) &&
                redisService.getExpire(Constant.JWT_REFRESH_KEY.concat(userId), TimeUnit.MILLISECONDS)
                        > JwtTokenUtils.getRemainingTime(accessToken)) {
            /**
             * 是否存在刷新的标识
             */
            if (!redisService.hasKey(Constant.JWT_REFRESH_IDENTIFICATION.concat(accessToken))) {
                throw new BusinessException(BaseResponseCode.TOKEN_PAST_DUE);
            }
        }

        /**
         * 判断redis是否存在该用户的token,并且是否一致,不一致则提醒异地登录

        if (redisService.hasKey(userId)&&!accessToken.equals(redisService.get(userId))){
            throw new BusinessException(BaseResponseCode.YIDI_LOGIN_ERROR);
        }
         */

        return true;
    }
}

