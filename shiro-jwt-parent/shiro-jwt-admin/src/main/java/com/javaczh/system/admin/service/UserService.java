package com.javaczh.system.admin.service;

import com.javaczh.system.admin.entity.dto.UserDto;
import com.javaczh.system.admin.entity.dto.UserEndowRoleDto;
import com.javaczh.system.admin.entity.dto.UserLoginDto;
import com.javaczh.system.admin.entity.dto.UserQueryDto;
import com.javaczh.system.admin.entity.vo.UserRoleDetailsVo;
import com.javaczh.system.admin.entity.vo.UserVO;
import com.javaczh.system.common.entity.PageResult;

import java.util.List;

/**
 * @ClassName UserService
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 23:12
 * @Version 1.0
 */
public interface UserService {

    public UserVO login(UserLoginDto userLoginDto);

    void logout(String accessToken, String refreshToken);

    PageResult<UserVO> selectUserByParam(UserQueryDto userQueryDto);

    void saveImageCodeValue(String code);

    Boolean addOrUpdateUser(UserDto userDto);

    Boolean batchDeleteUser(List<String> ids);

    /**
     * 赋予角色查询
     *
     * @return
     */
    UserRoleDetailsVo getUserRoleDetails(String userId);

    /**
     * 保存角色信息
     *
     * @param userEndowRoleDto
     * @return
     */
    Boolean endowRoles(UserEndowRoleDto userEndowRoleDto);

    /**
     * jwt 自动刷新
     * jwt 刷新有两种情况要考虑？
     * 1.一种是管理员修改了该用户的角色/权限(需要主动去刷新)。
     * 2.另一种是 jwt 过期要刷新。
     * 3.刷新成功后生成新的token自动再刷新当前请求接口
     *
     * @param refreshToken
     * @return
     */
    String refreshToken(String refreshToken);

    void update();


    void select();

}
