package com.javaczh.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName AdminApp
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/20 15:21
 * @Version 1.0
 */
@SpringBootApplication

public class AdminApp {
    public static void main(String[] args) {
        SpringApplication.run(AdminApp.class, args);
    }
}
