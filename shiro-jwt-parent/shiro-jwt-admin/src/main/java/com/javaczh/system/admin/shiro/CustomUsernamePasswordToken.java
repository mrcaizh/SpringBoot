package com.javaczh.system.admin.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @ClassName CustomUsernamePasswordToken
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 15:16
 * @Version 1.0
 */
public class CustomUsernamePasswordToken extends UsernamePasswordToken {
    private String token;

    public CustomUsernamePasswordToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

}
