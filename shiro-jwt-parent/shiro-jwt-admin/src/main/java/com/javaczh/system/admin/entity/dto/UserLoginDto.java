package com.javaczh.system.admin.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @ClassName UserLoginDto
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/4 10:36
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class UserLoginDto {
    @NotNull(message = "账号不能为空")
    @ApiModelProperty(value = "账号")
    private String username;
    @NotNull(message = "用户密码不能为空")
    @ApiModelProperty(value = "用户密码")
    private String password;
    @ApiModelProperty(value = "登录类型(1:pc;2:App)")
    @NotBlank(message = "登录类型不能为空")
    private String type;
    @ApiModelProperty(value = "图片验证码")
    @NotBlank(message = "验证码不能为空")
    private String imageCode;

}
