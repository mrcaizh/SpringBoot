package com.javaczh.system.admin.mapper;

import com.javaczh.system.admin.entity.dto.UserQueryDto;
import com.javaczh.system.admin.entity.model.SysRole;
import com.javaczh.system.admin.entity.model.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface SysUserMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    SysUser selectUserByUserName(@Param("username") String username);

    List<SysUser> selectUserByParam(UserQueryDto userQueryDto);

    int batchDeleteUser(List<String> ids);


    void update();

}