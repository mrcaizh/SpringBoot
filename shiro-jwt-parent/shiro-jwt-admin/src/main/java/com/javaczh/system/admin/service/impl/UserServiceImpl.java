package com.javaczh.system.admin.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.javaczh.system.admin.aop.annotation.initLoginUser;
import com.javaczh.system.admin.contants.Constant;
import com.javaczh.system.admin.entity.dto.UserDto;
import com.javaczh.system.admin.entity.dto.UserEndowRoleDto;
import com.javaczh.system.admin.entity.dto.UserLoginDto;
import com.javaczh.system.admin.entity.dto.UserQueryDto;
import com.javaczh.system.admin.entity.mapstruct.RoleMapStruct;
import com.javaczh.system.admin.entity.mapstruct.UserMapStruct;
import com.javaczh.system.admin.entity.model.*;
import com.javaczh.system.admin.entity.vo.RoleVo;
import com.javaczh.system.admin.entity.vo.UserRoleDetailsVo;
import com.javaczh.system.admin.mapper.SysRoleMapper;
import com.javaczh.system.admin.mapper.SysRolePermissionMapper;
import com.javaczh.system.admin.mapper.SysUserRoleMapper;
import com.javaczh.system.admin.shiro.user.UserContext;
import com.javaczh.system.admin.entity.vo.UserVO;
import com.javaczh.system.admin.mapper.SysUserMapper;
import com.javaczh.system.admin.service.UserService;
import com.javaczh.system.admin.utils.JwtTokenUtils;
import com.javaczh.system.admin.utils.PageUtils;
import com.javaczh.system.common.entity.PageResult;
import com.javaczh.system.common.exception.BusinessException;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import com.javaczh.system.common.utils.id.IdWorker;
import com.javaczh.system.common.utils.password.PasswordUtils;
import com.javaczh.system.common.utils.redis.RedisService;
import io.jsonwebtoken.Claims;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName UserServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/5/22 23:12
 * @Version 1.0
 */
@Service
@Slf4j
//@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisService redisService;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysRolePermissionMapper sysRolePermissionMapper;

    @Override
    public UserVO login(UserLoginDto userLoginDto) {
        validateImageCode(userLoginDto.getImageCode());
        SysUser sysUser = sysUserMapper.selectUserByUserName(userLoginDto.getUsername());
        //该账号不存在
        if (sysUser == null) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_ERROR);
        }
        //账户状态(1.正常 2.锁定)
        if (2 == sysUser.getStatus()) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_LOCK);
        }     //验证密码,通过明文加密和数据库加密的密码进行对比
        if (!PasswordUtils.matches(sysUser.getSalt(), userLoginDto.getPassword(), sysUser.getPassword())) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_PASSWORD_ERROR);
        }
        UserVO vo = UserMapStruct.INSTANCE.modelToVo(sysUser);
        //进行token签发
        Map<String, Object> claims = new HashMap<>(3);
        claims.put(Constant.ROLES_INFOS_KEY, getRolesByUserId(sysUser.getId()));
        claims.put(Constant.PERMISSIONS_INFOS_KEY, getPermissionsByUserId(sysUser.getId()));
        claims.put(Constant.JWT_USER_NAME, sysUser.getUsername());
        claims.put(Constant.JWT_USER_INFO_KEY, JSONObject.toJSONString(vo));
        //根据用户id生成认证token
        String accessToken = JwtTokenUtils.getAccessToken(sysUser.getId(), claims);
        //根据用户id生成刷新token,app端生成的token的失效长
        String refreshToken;
        if ("1".equals(userLoginDto.getType())) {
            refreshToken = JwtTokenUtils.getRefreshToken(sysUser.getId(), claims);
        } else {
            refreshToken = JwtTokenUtils.getRefreshAppToken(sysUser.getId(), claims);
        }
        /**   
         * 异地登录失效逻辑     
         */
        redisService.set(sysUser.getId(), accessToken, 60, TimeUnit.MINUTES);
        vo.setAccessToken(accessToken);
        vo.setRefreshToken(refreshToken);
        return vo;
    }

    /**
     * 图片验证码校验
     *
     * @param imageCode
     */
    private void validateImageCode(String imageCode) {
        /*boolean flag = true;
        List<Object> imageCodeList = redisService.lrange(Constant.IMAGE_CODE_VALUE, 0, -1);
        Iterator<Object> iterator = imageCodeList.iterator();
        while (iterator.hasNext()) {
            String imageCodeValue = (String) iterator.next();
            if (imageCode.equalsIgnoreCase(imageCodeValue)) {
                flag = false;
                iterator.remove();
            }
        }
        if (flag) {
            throw new BusinessException(BaseResponseCode.IMAGE_CODE_NOT_MATCH);
        } else {
            redisService.ltrim(Constant.IMAGE_CODE_VALUE, 0, -1);
            redisService.lpush(Constant.IMAGE_CODE_VALUE, imageCodeList);
        }*/
    }


    /**
     * 主要是清空 shiro 的一些缓存信息，然后就是我们系统具体业务了 把 access_token 加入黑名单、refresh_token 加入黑名单。
     *
     * @param accessToken
     * @param refreshToken
     */
    @Override
    public void logout(String accessToken, String refreshToken) {
        if (StringUtils.isEmpty(accessToken) || StringUtils.isEmpty(refreshToken)) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        //获取主体
        Subject subject = SecurityUtils.getSubject();
        log.info("subject.getPrincipals()={}", subject.getPrincipals());
        if (subject.isAuthenticated()) {
            subject.logout();
        }
        String userId = JwtTokenUtils.getUserId(accessToken);
        /**
         * 把token 加入黑名单 禁止再登录
         */
        redisService.set(Constant.JWT_ACCESS_TOKEN_BLACKLIST + accessToken, userId, JwtTokenUtils.getRemainingTime(accessToken),
                TimeUnit.MILLISECONDS);
        //把 refreshToken 加入黑名单 禁止再拿来刷新token
        redisService.set(Constant.JWT_REFRESH_TOKEN_BLACKLIST + refreshToken,
                userId, JwtTokenUtils.getRemainingTime(refreshToken), TimeUnit.MILLISECONDS);
    }

    /**
     * 根据参数分页查询用户
     *
     * @param userQueryDto
     * @return
     */
    @Override
    @initLoginUser
    public PageResult<UserVO> selectUserByParam(UserQueryDto userQueryDto) {
        PageHelper.startPage(userQueryDto.getSimplePage().getPageNum(), userQueryDto.getSimplePage().getPageSize());
        PageResult<SysUser> pageResult = PageUtils.getPageResult(sysUserMapper.selectUserByParam(userQueryDto));
        return UserMapStruct.INSTANCE.pageResultToVo(pageResult);
    }

    /**
     * mock 数据
     * 通过用户id获取该用户所拥有的角色
     * 后期修改为通过操作DB获取
     *
     * @return java.util.List<java.lang.String>
     * @throws
     */
    private List<String> getRolesByUserId(String userId) {
//        List<String> roles = new ArrayList<>();
//        if ("9a26f5f1-cbd2-473d-82db-1d6dcf4598f8".equals(userId)) {
//            roles.add("admin");
//        } else {
//            roles.add("test");
//        }
        List<String> roles = sysUserRoleMapper.selectRoleNamesByUserId(userId);
        return roles;
    }

    /**
     * mock 数据
     * 通过用户id获取该用户所拥有的角色
     * 后期通过操作数据获取
     *
     * @param userId
     * @return java.util.List<java.lang.String>
     * @throws
     */
    private List<String> getPermissionsByUserId(String userId) {
        List<SysPermission> list = sysRolePermissionMapper.selectPermissionUrlByUserId(userId);

        List<String> permissions = new ArrayList<>(3);
//        if ("9a26f5f1-cbd2-473d-82db-1d6dcf4598f8".equals(userId)) {
//            permissions.add("sys:user:list");
//            permissions.add("sys:user:add");
//            permissions.add("sys:user:update");
//            permissions.add("sys:user:detail");
//        } else {
//            permissions.add("sys:user:detail");
//        }
        for (SysPermission sysPermission : list) {
            permissions.add(sysPermission.getUrl());
        }
        return permissions;

    }

    /**
     * 保存图片验证码的值, 10分钟内有效
     *
     * @param code
     */
    @Override
    public void saveImageCodeValue(String code) {
        //  redisService.lpush(Constant.IMAGE_CODE_VALUE, code);
        // redisService.set(Constant.IMAGE_CODE_VALUE.concat(code), code, 10, TimeUnit.MINUTES);
    }

    @Override
    public Boolean addOrUpdateUser(UserDto userDto) {
        UserInfo loginUser = UserContext.getLoginUser();
        SysUser sysUser = UserMapStruct.INSTANCE.dtoToModel(userDto);
        int result = 0;
        Date date = new Date();
        if (StringUtils.isEmpty(sysUser.getId())) {
            sysUser.setId(idWorker.nextIdToString());
            //设置盐
            sysUser.setSalt(PasswordUtils.getSalt());
            //对密码进行加密
            sysUser.setPassword(PasswordUtils.encode(sysUser.getPassword(), sysUser.getSalt()));
            sysUser.setCreateId(loginUser.getId());
            sysUser.setUpdateId(loginUser.getId());
            sysUser.setCreateTime(date);
            sysUser.setUpdateTime(date);
            result = sysUserMapper.insertSelective(sysUser);
        } else {
            sysUser.setUpdateId(loginUser.getId());
            sysUser.setUpdateTime(date);
            result = sysUserMapper.updateByPrimaryKeySelective(sysUser);
        }
        return result >= 1;
    }

    @Override
    public Boolean batchDeleteUser(List<String> ids) {
        int i = sysUserMapper.batchDeleteUser(ids);
        if (i <= 0) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return i >= 1;
    }


    /**
     * 赋予角色查询
     *
     * @return
     */
    @Override
    public UserRoleDetailsVo getUserRoleDetails(String userId) {
        UserRoleDetailsVo detailsVo = new UserRoleDetailsVo();
        //查询全部角色信息
        List<SysRole> roles = sysRoleMapper.selectAllRoles();
        detailsVo.setAllRoles(RoleMapStruct.INSTANCE.modelListToVoList(roles));
        //查询已拥有的角色id
        detailsVo.setAlreadyOwnedRoleIds(sysRoleMapper.selectRoleByUserId(userId));
        return detailsVo;
    }

    /**
     * 保存角色信息
     *
     * @param userEndowRoleDto
     * @return
     */
    @Override
    public Boolean endowRoles(UserEndowRoleDto userEndowRoleDto) {
        List<SysUserRole> list = new ArrayList<>();
        sysUserRoleMapper.deleteByUserId(userEndowRoleDto.getUserId());
        Iterator<String> iterator = userEndowRoleDto.getRoleIds().iterator();
        Date date = new Date();
        while (iterator.hasNext()) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setId(idWorker.nextIdToString());
            sysUserRole.setUserId(userEndowRoleDto.getUserId());
            sysUserRole.setRoleId(iterator.next());
            sysUserRole.setCreateTime(date);
            list.add(sysUserRole);
        }
        int result = sysUserRoleMapper.bacthAddUserRole(list);
        if (result <= 0) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return true;
    }

    /**
     * jwt 自动刷新
     * jwt 刷新有两种情况要考虑？
     * 1.一种是管理员修改了该用户的角色/权限(需要主动去刷新)。
     * 2.另一种是 jwt 过期要刷新。
     * 3.刷新成功后生成新的token自动再刷新当前请求接口
     *
     * @param refreshToken
     * @return
     */
    @Override
    public String refreshToken(String refreshToken) {
        //它是否过期
        //它是否被加如了黑名
        if (!JwtTokenUtils.validateToken(refreshToken) || redisService.hasKey(Constant.JWT_REFRESH_TOKEN_BLACKLIST + refreshToken)) {
            throw new BusinessException(BaseResponseCode.TOKEN_ERROR);
        }
        String userId = JwtTokenUtils.getUserId(refreshToken);
        String username = JwtTokenUtils.getUserName(refreshToken);
        Claims claimsFromToken = JwtTokenUtils.getClaimsFromToken(refreshToken);

        log.info("userId={}", userId);
        Map<String, Object> claims = new HashMap<>();
        claims.put(Constant.JWT_USER_NAME, username);
        claims.put(Constant.ROLES_INFOS_KEY, getRolesByUserId(userId));
        claims.put(Constant.PERMISSIONS_INFOS_KEY, getPermissionsByUserId(userId));
        claims.put(Constant.JWT_USER_INFO_KEY, claimsFromToken.get(Constant.JWT_USER_INFO_KEY));
        String newAccessToken = JwtTokenUtils.getAccessToken(userId, claims);
        return newAccessToken;
    }


    @Override
    @Transactional
    public void update() {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey("126844864776333312023");
        System.out.println(sysUser);
        modified(sysUser);
        RestTemplate restTemplate=new RestTemplate();
        String forObject = restTemplate.getForObject("http://localhost:8091/api/user/select", String.class);
        System.out.println();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void modified(SysUser sysUser) {
        sysUser.setRealName("6666666666666");
        sysUserMapper.updateByPrimaryKeySelective(sysUser);
    }

    @Override
    public void select() {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey("126844864776333312023");
        System.out.println("select:  "+sysUser);
    }
}
