package com.javaczh.system.admin.service.impl;

import com.github.pagehelper.PageHelper;
import com.javaczh.system.admin.entity.dto.RoleDto;
import com.javaczh.system.admin.entity.dto.RolePermissionDto;
import com.javaczh.system.admin.entity.dto.RoleQueryDto;
import com.javaczh.system.admin.entity.mapstruct.RoleMapStruct;
import com.javaczh.system.admin.entity.model.SysRole;
import com.javaczh.system.admin.entity.vo.RoleVo;
import com.javaczh.system.admin.mapper.SysRoleMapper;
import com.javaczh.system.admin.service.RolePermissionService;
import com.javaczh.system.admin.service.RoleService;
import com.javaczh.system.admin.utils.PageUtils;
import com.javaczh.system.common.entity.PageResult;
import com.javaczh.system.common.exception.BusinessException;
import com.javaczh.system.common.exception.code.BaseResponseCode;
import com.javaczh.system.common.utils.id.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * @ClassName RoleServiceImpl
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/2 14:41
 * @Version 1.0
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private IdWorker idWorker;
    @Autowired
    private RolePermissionService rolePermissionService;

    /**
     * 根据参数分页查询角色
     *
     * @param roleQueryDto
     * @return
     */
    @Override
    public PageResult<RoleVo> selectRolesByParam(RoleQueryDto roleQueryDto) {
        PageHelper.startPage(roleQueryDto.getSimplePage().getPageNum(), roleQueryDto.getSimplePage().getPageSize());
        List<SysRole> list = sysRoleMapper.selectRolesByParam(roleQueryDto);
        PageResult<SysRole> pageResult = PageUtils.getPageResult(list);
        return RoleMapStruct.INSTANCE.pageResultToVo(pageResult);
    }

    @Override
    public Boolean addOrUpdateRole(RoleDto roleDto) {
        SysRole sysRole = RoleMapStruct.INSTANCE.dtoToModel(roleDto);
        Date date = new Date();
        int result;
        //新增
        if (StringUtils.isEmpty(roleDto.getId())) {
            sysRole.setId(idWorker.nextIdToString());
            sysRole.setUpdateTime(date);
            sysRole.setCreateTime(date);
            result = sysRoleMapper.insertSelective(sysRole);
            if (result == 1) {
                RolePermissionDto dto = new RolePermissionDto();
                dto.setRoleId(sysRole.getId());
                dto.setPermissionIds(roleDto.getPermissionIds());
                //保存中间表数据
                result += rolePermissionService.addRolePermission(dto);
            }
        } else {
            sysRole.setUpdateTime(date);
            result = sysRoleMapper.updateByPrimaryKeySelective(sysRole);
            if (result == 1) {
                RolePermissionDto dto = new RolePermissionDto();
                dto.setRoleId(sysRole.getId());
                dto.setPermissionIds(roleDto.getPermissionIds());
                //保存中间表数据
                result += rolePermissionService.updateRolePermission(dto);
            }
        }
        return result >= 2;
    }


    @Override
    public Boolean deleteRole(List<String> ids) {
        int i = sysRoleMapper.deleteRoleByIds(ids);
        if (i <= 0) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return i >= 1;
    }
}
