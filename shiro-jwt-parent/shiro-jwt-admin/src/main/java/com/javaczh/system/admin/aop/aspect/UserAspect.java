package com.javaczh.system.admin.aop.aspect;

import com.alibaba.fastjson.JSONObject;
import com.javaczh.system.admin.contants.Constant;
import com.javaczh.system.admin.entity.model.UserInfo;
import com.javaczh.system.admin.shiro.user.UserContext;
import com.javaczh.system.admin.utils.JwtTokenUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName UserAspect
 * @Description
 * @Author CaiZiHao
 * @Date 2020/6/4 13:16
 * @Version 1.0
 */
@Component
@Aspect
@Slf4j
public class UserAspect {
    @Autowired
    private HttpServletRequest request;

//    @Autowired
//    private SysUserRoleMapper sysUserRoleMapper;

    /**
     * 配置织入点(以@UserInfo注解为标志)
     * 只要出现 @UserInfo注解都会进入
     */
    @Pointcut("@annotation(com.javaczh.system.admin.aop.annotation.initLoginUser)")
    public void logPointCut() {

    }

    @Pointcut("execution(public * com.javaczh.system.admin.controller.*.*(..))")
    public void beforeController() {

    }


    /**
     * 前置通知,主要是初始化当前登陆对象
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Before("beforeController()")
    //  @Before("logPointCut()")
    public void around(JoinPoint point) {
        log.info("============== 获取当前登陆对象 ===============");
        UserInfo userInfo = new UserInfo();
        //获取认证token
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        if (!StringUtils.isEmpty(accessToken)) {
            if (JwtTokenUtils.validateToken(accessToken)) {
                //构建登陆对象
                Claims claims = JwtTokenUtils.getClaimsFromToken(accessToken);
                String json = claims.get(Constant.JWT_USER_INFO_KEY, String.class);
                userInfo = JSONObject.parseObject(json, UserInfo.class);
                if (!StringUtils.isEmpty(userInfo.getId())) {
//                    Set<String> ids = sysUserRoleMapper.selectRoleIdsByUserId(userInfo.getId());
//                    userInfo.setRoleIds(ids);
                }
                //设置用户
                UserContext.setUser(userInfo);
            }
        }
        log.info("请求URL   ==> : {}", request.getRequestURL().toString());
        log.info("请求方法   ==> : {}", request.getMethod());
        log.info("Class Method  : {}.{}", point.getSignature().getDeclaringTypeName(), point.getSignature().getName());
        log.info("当前登录人   ==> : {}", JSONObject.toJSONString(userInfo));
    }
}
